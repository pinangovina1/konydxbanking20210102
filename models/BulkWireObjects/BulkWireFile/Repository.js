define([], function(){
	var BaseRepository = kony.mvc.Data.BaseRepository;

	//Create the Repository Class
	function BulkWireFileRepository(modelDefinition, config, defaultAppMode, dataSourceFactory, injectedDataSource) {
		BaseRepository.call(this, modelDefinition, config, defaultAppMode, dataSourceFactory, injectedDataSource);
	};

	//Setting BaseRepository as Parent to this Repository
	BulkWireFileRepository.prototype = Object.create(BaseRepository.prototype);
	BulkWireFileRepository.prototype.constructor = BulkWireFileRepository;

	//For Operation 'getBulkWireFileFormatTypes' with service id 'getBulkWireFileFormatTypes9584'
	BulkWireFileRepository.prototype.getBulkWireFileFormatTypes = function(params, onCompletion){
		return BulkWireFileRepository.prototype.customVerb('getBulkWireFileFormatTypes', params, onCompletion);
	};

	//For Operation 'downloadSampleFileBulkWire' with service id 'downloadSampleFileBulkWire1967'
	BulkWireFileRepository.prototype.downloadSampleFileBulkWire = function(params, onCompletion){
		return BulkWireFileRepository.prototype.customVerb('downloadSampleFileBulkWire', params, onCompletion);
	};

	//For Operation 'getBulkWireFilesForUser' with service id 'getBulkWireFilesForUser2288'
	BulkWireFileRepository.prototype.getBulkWireFilesForUser = function(params, onCompletion){
		return BulkWireFileRepository.prototype.customVerb('getBulkWireFilesForUser', params, onCompletion);
	};

	//For Operation 'getBulkwireFileLineItems' with service id 'getBulkWireFileLineItems1730'
	BulkWireFileRepository.prototype.getBulkwireFileLineItems = function(params, onCompletion){
		return BulkWireFileRepository.prototype.customVerb('getBulkwireFileLineItems', params, onCompletion);
	};

	//For Operation 'downloadFileBulkWire' with service id 'downloadFileBulkWire2111'
	BulkWireFileRepository.prototype.downloadFileBulkWire = function(params, onCompletion){
		return BulkWireFileRepository.prototype.customVerb('downloadFileBulkWire', params, onCompletion);
	};

	return BulkWireFileRepository;
})