define([], function(){
	var BaseRepository = kony.mvc.Data.BaseRepository;

	//Create the Repository Class
	function ConsentRepository(modelDefinition, config, defaultAppMode, dataSourceFactory, injectedDataSource) {
		BaseRepository.call(this, modelDefinition, config, defaultAppMode, dataSourceFactory, injectedDataSource);
	};

	//Setting BaseRepository as Parent to this Repository
	ConsentRepository.prototype = Object.create(BaseRepository.prototype);
	ConsentRepository.prototype.constructor = ConsentRepository;

	//For Operation 'createTermsAndConditions' with service id 'createTermsAndConditions7692'
	ConsentRepository.prototype.createTermsAndConditions = function(params, onCompletion){
		return ConsentRepository.prototype.customVerb('createTermsAndConditions', params, onCompletion);
	};

	//For Operation 'refreshConsent' with service id 'refreshConnection4725'
	ConsentRepository.prototype.refreshConsent = function(params, onCompletion){
		return ConsentRepository.prototype.customVerb('refreshConsent', params, onCompletion);
	};

	return ConsentRepository;
})