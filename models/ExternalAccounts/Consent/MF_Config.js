/*
    This is an auto generated file and any modifications to it may result in corrupted data.
*/
define([], function() {
	var mappings = {
		"connect_url": "connect_url",
		"digitalProfileId": "digitalProfileId",
		"expires_at": "expires_at",
		"fetch_scopes": "fetch_scopes",
		"from_date": "from_date",
		"javascript_callback_type": "javascript_callback_type",
		"period_days": "period_days",
		"providerCode": "providerCode",
		"scopes": "scopes",
	};

	Object.freeze(mappings);

	var typings = {
		"connect_url": "string",
		"digitalProfileId": "string",
		"expires_at": "string",
		"fetch_scopes": "string",
		"from_date": "string",
		"javascript_callback_type": "string",
		"period_days": "string",
		"providerCode": "string",
		"scopes": "string",
	}

	Object.freeze(typings);

	var primaryKeys = [
	];

	Object.freeze(primaryKeys);

	var config = {
		mappings: mappings,
		typings: typings,
		primaryKeys: primaryKeys,
		serviceName: "ExternalAccounts",
		tableName: "Consent"
	};

	Object.freeze(config);

	return config;
})