define([], function(){
	var BaseRepository = kony.mvc.Data.BaseRepository;

	//Create the Repository Class
	function InfinityUserRepository(modelDefinition, config, defaultAppMode, dataSourceFactory, injectedDataSource) {
		BaseRepository.call(this, modelDefinition, config, defaultAppMode, dataSourceFactory, injectedDataSource);
	};

	//Setting BaseRepository as Parent to this Repository
	InfinityUserRepository.prototype = Object.create(BaseRepository.prototype);
	InfinityUserRepository.prototype.constructor = InfinityUserRepository;

	//For Operation 'createCustomRole' with service id 'CreateCustomRole5356'
	InfinityUserRepository.prototype.createCustomRole = function(params, onCompletion){
		return InfinityUserRepository.prototype.customVerb('createCustomRole', params, onCompletion);
	};

	//For Operation 'updateCustomRole' with service id 'updateCustomRole8583'
	InfinityUserRepository.prototype.updateCustomRole = function(params, onCompletion){
		return InfinityUserRepository.prototype.customVerb('updateCustomRole', params, onCompletion);
	};

	//For Operation 'getInfinityUserAccounts' with service id 'GetInfinityUserAccounts1944'
	InfinityUserRepository.prototype.getInfinityUserAccounts = function(params, onCompletion){
		return InfinityUserRepository.prototype.customVerb('getInfinityUserAccounts', params, onCompletion);
	};

	//For Operation 'getInfinityUserContractDetails' with service id 'GetInfinityUserContractDetails7453'
	InfinityUserRepository.prototype.getInfinityUserContractDetails = function(params, onCompletion){
		return InfinityUserRepository.prototype.customVerb('getInfinityUserContractDetails', params, onCompletion);
	};

	//For Operation 'getAllEligibleRelationalCustomers' with service id 'getAllEligibleRelationalCustomers6195'
	InfinityUserRepository.prototype.getAllEligibleRelationalCustomers = function(params, onCompletion){
		return InfinityUserRepository.prototype.customVerb('getAllEligibleRelationalCustomers', params, onCompletion);
	};

	//For Operation 'getInfinityUserFeatureActions' with service id 'GetInfinityUserFeatureActions4845'
	InfinityUserRepository.prototype.getInfinityUserFeatureActions = function(params, onCompletion){
		return InfinityUserRepository.prototype.customVerb('getInfinityUserFeatureActions', params, onCompletion);
	};

	//For Operation 'createInfinityUser' with service id 'createInfinityUser6095'
	InfinityUserRepository.prototype.createInfinityUser = function(params, onCompletion){
		return InfinityUserRepository.prototype.customVerb('createInfinityUser', params, onCompletion);
	};

	//For Operation 'getCoreCustomerFeatureActionLimits' with service id 'GetCoreCustomerFeatureActionLimits8284'
	InfinityUserRepository.prototype.getCoreCustomerFeatureActionLimits = function(params, onCompletion){
		return InfinityUserRepository.prototype.customVerb('getCoreCustomerFeatureActionLimits', params, onCompletion);
	};

	//For Operation 'getCustomRoleByCompanyID' with service id 'GetCustomRoleByCompanyID5803'
	InfinityUserRepository.prototype.getCustomRoleByCompanyID = function(params, onCompletion){
		return InfinityUserRepository.prototype.customVerb('getCustomRoleByCompanyID', params, onCompletion);
	};

	//For Operation 'getAssociatedCustomers' with service id 'getAssociatedCustomers1309'
	InfinityUserRepository.prototype.getAssociatedCustomers = function(params, onCompletion){
		return InfinityUserRepository.prototype.customVerb('getAssociatedCustomers', params, onCompletion);
	};

	//For Operation 'editInfinityUser' with service id 'editInfinityUser4362'
	InfinityUserRepository.prototype.editInfinityUser = function(params, onCompletion){
		return InfinityUserRepository.prototype.customVerb('editInfinityUser', params, onCompletion);
	};

	//For Operation 'getAssociatedContractUsers' with service id 'GetAssociatedContractUsers3892'
	InfinityUserRepository.prototype.getAssociatedContractUsers = function(params, onCompletion){
		return InfinityUserRepository.prototype.customVerb('getAssociatedContractUsers', params, onCompletion);
	};

	//For Operation 'getCompanyLevelCustomRoles' with service id 'GetCompanyLevelCustomRoles5912'
	InfinityUserRepository.prototype.getCompanyLevelCustomRoles = function(params, onCompletion){
		return InfinityUserRepository.prototype.customVerb('getCompanyLevelCustomRoles', params, onCompletion);
	};

	//For Operation 'getInfinityUser' with service id 'getInfinityUser6429'
	InfinityUserRepository.prototype.getInfinityUser = function(params, onCompletion){
		return InfinityUserRepository.prototype.customVerb('getInfinityUser', params, onCompletion);
	};

	//For Operation 'getCustomRoleDetails' with service id 'getCustomRoleDetails9302'
	InfinityUserRepository.prototype.getCustomRoleDetails = function(params, onCompletion){
		return InfinityUserRepository.prototype.customVerb('getCustomRoleDetails', params, onCompletion);
	};

	//For Operation 'getInfinityUserLimits' with service id 'GetInfinityUserLimits1247'
	InfinityUserRepository.prototype.getInfinityUserLimits = function(params, onCompletion){
		return InfinityUserRepository.prototype.customVerb('getInfinityUserLimits', params, onCompletion);
	};

	//For Operation 'getInfinityUserContractCustomers' with service id 'GetInfinityUserContractCustomers6762'
	InfinityUserRepository.prototype.getInfinityUserContractCustomers = function(params, onCompletion){
		return InfinityUserRepository.prototype.customVerb('getInfinityUserContractCustomers', params, onCompletion);
	};

	//For Operation 'verifyCustomRoleName' with service id 'verifyCustomRoleName8176'
	InfinityUserRepository.prototype.verifyCustomRoleName = function(params, onCompletion){
		return InfinityUserRepository.prototype.customVerb('verifyCustomRoleName', params, onCompletion);
	};

	//For Operation 'getInfinityUserContractCoreCustomerActions' with service id 'GetInfinityUserContractCoreCustomerActions3789'
	InfinityUserRepository.prototype.getInfinityUserContractCoreCustomerActions = function(params, onCompletion){
		return InfinityUserRepository.prototype.customVerb('getInfinityUserContractCoreCustomerActions', params, onCompletion);
	};

	//For Operation 'CreateInfinityUserWithContract' with service id 'CreateInfinityUserWithContract3507'
	InfinityUserRepository.prototype.CreateInfinityUserWithContract = function(params, onCompletion){
		return InfinityUserRepository.prototype.customVerb('CreateInfinityUserWithContract', params, onCompletion);
	};

	//For Operation 'GetListCoreCustomerFeatureActionLimits' with service id 'GetListCoreCustomerFeatureActionLimits3221'
	InfinityUserRepository.prototype.GetListCoreCustomerFeatureActionLimits = function(params, onCompletion){
		return InfinityUserRepository.prototype.customVerb('GetListCoreCustomerFeatureActionLimits', params, onCompletion);
	};

	return InfinityUserRepository;
})