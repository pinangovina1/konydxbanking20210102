define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** postShow defined for frmBudgetInitiateTransferAck **/
    AS_Form_c4f18440cb7142d18e8ad9b865d3c6e0: function AS_Form_c4f18440cb7142d18e8ad9b865d3c6e0(eventobject) {
        var self = this;
        this.postShow();
    },
    /** init defined for frmBudgetInitiateTransferAck **/
    AS_Form_g6ee4900cf2148b291c9d899a6827e57: function AS_Form_g6ee4900cf2148b291c9d899a6827e57(eventobject) {
        var self = this;
        this.init();
    },
    /** preShow defined for frmBudgetInitiateTransferAck **/
    AS_Form_icf6f0f50b9a4dfda37ebf21bcfb0b22: function AS_Form_icf6f0f50b9a4dfda37ebf21bcfb0b22(eventobject) {
        var self = this;
        this.preShow();
    }
});