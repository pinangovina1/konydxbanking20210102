define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    AS_BarButtonItem_d95f4c7242df4751a8621a007d0d2ff2: function AS_BarButtonItem_d95f4c7242df4751a8621a007d0d2ff2(eventobject) {
        var self = this;
        this.onCancelClick();
    },
    /** onClick defined for btnSeven **/
    AS_Button_af6f9b532bcb4e72b3437cd94801f0fb: function AS_Button_af6f9b532bcb4e72b3437cd94801f0fb(eventobject) {
        var self = this;
        this.setKeypadChar(7);
    },
    /** onClick defined for btnZero **/
    AS_Button_bad26045e4764e6892d9dd7779fa1f0d: function AS_Button_bad26045e4764e6892d9dd7779fa1f0d(eventobject) {
        var self = this;
        this.setKeypadChar(0);
    },
    /** onClick defined for btnTwo **/
    AS_Button_c1b1ad09661247a7a5762222b6dd49a7: function AS_Button_c1b1ad09661247a7a5762222b6dd49a7(eventobject) {
        var self = this;
        this.setKeypadChar(2);
    },
    /** onClick defined for btnDot **/
    AS_Button_c7c86a1899eb4ec98929364051fc959f: function AS_Button_c7c86a1899eb4ec98929364051fc959f(eventobject) {
        var self = this;
        this.setKeypadChar('.');
    },
    /** onClick defined for btnOne **/
    AS_Button_e36927aa07be4fb6b26680f46999ae80: function AS_Button_e36927aa07be4fb6b26680f46999ae80(eventobject) {
        var self = this;
        this.setKeypadChar(1);
    },
    /** onClick defined for btnFour **/
    AS_Button_ec9cd86084844583af2b482a829b78f4: function AS_Button_ec9cd86084844583af2b482a829b78f4(eventobject) {
        var self = this;
        this.setKeypadChar(4);
    },
    /** onClick defined for btnSix **/
    AS_Button_f21008d2aa3e484db49c38c12a6fcf1b: function AS_Button_f21008d2aa3e484db49c38c12a6fcf1b(eventobject) {
        var self = this;
        this.setKeypadChar(6);
    },
    /** onClick defined for btnEight **/
    AS_Button_f27cbfd97cc14858b4280c899ad6a6a2: function AS_Button_f27cbfd97cc14858b4280c899ad6a6a2(eventobject) {
        var self = this;
        this.setKeypadChar(8);
    },
    /** onClick defined for btnFive **/
    AS_Button_g064cdc4508749e1ba564f9efa50e1c9: function AS_Button_g064cdc4508749e1ba564f9efa50e1c9(eventobject) {
        var self = this;
        this.setKeypadChar(5);
    },
    /** onClick defined for btnNine **/
    AS_Button_g414fbc43114438aa95f319c481a3219: function AS_Button_g414fbc43114438aa95f319c481a3219(eventobject) {
        var self = this;
        this.setKeypadChar(9);
    },
    /** onClick defined for btnThree **/
    AS_Button_i5e10a78d1e04ab8a417fdd5bf769938: function AS_Button_i5e10a78d1e04ab8a417fdd5bf769938(eventobject) {
        var self = this;
        this.setKeypadChar(3);
    },
    /** init defined for frmEditOptimizeGoal **/
    AS_Form_b599889d5de149d4b39047195c84e44d: function AS_Form_b599889d5de149d4b39047195c84e44d(eventobject) {
        var self = this;
        this.init();
    },
    /** postShow defined for frmEditOptimizeGoal **/
    AS_Form_d535a2a6352b4985ac10831f0bd64183: function AS_Form_d535a2a6352b4985ac10831f0bd64183(eventobject) {
        var self = this;
        this.postShow();
    },
    /** preShow defined for frmEditOptimizeGoal **/
    AS_Form_j8bb4e5d65c8441e86e9a82d16bf5c25: function AS_Form_j8bb4e5d65c8441e86e9a82d16bf5c25(eventobject) {
        var self = this;
        this.preShow();
    },
    /** onTouchEnd defined for imgClearKeypad **/
    AS_Image_ae13af8ebc4340f1b77764127888fb57: function AS_Image_ae13af8ebc4340f1b77764127888fb57(eventobject, x, y) {
        var self = this;
        this.clearKeypadChar()
    }
});