define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    AS_BarButtonItem_cfa7c778dde44a88b9a17298d68c0540: function AS_BarButtonItem_cfa7c778dde44a88b9a17298d68c0540(eventobject) {
        var self = this;
        this.onCancelClick();
    },
    /** preShow defined for frmSCReview **/
    AS_Form_d34108a610024c22b1a892512e72726e: function AS_Form_d34108a610024c22b1a892512e72726e(eventobject) {
        var self = this;
        this.preshow();
    },
    /** init defined for frmSCReview **/
    AS_Form_h81e163899154bdc9f68b8e39d31a9b3: function AS_Form_h81e163899154bdc9f68b8e39d31a9b3(eventobject) {
        var self = this;
        this.init();
    },
    /** postShow defined for frmSCReview **/
    AS_Form_i465be7b630f44e7a5f4e266d54a7722: function AS_Form_i465be7b630f44e7a5f4e266d54a7722(eventobject) {
        var self = this;
        this.postShow();
    }
});