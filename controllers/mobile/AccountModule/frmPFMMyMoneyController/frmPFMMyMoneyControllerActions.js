define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onClick defined for flxDummy **/
    AS_FlexContainer_f440b7436f27438ca4595c1d01793cff: function AS_FlexContainer_f440b7436f27438ca4595c1d01793cff(eventobject) {
        var self = this;
        // Dummy Action
    },
    /** postShow defined for frmPFMMyMoney **/
    AS_Form_a5df6ed5cc624a70aa85db84531df974: function AS_Form_a5df6ed5cc624a70aa85db84531df974(eventobject) {
        var self = this;
        this.pfmPostshow();
    },
    /** preShow defined for frmPFMMyMoney **/
    AS_Form_c9294c1e0a0243fb9f426fd2dd224ffe: function AS_Form_c9294c1e0a0243fb9f426fd2dd224ffe(eventobject) {
        var self = this;
        this.pfmPreshow();
    },
    /** init defined for frmPFMMyMoney **/
    AS_Form_jef8886af76f4bf09cfd02330accc0e1: function AS_Form_jef8886af76f4bf09cfd02330accc0e1(eventobject) {
        var self = this;
        this.init();
    }
});