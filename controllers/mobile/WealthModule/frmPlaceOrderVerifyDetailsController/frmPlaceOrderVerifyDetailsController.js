define({

	ORDER_TYPE: {
		MARKET: 'Market',
		LIMIT: 'Limit',
		STOPLOSS: 'Stop Loss',
		STOPLIMIT: 'Stop Limit'
	},

	VALIDITY: {
		DAYORDER: 'Day Order',
		GOODTILL: 'Good Till Cancelled'
	},
	init: function () {
		var navManager = applicationManager.getNavigationManager();
		var currentForm = navManager.getCurrentForm();
		applicationManager.getPresentationFormUtility().initCommonActions(this, "YES", currentForm);
	},


	preShow: function () {
		if (applicationManager.getPresentationFormUtility().getDeviceName() === "iPhone") {
			this.view.flxHeader.setVisibility(false);
			this.view.flxMainContainer.top = "0dp";
		} else {
			this.view.flxHeader.setVisibility(true);
		}


	},

	postShow: function () {
		this.initActions();
		var navManager = applicationManager.getNavigationManager();
		const orderData = navManager.getCustomInfo('frmInstrumentOrder');
		if (orderData.orderType === this.ORDER_TYPE.LIMIT) {
			this.view.flxSeparator5.setVisibility(true);
			this.view.flxSeparator6.setVisibility(false);

			this.view.flxLimitPrice.setVisibility(true);
			this.view.flxStopPrice.setVisibility(false);

		} else if (orderData.orderType === this.ORDER_TYPE.STOPLOSS) {
			this.view.flxSeparator5.setVisibility(false);
			this.view.flxSeparator6.setVisibility(true);
			this.view.flxLimitPrice.setVisibility(false);
			this.view.flxStopPrice.setVisibility(true);
		} else if (orderData.orderType === this.ORDER_TYPE.STOPLIMIT) {
			this.view.flxSeparator5.setVisibility(true);
			this.view.flxSeparator6.setVisibility(true);
			this.view.flxLimitPrice.setVisibility(true);
			this.view.flxStopPrice.setVisibility(true);
		} else { //For ordertype MARKET:
			this.view.flxSeparator5.setVisibility(false);
			this.view.flxSeparator6.setVisibility(false);
			this.view.flxLimitPrice.setVisibility(false);
			this.view.flxStopPrice.setVisibility(false);
		}
		this.setUIData();
	},

	initActions: function () {
		this.view.customHeader.flxBack.onTouchEnd = this.onBack;
		this.view.customHeader.btnRight.onClick = this.cancelOnClick;
		this.view.btnContinue.onClick = this.confirmOnClick;
		this.view.imgInfo.onTouchEnd = this.onClickInfo;
		this.view.flxNo.onTouchEnd = this.onClose;
	},
	onClickInfo: function () {
		this.view.flxInfo.isVisible = true;
	},
	onClose: function () {
		this.view.flxInfo.isVisible = false;
	},
	setUIData: function () {

		var wealthMod = applicationManager.getModulesPresentationController("WealthModule");
		var formatUtil = applicationManager.getFormatUtilManager();
		// var data = wealthMod.getWealthObject();
		var navManager = applicationManager.getNavigationManager();
		const data1 = navManager.getCustomInfo('frmInstrumentDetails');
		const data = data1.instrumentDetails.instrumentDetails;
		const orderData = navManager.getCustomInfo('frmInstrumentOrder');
		var quantity = orderData.quantity;
		var marketData = scope_WealthPresentationController.marketOrder;
	//	this.view.lblInstrumentVal.text = data.instrumentName;
      if(data1.response !== undefined){
        var currentPosition=data1.response;
        this.view.lblInstrumentVal.text = currentPosition.description;
        }
        else{
		this.view.lblInstrumentVal.text = orderData.description;
        }
		
		this.view.lblOrderVal.text = orderData.order;
		this.view.lblQuantityVal.text = quantity.toString();
		this.view.lblOrderTypeVal.text = orderData.orderType;
		this.view.lblCurrentPriceVal.text = formatUtil.formatAmountandAppendCurrencySymbol(scope_WealthPresentationController.lastPrice, data.referenceCurrency);
		this.view.lblValidityVal.text = orderData.validity;
		this.view.lblAmountVal.text = formatUtil.formatAmountandAppendCurrencySymbol(orderData.netAmount, data.referenceCurrency);
		// Fees to be taken from Service response
		this.view.lblFeesVal.text = formatUtil.formatAmountandAppendCurrencySymbol(marketData.fees, data.referenceCurrency);
		if (orderData.orderType === this.ORDER_TYPE.LIMIT || orderData.orderType === this.ORDER_TYPE.STOPLIMIT) {

			this.view.lblLimitPriceVal.text = formatUtil.formatAmountandAppendCurrencySymbol(orderData.limitPrice, data.referenceCurrency);
		}
		if (orderData.orderType === this.ORDER_TYPE.STOPLOSS || orderData.orderType === this.ORDER_TYPE.STOPLIMIT) {
			this.view.lblStopPrice.text = orderData.orderType === this.ORDER_TYPE.STOPLOSS ? 'Stop Loss:' : 'Stop Price:'

			const stopText = orderData.orderType === this.ORDER_TYPE.STOPLIMIT ? orderData.stopPrice : orderData.stopLoss;
			this.view.lblStopPriceVal.text = formatUtil.formatAmountandAppendCurrencySymbol(stopText, data.referenceCurrency);
		}
	},


	onBack: function () {

		var navigationMan = applicationManager.getNavigationManager();
		navigationMan.goBack();
	},

	cancelOnClick: function () {
		var params = {
			"portfolioId": scope_WealthPresentationController.portfolioId,
			"navPage": "Portfolio",
			"graphDuration": "OneY"
		};

		var wealthModule = applicationManager.getModulesPresentationController("WealthModule");

		wealthModule.getPortfolioAndGraphDetails(params);

	},


	confirmOnClick: function () {
		var wealthMod = applicationManager.getModulesPresentationController("WealthModule");
		var data = wealthMod.getWealthObject();
		var nav = applicationManager.getNavigationManager();
		const orderData = nav.getCustomInfo('frmInstrumentOrder');
		wealthMod.createMarketOrder(orderData);
		/*var param={
		  "buyCurrency":data.buyCurrency,
		  "sellCurrency":data.sellCurrency,
		  "buyAmount":data.buyAmount,
		  "sellAmount":data.sellAmount
		}
      
		wealthMod.createOrder(param);*/

	},
	bindGenericError: function (errorMsg) {
		applicationManager.getPresentationUtility().dismissLoadingScreen();
		var scopeObj = this;
		applicationManager.getDataProcessorUtility().showToastMessageError(scopeObj, errorMsg);
	}


});
