define(['CommonUtilities'], function(CommonUtilities) {
    return {
        portfolioData: '',
        xaxisArr: [],
        plotDataArr: [],
        chartConfig: '',
        cashAcc: [],
        marketValue: '',
        init: function() {
            var navManager = applicationManager.getNavigationManager();
            var currentForm = navManager.getCurrentForm();
            applicationManager.getPresentationFormUtility().initCommonActions(this, "YES", currentForm);

        },

        preShow: function() {
            if (applicationManager.getPresentationFormUtility().getDeviceName() !== "iPhone") {
                this.view.flxHeader.isVisible = true;
            } else {

                this.view.flxHeader.isVisible = false;
            }
            this.initActions();
            this.fetchAssetsAndCashData();
            var navManager = applicationManager.getNavigationManager();
            var objArr = navManager.getCustomInfo("frmPortfolioDetails");
            this.portfolioData = objArr.response;
            this.chartConfig = "1M";
            var graphDuration = this.portfolioData.graphDuration;
            this.changeGraphData(graphDuration, false);
            this.setDataToForm();
          this.view.investmentLineChart.currentFilter = this.chartConfig;
        },
        setDataToForm: function() {
            var val = this.portfolioData;
            var wealthModule = applicationManager.getModulesPresentationController("WealthModule");
            this.view.customHeader.lblLocateUs.text = wealthModule.getMaskedAccountName();
            this.view.title = wealthModule.getMaskedAccountName();
            var forUtility = applicationManager.getFormatUtilManager();
            this.view.investmentLineChart.currencySymbol = forUtility.getCurrencySymbol(val.referenceCurrency);
            var totalVal = forUtility.formatAmountandAppendCurrencySymbol(val.marketValue, val.referenceCurrency);
            this.view.lblTotalVal.text = totalVal;
            this.marketValue = val.marketValue;
            var unrealizedPL = forUtility.formatAmountandAppendCurrencySymbol(val.unRealizedPLAmount, val.referenceCurrency);
            var todaysPL = forUtility.formatAmountandAppendCurrencySymbol(val.todayPLAmount, val.referenceCurrency);
            if (val.unRealizedPL == "P") {
                this.view.lblUnrealizedPLValue.skin = "sknIWlbl2F8523SemiBold15px";
                this.view.lblUnrealizedPLValue.text = "+" + unrealizedPL + " (+" + val.unRealizedPLPercentage + "%)";
            } else {
                this.view.lblUnrealizedPLValue.skin = "sknIblEE0005SSPsb45px";
                this.view.lblUnrealizedPLValue.text = "-" + unrealizedPL + " (-" + val.unRealizedPLPercentage + "%)";
            }
            if (val.todayPL == "P") {
                this.view.lblTodayPLValue.skin = "sknIWlbl2F8523SemiBold15px";
                this.view.lblTodayPLValue.text = "+" + todaysPL + " (+" + val.todayPLPercentage + "%)";
            } else {
                this.view.lblTodayPLValue.skin = "sknIblEE0005SSPsb45px";
                this.view.lblTodayPLValue.text = "-" + todaysPL + " (-" + val.todayPLPercentage + "%)";
            }
        },
        fetchAssetsAndCashData: function() {
            var inputParamsForAssetsAndCash = {
                "portfolioId": scope_WealthPresentationController.portfolioId
            };
            var wealthModule = applicationManager.getModulesPresentationController("WealthModule");
            wealthModule.getAssetsAllocation(inputParamsForAssetsAndCash);
        },
        navigateToSearch: function() {
            var params = {
                "sortBy": "",
                "searchByInstrumentName": "",
                "portfolioId": scope_WealthPresentationController.portfolioId
            }
            var wealthModule = applicationManager.getModulesPresentationController("WealthModule");
            wealthModule.getInstrumentSearchList(params, this.marketValue);

        },
        postShow: function() {
            //this.fetchAssetsAndCashData();
        },
        initActions: function() {
            this.view.flxAdditionalOptions.setVisibility(false);
            this.view.flxSearchDummy.onTouchEnd = this.navigateToSearch;
            this.view.customHeader.flxBack.onClick = this.navigateCustomBack;
            this.view.flxHoldings.onTouchEnd = this.holdings;
            this.view.flxTransactions.onTouchEnd = this.transactions;
            this.view.flxOrders.onTouchEnd = this.openOrder;
            this.view.flxMore.onTouchEnd = this.moreOptions;
            this.view.flxCancelOption.onTouchEnd = this.onCancel;
            this.view.lblAccounts.onTouchEnd = this.accounts;
            this.view.btnConvertCurrency.onClick = this.clickConvertCurrency;
            let filterValues = Object.keys(this.chartFilters).map(key => this.chartFilters[key]);
            this.view.investmentLineChart.setChartFilters(filterValues);
            this.view.flxCashSeparator.setVisibility(false);
        },
        calculatePercent: function(part, total) {
            var partAmount = Number(part);
            var totalAmount = Number(total);
            var percent = (partAmount / totalAmount) * 100;
            return percent.toFixed(2);
        },
        setSkinBasedOnPercent: function(dataArr) {
            var skinVal = ["sknFlexslider2C82BE", "sknFlxslider53A8E2", "sknFlxslider24BFEC", "sknFlxslider76DDFB", "sknFlxsliderC7E0F1"];
            dataArr[0].flxSpent = {
                "skin": skinVal[0],
                "width": dataArr[0].assetPer.toString() + "%"
            };
            for (var i = 1; i <= dataArr.length - 1; i++) {
                if (dataArr[i].assetPer === dataArr[i - 1].assetPer) {
                    dataArr[i].flxSpent = {
                        "skin": skinVal[i - 1],
                        "width": dataArr[i].assetPer.toString() + "%"
                    };
                } else {
                    dataArr[i].flxSpent = {
                        "skin": skinVal[i],
                        "width": dataArr[i].assetPer.toString() + "%"
                    };
                }
            }
            return dataArr;
        },
      openOrder: function() {
          var sortByValue = undefined; 
          var type = "open";
          var today = new Date();
          var data = {};
          data.response = sortByValue;
          var navManager = applicationManager.getNavigationManager();
          navManager.setCustomInfo("frmSortBy", data);
		  navManager.setCustomInfo("frmPortfolioDetails", true);
          var orderId;
        if(scope_WealthPresentationController.orderList.length === 0){
          orderId = null;
        }
        else{
          orderId = scope_WealthPresentationController.orderList.toString();
        }
        var previousDate = new Date(today.getTime() - (30 * 24 * 60 * 60 * 1000));
        var startDate = previousDate.getFullYear() + '-' + ('0' + (previousDate.getMonth() + 1)).slice(-2) + '-' + ('0' + previousDate.getDate()).slice(-2);
        var endDate = today.getFullYear() + '-' + ('0' + (today.getMonth() + 1)).slice(-2) + '-' + ('0' + today.getDate()).slice(-2);
            var params = {
                "portfolioId": scope_WealthPresentationController.portfolioId,
                "orderId": orderId,
                "sortBy": "description",
                "type": type, 
                "startDate": "2018-12-06",
				"endDate": endDate,
                "searchByInstrumentName": ""
          }
        var wealthModule = applicationManager.getModulesPresentationController("WealthModule");
        var orderList = wealthModule.getOrdersDetails(params);
        scope_WealthPresentationController.isPortfolio = true;
        scope_WealthPresentationController.isDateRange = false;
        scope_WealthPresentationController.isFirst = true;
        wealthModule.commonFunctionForNavigation("frmOrder");
        },
        populateAssets: function(assetObj) {
            var currForm = kony.application.getCurrentForm();
            var responseObj = assetObj.response;
            var assets = responseObj.assets;
            var assignedSkinsArr = [];
            var segData = [];
            var forUtility = applicationManager.getFormatUtilManager();
            for (var list in assets) {
                var assetPercent = this.calculatePercent(assets[list].marketValue, responseObj.totalMarketValue);
                var marketValue = forUtility.formatAmountandAppendCurrencySymbol(assets[list].marketValue, responseObj.referenceCurrency);
                var storeData;
                if (assetPercent !== "0") {
                    storeData = {
                        assetClass: assets[list].assetGroup,
                        assetPer: assetPercent,
                        assetVal: assets[list].assetGroup + " (" + assetPercent.toString() + "%)",
                        assetCost: marketValue,
                        flxSpent: {
                            "skin": "",
                            "width": assetPercent.toString() + "%"
                        }
                    };
                    segData.push(storeData);
                } else {
                    continue;
                }
            }
            segData.sort(function(a, b) {
                return parseFloat(b.assetPer) - parseFloat(a.assetPer);
            });
            assignedSkinsArr = this.setSkinBasedOnPercent(segData);
            this.view.segAssetSummary.widgetDataMap = {
                lblAssetDet: "assetVal",
                lblAssetCost: "assetCost",
                flxSpent: "flxSpent"
            };
            this.view.segAssetSummary.setData(assignedSkinsArr);
            this.cashAcc = [];
            var totalVal = "";
            var totalRefVal = "";
            var trimmedAccName = "";
            var cashAccFromResponse = [];
            cashAccFromResponse = responseObj.cashAccounts;
            this.cashAcc = this.checkCashBalance(cashAccFromResponse);
            scope_WealthPresentationController.portfolioCashAccounts = this.cashAcc;
            var cashList;
            var cashData = [];
            for (var i in this.cashAcc) {
                trimmedAccName = CommonUtilities.truncateStringWithGivenLength(this.cashAcc[i].accountName + "....", 26) + CommonUtilities.getLastFourDigit(this.cashAcc[i].accountNumber);
                totalVal = forUtility.formatAmountandAppendCurrencySymbol(this.cashAcc[i].balance, this.cashAcc[i].currency);
                totalRefVal = forUtility.formatAmountandAppendCurrencySymbol(this.cashAcc[i].referenceCurrencyValue, responseObj.totalCashBalanceCurrency);
                cashList = {
                    accountName: trimmedAccName,
                    cashBalance: totalVal,
                    refCashBalance: totalRefVal,
                    refCurrency: this.cashAcc[i].currency,
                    accountNumber: this.cashAcc[i].accountNumber
                };
                cashData.push(cashList);
            }
            if (cashData.length === 1) {
                if (this.cashAcc[0].currency === responseObj.totalCashBalanceCurrency) {
                    this.view.segCashSummary.setVisibility(false);
                    this.view.flxCashSeparator.setVisibility(true);
                } else {
                    this.view.segCashSummary.setVisibility(true);
                    this.view.flxCashSeparator.setVisibility(false);
                }
            } else {
                this.view.segCashSummary.setVisibility(true);
                this.view.flxCashSeparator.setVisibility(false);
            }
            this.view.segCashSummary.widgetDataMap = {
                lblCashAcc: "accountName",
                lblCashAccBal: "cashBalance",
                lblRefCashBal: "refCashBalance"
            };
            scope_WealthPresentationController.totalCashBalance = responseObj.totalCashBalance;
            scope_WealthPresentationController.totalCashBalanceCurrency = responseObj.totalCashBalanceCurrency;
            var cashBalTotal = forUtility.formatAmountandAppendCurrencySymbol(responseObj.totalCashBalance, responseObj.totalCashBalanceCurrency);
            this.view.lblCashBalanceVal.text = cashBalTotal;
            this.view.segCashSummary.setData(cashData);
          scope_WealthPresentationController.accountNumber = cashData[0].accountNumber;
            var navMan = applicationManager.getNavigationManager();
            var dataSet = {};
            dataSet.cashData = cashData;
            dataSet.response = cashData[0].refCurrency + "-" + cashData[0].accountName.slice(-4);
            dataSet.accountName = cashData[0].refCurrency + " " + cashData[0].accountName;
            navMan.setCustomInfo('frmCashAccounts', dataSet);
            currForm.forceLayout();
        },
        checkCashBalance: function(cashArr) {
            if (scope_WealthPresentationController.newAccountsArr.length > 0) {
                cashArr.push(...scope_WealthPresentationController.newAccountsArr);
            }
          if(scope_WealthPresentationController.balanceArr.length > 0){
          for(i in scope_WealthPresentationController.balanceArr){
            cashArr.forEach(function(e) {
                if (e.currency === scope_WealthPresentationController.balanceArr[i].currency) {
                    e.balance = scope_WealthPresentationController.balanceArr[i].amount;
                }
            });
            }
            }
            return cashArr;
        },
        navigateCustomBack: function() {
            var wealthModule = applicationManager.getModulesPresentationController("WealthModule");
            wealthModule.commonFunctionForgoBack();
        },
        holdings: function() {
            var sortByValue = undefined;
            var data = {};
            data.response = sortByValue;
            var navManager = applicationManager.getNavigationManager();
            navManager.setCustomInfo("frmSortBy", data);
            navManager.setCustomInfo("frmPortfolioDetails", true);
            var params = {
                "portfolioId": scope_WealthPresentationController.portfolioId,
                "navPage": "Holdings",
                "sortBy": "description",
                "searchByInstrumentName": ""
            }
            var wealthModule = applicationManager.getModulesPresentationController("WealthModule");
            wealthModule.getHoldings(params);
        },
        transactions: function() {
            var navManager = applicationManager.getNavigationManager();
            var sortByValue = undefined;
            var data = {};
            data.response = sortByValue;
            navManager.setCustomInfo("frmPortfolioDetails", true);
            navManager.setCustomInfo("frmSortBy", data);
            data.selectedPeriod = "previous30DaysSelected";
            navManager.setCustomInfo("frmDateRange", data);
            var today = new Date();
            var previousDate = new Date(today.getTime() - (30 * 24 * 60 * 60 * 1000));
            var startDate = previousDate.getFullYear() + '-' + ('0' + (previousDate.getMonth() + 1)).slice(-2) + '-' + ('0' + previousDate.getDate()).slice(-2);
            var endDate = today.getFullYear() + '-' + ('0' + (today.getMonth() + 1)).slice(-2) + '-' + ('0' + today.getDate()).slice(-2);

            var params = {
                "portfolioId": scope_WealthPresentationController.portfolioId,
                "startDate": startDate,
                "endDate": endDate,
                "searchByInstrumentName": "",
                "sortBy": "tradeDate"
            }
            var wealthModule = applicationManager.getModulesPresentationController("WealthModule");
            wealthModule.getTransactions(params);

        },
        
        accounts: function() {
            var navManager = applicationManager.getNavigationManager();
            var sortByValue = undefined;
            var data = {};
            data.response = sortByValue;
            navManager.setCustomInfo("frmPortfolioDetails", true);
            navManager.setCustomInfo("frmSortBy", data);
            data.selectedPeriod = "previous30DaysSelected";
            navManager.setCustomInfo("frmDateRange", data);
          var today = new Date();
          var previousDate = new Date(today.getTime() - (30 * 24 * 60 * 60 * 1000));
          var dateFrom = previousDate.getFullYear() + ('0' + (previousDate.getMonth() + 1)).slice(-2) + ('0' + previousDate.getDate()).slice(-2);
          var dateTo = today.getFullYear() + ('0' + (today.getMonth() + 1)).slice(-2) + ('0' + today.getDate()).slice(-2);
          var params ={
"portfolioId":scope_WealthPresentationController.portfolioId,
"accountId":scope_WealthPresentationController.accountNumber,
"dateFrom":dateFrom,
"dateTo":dateTo,
"listType":"SEARCH",
"sortBy":"bookingDate",
"searchByInstrumentName":""
}
      var wealthModule = applicationManager.getModulesPresentationController("WealthModule");
      wealthModule.getAccountActivity(params);
        },
        moreOptions: function() {
            this.view.flxAdditionalOptions.setVisibility(true);
        },
        onCancel: function() {
            this.view.flxAdditionalOptions.setVisibility(false);
        },
        getNewGraphData: function(filterParam) {
            var inputParams = {
                "portfolioId": scope_WealthPresentationController.portfolioId,
                "navPage": "Portfolio",
                "graphDuration": filterParam
            };
            var wealthModule = applicationManager.getModulesPresentationController("WealthModule");
            wealthModule.getPortfolioAndGraphDetails(inputParams);
        },
        bindNewGraphData: function(obj) {
            var responseObj = obj.response;
            var graphData = [];
            var temp = [];
            var temp = responseObj[responseObj.graphDuration];
            graphData = temp;
            //     for(var item in graphData){
            //       this.xaxisArr.push(item);
            //       this.plotDataArr.push(graphData[item]); 
            //     }
            // this.view.investmentLineChart.setChartData(this.plotDataArr,this.xaxisArr,null,this.chartConfig);
            this.view.investmentLineChart.setChartData(graphData, null, null, null, "PORTFOLIO");
        },
        changeGraphData: function(filterParam, isCallRequired) {
            this.xaxisArr = [];
            this.plotDataArr = [];
            var responseObj;
            if (isCallRequired) {
                this.getNewGraphData(filterParam);
            } else {
                responseObj = this.portfolioData;
                var graphData = [];
                var temp = [];
                var temp = responseObj[responseObj.graphDuration];
                graphData = temp;
                //     for(var item in graphData){
                //       this.xaxisArr.push(item);
                //       this.plotDataArr.push(graphData[item]); 
                //     }
                // this.view.investmentLineChart.setChartData(this.plotDataArr,this.xaxisArr,null,this.chartConfig);
                this.view.investmentLineChart.setChartData(graphData, null, null, null, "PORTFOLIO");
            }

        },
        // Called when chart filter changed - Mapped in onFilterChange event on CHart Component
        chartFilters: {
            ONE_MONTH: '1M',
            ONE_YEAR: '1Y',
            FIVE_YEARS: '5Y',
            YTD: 'YTD',
        },
        clickConvertCurrency: function() {
            var wealthMod = applicationManager.getModulesPresentationController("WealthModule");
            var navManager = applicationManager.getNavigationManager();
            wealthMod.clearWealthData();
            data = {};
            data.cashAcc = this.cashAcc;
            navManager.setCustomInfo("frmSelectCurrency", data);
            scope_WealthPresentationController.currencyConv = true;
            scope_WealthPresentationController.toConv = true;
            scope_WealthPresentationController.currencyConvData = true;

            wealthMod.commonFunctionForNavigation("frmSelectCurrency");
        },
        onFilterChanged: function(filter) {
            var maxVal = 0;
            var filterMap = "";
            this.chartConfig = filter;
            if (filter === this.chartFilters.ONE_MONTH) {
                filterMap = "OneM";
                this.changeGraphData(filterMap, true);
            } else if (filter === this.chartFilters.ONE_YEAR) {
                filterMap = "OneY";
                this.changeGraphData(filterMap, true);
            } else if (filter === this.chartFilters.FIVE_YEARS) {
                filterMap = "FiveY";
                this.changeGraphData(filterMap, true);
            } else {
                filterMap = "YTD";
                this.changeGraphData(filterMap, true);
            }

        },
    };
});