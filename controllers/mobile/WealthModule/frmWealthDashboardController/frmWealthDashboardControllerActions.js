define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** init defined for frmWealthDashboard **/
    AS_Form_ebef21cc490d4e41b8b804261cdd4e94: function AS_Form_ebef21cc490d4e41b8b804261cdd4e94(eventobject) {
        var self = this;
        this.init();
    },
    /** preShow defined for frmWealthDashboard **/
    AS_Form_f0cbbecd39834baf967f73176e410df6: function AS_Form_f0cbbecd39834baf967f73176e410df6(eventobject) {
        var self = this;
        this.preShow();
    },
    /** postShow defined for frmWealthDashboard **/
    AS_Form_jdbd335e167e4639b4362a223c40364a: function AS_Form_jdbd335e167e4639b4362a223c40364a(eventobject) {
        var self = this;
        this.postShow();
    }
});