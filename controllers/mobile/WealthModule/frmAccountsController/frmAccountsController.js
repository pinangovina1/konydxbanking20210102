define({
    selectedData: [],
    accountData: "",
    dateRange: [],
    sortByCustomData: "",
    init: function() {
        var navManager = applicationManager.getNavigationManager();
        var currentForm = navManager.getCurrentForm();
        applicationManager.getPresentationFormUtility().initCommonActions(this, "YES", currentForm);

    },
    preShow: function() {
        if (applicationManager.getPresentationFormUtility().getDeviceName() !== "iPhone") {
            this.view.flxHeader.isVisible = true;
        } else {
            this.view.flxHeader.isVisible = false;
        }
        this.view.flxAdditionalOptions.setVisibility(false);
        var navManager = applicationManager.getNavigationManager();
        var searchFlag = navManager.getCustomInfo("frmPortfolioDetails");
        if (searchFlag == true) {
            this.view.tbxSearch.text = "";
            this.view.imgClose.isVisible = false;
        }
        navManager.setCustomInfo("frmPortfolioDetails", false);
        this.dateRange = navManager.getCustomInfo("frmDateRange");
        this.sortByCustomData = navManager.getCustomInfo("frmSortBy");
        
        this.initActions();
    },
    initActions: function() {
     //   this.setDataToSegment();
        this.view.customHeader.flxSearch.onTouchEnd = this.moreOptions;
        this.view.flxCancelOption.onTouchEnd = this.onCancel;
        this.view.flxSortBy.onTouchEnd = this.onClickSortBy;
        this.view.customHeader.flxBack.onClick = this.onBack;
        this.view.flxTimePeriod.onTouchEnd = this.timePeriod;
        this.setLblPreviousDays();
        this.view.segList.onRowClick = this.accountDetails;
        this.view.flxCashAccounts.onTouchEnd = this.cashAccounts;
        var navManager = applicationManager.getNavigationManager();
        this.accountData = navManager.getCustomInfo("frmCashAccounts");
        this.view.lblCash.text = this.accountData.response;
        this.view.tbxSearch.onDone = this.searchData;
        this.view.tbxSearch.onTextChange = this.onSearch;
        this.view.imgClose.onTouchEnd = this.clearText;
    },
      onSearch: function() {
        this.view.imgClose.isVisible = true;
        if (this.view.tbxSearch.text == "" || this.view.tbxSearch.text == null) {
            this.view.imgClose.isVisible = false;
        }
    },
    clearText: function() {
        this.view.tbxSearch.text = "";
        this.view.imgClose.isVisible = false;
        this.searchData();
    },
      setLblPreviousDays: function() {
        var forUtility = applicationManager.getFormatUtilManager();
        if (this.dateRange.selectedPeriod) {
            if (this.dateRange.selectedPeriod == "previous30DaysSelected") {
                this.view.lblPreviousDays.text = kony.i18n.getLocalizedString("i18n.wealth.previous30Days");
            } else if (this.dateRange.selectedPeriod == "3MonthsSelected") {
                this.view.lblPreviousDays.text = kony.i18n.getLocalizedString("i18n.wealth.threeMonths");
            } else if (this.dateRange.selectedPeriod == "6MonthsSelected") {
                this.view.lblPreviousDays.text = kony.i18n.getLocalizedString("i18n.wealth.sixMonths");
            } else if (this.dateRange.selectedPeriod == "lastYearSelected") {
                this.view.lblPreviousDays.text = kony.i18n.getLocalizedString("i18n.wealth.lastYear");
            } else {
              var startDate = this.dateFormat(this.dateRange.startDate);
              var startDateObj=startDate.split("-");
              var formattedstartDate=startDateObj[1] +"/"+ startDateObj[2] +"/"+ startDateObj[0];
              var endDate = this.dateFormat(this.dateRange.endDate);
              var endDateObj=endDate.split("-");
              var formattedendDate=endDateObj[1] +"/"+ endDateObj[2] +"/"+ endDateObj[0];
              this.view.lblPreviousDays.text = formattedstartDate + " - " + formattedendDate;
            }
        } 
    },
    postShow: function() {

    },
    cashAccounts: function() {
        var navManager = applicationManager.getNavigationManager();
        var data = {};
        data.searchText = this.view.tbxSearch.text;
        if (this.accountData.cashAccountName == undefined) {
            data.response = this.accountData.response;
            data.accountName = this.accountData.accountName;
            data.cashData = this.accountData.cashData;
            navManager.setCustomInfo("frmCashAccounts", data);
        } else {
            data.response = this.accountData.response;
            data.accountName = this.accountData.cashAccountName;
            data.cashData = this.accountData.cashData;
            navManager.setCustomInfo("frmCashAccounts", data);
        }
        navManager.navigateTo("frmCashAccounts");
    },
    moreOptions: function() {
        this.view.flxAdditionalOptions.setVisibility(true);
    },
    onCancel: function() {
        this.view.flxAdditionalOptions.setVisibility(false);
    },
      searchData: function() {
        var searchText = this.view.tbxSearch.text;
        this.view.segList.removeAll();
        var today = new Date();
        var endDate = today.getFullYear() + ('0' + (today.getMonth() + 1)).slice(-2) + ('0' + today.getDate()).slice(-2);
        var previousDate = new Date(today.getTime() - (30 * 24 * 60 * 60 * 1000));
        var startDate = previousDate.getFullYear() + ('0' + (previousDate.getMonth() + 1)).slice(-2) + ('0' + previousDate.getDate()).slice(-2);
        var sortVal = "bookingDate";
        if (this.sortByCustomData.response == undefined && this.dateRange.startDate == undefined) {
            this.dateRange.startDate = startDate;
            this.dateRange.endDate = endDate;
        } else if (this.sortByCustomData.response == undefined || this.dateRange.startDate == undefined) {
            if (this.dateRange.startDate) {
                startDate = this.dateRange.startDate;
                endDate = this.dateRange.endDate;
            } else {
                sortVal = this.sortByCustomData.response;
            }
        } else {
            startDate = this.dateRange.startDate;
            endDate = this.dateRange.endDate;
            sortVal = this.sortByCustomData.response;
        }
        var params = {
"portfolioId":scope_WealthPresentationController.portfolioId,
"accountId":scope_WealthPresentationController.accountNumber,
"dateFrom":startDate,
"dateTo":endDate,
"listType":"SEARCH",
"sortBy":sortVal,
"searchByInstrumentName":searchText
                }
        var wealthModule = applicationManager.getModulesPresentationController("WealthModule");
        wealthModule.getAccountActivity(params);
    },
    onClickSortBy: function() {
       var data = {};
        data.searchText = this.view.tbxSearch.text;
        var navManager = applicationManager.getNavigationManager();
        if (this.sortByCustomData.response == undefined) {
            data.sortByValue = "bookingDate";
            navManager.setCustomInfo("frmAccounts", data);
        } else {
            data.sortByValue = this.sortByCustomData.response;
            navManager.setCustomInfo("frmAccounts", data);
        }
        var navManager = applicationManager.getNavigationManager();
        navManager.navigateTo("frmSortBy");
    },
    onBack: function() {
        var navigationMan = applicationManager.getNavigationManager();
        navigationMan.goBack();
    },
    timePeriod: function() {
        var navManager = applicationManager.getNavigationManager();
        var dateFlag = navManager.getCustomInfo("frmDateRange");
      var selectedValue = this.view.lblPreviousDays.text;
      var dataSet = {};
      var period;
      if(selectedValue == "Previous 30 days"){
        period ="previous30DaysSelected";
      }
      else if(selectedValue == "3 Months"){
        period ="3MonthsSelected";
      }
      else if(selectedValue == "6 Months"){
        period ="6MonthsSelected";
      }
      else if(selectedValue == "Last year"){
        period ="lastYearSelected";
      }
      else{
        period ="freeDateSelected";
      }
        dataSet.searchValue = this.view.tbxSearch.text;
        dataSet.flag = dateFlag.flag;
        dataSet.selectedDays = period;
        navManager.setCustomInfo('frmAccounts', dataSet);
        navManager.navigateTo("frmDateRange");
    },
    setDataToSegment: function(accountsList) {
        var scope = this;
        var currForm = kony.application.getCurrentForm();
        var segData = [];
        var rowData = [];
      var data = accountsList.response.accountActivityList.body;
      if(data.length == 0){
        this.view.segList.setVisibility(false);
        this.view.flxError.setVisibility(true);
      }
      else{
        this.view.segList.setVisibility(true);
        this.view.flxError.setVisibility(false);
        for (var list in data) {
            var storeData;
            var values;
            var forUtility = applicationManager.getFormatUtilManager();
            var amount = forUtility.formatAmountandAppendCurrencySymbol(data[list].amount, data[list].currencyId);
            var balance = forUtility.formatAmountandAppendCurrencySymbol(data[list].balance, data[list].currencyId);
            var name = data[list].displayName;
            if (data[list].shortName != undefined || data[list].shortName != "") {
                name = data[list].displayName + " " + data[list].shortName;
            }
            if (name.length >= 30) {
                name = name.substr(0, 30) + "...";
            }
            var bookDate = this.dateFormat(data[list].bookingDate);
            var bookingDate = forUtility.getDateObjectfromString(bookDate);
            var valDate = this.dateFormat(data[list].valueDate);
            var valueDate = forUtility.getDateObjectfromString(valDate);
            var formattedBookingDate = forUtility.getFormatedDateString(bookingDate, forUtility.getApplicationDateFormat());
            var formattedValueDate = forUtility.getFormatedDateString(valueDate, forUtility.getApplicationDateFormat())
            storeData = {
                accountName: name,
                amount: amount,
                balance: balance,
                type: data[list].displayName,
                instrument: data[list].shortName,
                quantity: data[list].quantity,
                bookingDate: formattedBookingDate,
                valueDate: formattedValueDate
            }
            if (data[list].quantity != "" && data[list].shortName != "") {
                values = {
                    Change: amount,
                    "Account Balance": balance,
                    Type: data[list].displayName,
                    Instrument: data[list].shortName,
                    Quantity: data[list].quantity,
                    "Booking Date": formattedBookingDate,
                    "Value Date": formattedValueDate
                }
            } else if (data[list].quantity != "" || data[list].shortName != "") {
                if (data[list].quantity) {
                    values = {
                        Change: amount,
                        "Account Balance": balance,
                        Type: data[list].displayName,
                        Quantity: data[list].quantity,
                        "Booking Date": formattedBookingDate,
                        "Value Date": formattedValueDate
                    }
                } else {
                    values = {
                        Change: amount,
                        "Account Balance": balance,
                        Type: data[list].displayName,
                        Instrument: data[list].shortName,
                        "Booking Date": formattedBookingDate,
                        "Value Date": formattedValueDate
                    }
                }
            } else {
                values = {
                    Change: amount,
                    "Account Balance": balance,
                    Type: data[list].displayName,
                    "Booking Date": formattedBookingDate,
                    "Value Date": formattedValueDate
                }
            }
            rowData.push(values);
            segData.push(storeData);
        }
        this.view.segList.widgetDataMap = {
            lblName: "accountName",
            lblValue: "amount",
            lblDate: "bookingDate",
            lblAmountValue: "balance"
        }
        this.view.segList.setData(segData);
        this.selectedData = rowData;
        currForm.forceLayout();
    }
    },
  dateFormat: function(date){
    //yyyymmdd - yyyy-mm-dd
    var formattedDate = date.slice(0,4)+"-"+date.slice(4,6)+"-"+date.slice(6,8);
    return formattedDate;
  },
    accountDetails: function() {
        var rowIndex = this.view.segList.selectedRowIndex[1];
        var value = this.selectedData[rowIndex];
        var navManager = applicationManager.getNavigationManager();
        var data = {};
        data.details = value;
        navManager.setCustomInfo("frmActivityDetails", data);
        navManager.navigateTo("frmActivityDetails");
    }
});