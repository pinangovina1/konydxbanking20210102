define({

  init : function(){
    var navManager = applicationManager.getNavigationManager();
    var currentForm=navManager.getCurrentForm();
  //  this.view.flxShadowBox.shadowDepth = 20;
  //  this.view.flxPricingShadowBox.shadowDepth = 20;
  //  this.view.flxStockNewsShadowBox.shadowDepth = 20;
   // this.view.flxDocumentShadowBox.shadowDepth = 20;
    applicationManager.getPresentationFormUtility().initCommonActions(this,"YES",currentForm);
  },
  preShow : function(){
    if(applicationManager.getPresentationFormUtility().getDeviceName()==="iPhone"){
      this.view.flxHeader.setVisibility(false);
    }
     this.view.investmentLineChart.currentFilter='1D';
            
    //    this.initActions();
  },
  postShow:function(){
    this.initActions();
      this.setUiData();
        },
  setDocuments:function(data){
    var documentDetails=data.documentDetails;
    var results=[];
   /* var documentDetails=[{
      link:"",
      mediaType:"appplication/pdf",
      type:"AnnualReport"
    },{
      link:"",
      mediaType:"appplication/pdf",
      type:"SemiAnnualReport"
    }];*/
    for(var num in documentDetails){
      var storeData={
        pdfImage:"pdf.png",
        Name:documentDetails[num].type,
        downloadImage:"download.png",
        link:documentDetails[num].link
      }
      results.push(storeData);
    }
     this.view.segPdf.widgetDataMap = {
                imgPdf: "pdfImage",
                lblName: "Name",
                imgDownload: "downloadImage",
       			dummylabel:"link"
            };
            this.view.segPdf.setData(results);
  },
  setUiData:function(){
    var navManager=applicationManager.getNavigationManager();
   var data1=navManager.getCustomInfo('frmInstrumentDetails');
   		 navManager.setCustomInfo("frmPortfolioDetails", true);
   var data=data1.instrumentDetails;
   var currentposition=data1.response;
   this.setInstrumentDetailsTop(data,data.pricingDetails,currentposition);
    this.setCurrentPosition(currentposition,data.instrumentDetails);
  // this.setInstrumentDetails(data);
    this.setPricingData(data);
    this.setStockNews(data);
      this.setDocuments(data);
    var formatUtil=applicationManager.getFormatUtilManager();
   // if(scope_WealthPresentationController.instrumentChartFilter===""){
             // this.view.investmentLineChart.currentFilter='1D';
            
      //      }
    /*else{
              this.view.investmentLineChart.currentFilter=scope_WealthPresentationController.instrumentChartFilter;
            }*/
             if(scope_WealthPresentationController.instrumentDetailsEntry===true){
                 this.view.btnBuy.skin="sknBtnBgFFFFFFBorder1pxFont15px003E75";
     			 this.view.btnSell.skin="sknBtnBg003E75Border1pxFont15pxFFFFFF";
    
             }
    if(data.instrumentDetails)
    	this.view.investmentLineChart.currencySymbol = formatUtil.getCurrencySymbol(data.instrumentDetails.referenceCurrency);
  },
  setCurrentPosition:function(currentposition,data){
     var formatUtil=applicationManager.getFormatUtilManager();
        if (currentposition.unrealPLMkt === 0 || currentposition.unrealPLMkt === "0" || currentposition.unrealPLMkt === "0.00") {
      this.view.lblPnlVal.skin="sknlbl2f853ssp121pr";
    }else{
            if (currentposition.unrealPLMkt.substr(0, 1) === "-") {
      this.view.lblPnlVal.skin="sknlblee0005ssb121pr";
    }else{
      this.view.lblPnlVal.skin="sknlbl2f853ssp121pr";
    }
    }
   
    if(currentposition.holdingsId){
      this.view.flxCurrentposition.isVisible=true;
      this.view.lblmarketValue.text=formatUtil.formatAmountandAppendCurrencySymbol(currentposition.marketValue,data.referenceCurrency);
      this.view.lblPnlVal.text = formatUtil.formatAmountandAppendCurrencySymbol(currentposition.unrealPLMkt.substr(1), data.referenceCurrency);
            
     /*if (data.percentageChange.substr(0, 1) === "-") {
                this.view.lblPnlVal.text = "-"+ formatUtil.formatAmountandAppendCurrencySymbol(currentposition.unrealPLMkt.substr(1), data.referenceCurrency);
            } else {
                this.view.lblPnlVal.text = "+"+ formatUtil.formatAmountandAppendCurrencySymbol(currentposition.unrealPLMkt.substr(1), data.referenceCurrency);
            }*/
	this.view.lblQuantityVal.text=currentposition.quantity;
   
     this.view.lblAverageCostVal.text=formatUtil.formatAmountandAppendCurrencySymbol(currentposition.costPrice,data.referenceCurrency);  
      var value= parseFloat(currentposition.marketValue)/parseFloat(currentposition.totalValue);
      var value1=parseFloat(value*100).toFixed(1);
      this.view.lblWeightVal.text=value1+"%";
    }else{
       //this.view.lblmarketValue.text=formatUtil.formatAmountandAppendCurrencySymbol("0.00",data.referenceCurrency);
     //this.view.lblPnlVal.text=formatUtil.formatAmountandAppendCurrencySymbol("0.00",data.referenceCurrency);
     //this.view.lblQuantityVal.text=0;
    //this.view.lblWeightVal.text="0%"
    // this.view.lblAverageCostVal.text=formatUtil.formatAmountandAppendCurrencySymbol("0.00",data.referenceCurrency);  
    this.view.flxCurrentposition.isVisible=false;
    }
  },
  setInstrumentDetailsTop:function(data,data1,data2){
     var formatUtil=applicationManager.getFormatUtilManager();
    var instrumentDetails=data.instrumentDetails;
   // this.view.lblInstrumentName.text=instrumentDetails.instrumentName;
    this.view.lblInstrumentName.text=data2.description;
    this.view.lblInstrumentSymbol.text=instrumentDetails.ISINCode+" | "+instrumentDetails.exchange;
    if(instrumentDetails.lastPrice===0){
       this.view.lblInstrumentValue.text=formatUtil.formatAmountandAppendCurrencySymbol(data1.closeRate,instrumentDetails.referenceCurrency);
   
    }else{
       this.view.lblInstrumentValue.text=formatUtil.formatAmountandAppendCurrencySymbol(instrumentDetails.lastPrice,instrumentDetails.referenceCurrency);
   
    }
   if(instrumentDetails.percentageChange===0||instrumentDetails.percentageChange==="0"||instrumentDetails.percentageChange==="0.00"){
     this.view.imgIndicator.isVisible=false;
      this.view.lblCurrentValue.text=instrumentDetails.netchange+"("+instrumentDetails.percentageChange+"%"+")";
   
   }else{
     this.view.imgIndicator.isVisible=true;
      if(instrumentDetails.percentageChange.substr(0,1)==="-"){
      this.view.imgIndicator.src="whitearrowdownfilled.png";
       this.view.lblCurrentValue.text=instrumentDetails.netchange.substr(1)+"("+instrumentDetails.percentageChange+"%"+")";
   
    }else{
      this.view.imgIndicator.src="whitearrowupfilled.png";
       if(instrumentDetails.percentageChange.substr(0,1)==="+"){
          this.view.lblCurrentValue.text=instrumentDetails.netchange+"("+instrumentDetails.percentageChange+"%"+")";
   
       }else{
          this.view.lblCurrentValue.text=instrumentDetails.netchange+"(+"+instrumentDetails.percentageChange+"%"+")";
   
       }
      
    }
   
   }
    this.setDate(instrumentDetails);
    
  },
  setDate:function(instrumentDetails){
    var date = new Date();
     var hours ;
    var minutes;
    if(instrumentDetails.dateReceived){
      var time= instrumentDetails.timeReceived.split(":");
      hours=time[0];
      minutes=time[1];
    }else{
      hours = date.getHours();
    minutes = date.getMinutes();
      minutes = minutes < 10 ? '0'+minutes : minutes;
    }
   
  var ampm = hours >= 12 ? 'PM' : 'AM';
  hours = hours % 12;
  hours = hours ? hours : 12; // the hour '0' should be '12'
  
  var strTime = hours + ':' + minutes + ' ' + ampm;
    
    var shortMonths = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    var month=shortMonths[date.getMonth()];
  //return strTime;
    if(instrumentDetails.dateReceived){
      
      var date=instrumentDetails.dateReceived.split(" ");
      
      this.view.lblInstrumentValueTime.text="As of "+strTime+" "+date[1]+" "+date[0];
  
    }else{
      this.view.lblInstrumentValueTime.text="As of "+strTime+" "+month+" "+String(date.getDate()).padStart(2, '0') 
  
    }
    },
    setPricingData: function(data) {
        var pricingData = data.pricingDetails;
        var formatUtil = applicationManager.getFormatUtilManager();
        this.view.lblBidVal.text = formatUtil.formatAmountandAppendCurrencySymbol(pricingData.bidRate, pricingData.referenceCurrency);
      if(pricingData.bidVolume!==undefined){
        this.view.lblBidVolVal.text = pricingData.bidVolume.toString();
      }else{
        this.view.lblBidVolVal.text = pricingData.bidVolume;
      }  
      if(pricingData.volume!==undefined){
        this.view.lblVolVal.text = pricingData.volume.toString();
        
      }else{
        this.view.lblVolVal.text = pricingData.volume;
        
      }
      if(pricingData.askVolume!==undefined){
        this.view.lblAskVolVal.text = pricingData.askVolume.toString();
        
      }else{
        this.view.lblAskVolVal.text = pricingData.askVolume;
        
      }
        this.view.lblAskVal.text = formatUtil.formatAmountandAppendCurrencySymbol(pricingData.askRate, pricingData.referenceCurrency);
    this.view.lblHighVal.text=formatUtil.formatAmountandAppendCurrencySymbol(pricingData.high52W,pricingData.referenceCurrency);
    this.view.lblOpenVal.text=formatUtil.formatAmountandAppendCurrencySymbol(pricingData.openRate,pricingData.referenceCurrency);
    this.view.lblCloseVal.text=formatUtil.formatAmountandAppendCurrencySymbol(pricingData.closeRate,pricingData.referenceCurrency);
    this.view.lblLowVal.text=formatUtil.formatAmountandAppendCurrencySymbol(pricingData.low52W,pricingData.referenceCurrency);
    this.view.lblLatestVal.text=formatUtil.formatAmountandAppendCurrencySymbol(pricingData.latestRate,pricingData.referenceCurrency);
  },
  setStockNews:function(data){
   var news=data.stockNews;
    if(news!==undefined){
      this.view.flxNews.isVisible=true;
    var results=[];
    for(var num in news){
      if(num==="0"||num==="1"){
      var data=news[num];
      var time=data.RT.split('T');
      var curDate = new Date().toLocaleDateString();
      var curHrs = new Date().getHours();
      var RT;
      if (time[0] !== curDate) {
          var date= time[0].split('-');
  		  RT = date[1]+'/'+date[2]+'/'+date[0];
      } 
      else {
         var hours = time[1].split(':')[0];
          if(curHrs >= hours){
            RT = curHrs - hours + ' ' + 'Hours ago';
          }
          else{
            RT = hours - curHrs + ' ' + 'Hours ago';
          }
      }
      var storeData={
        Headline:data.HT,
        Time: RT,
        Provider: data.PR
      };
      results.push(storeData);
    }
    }
    this.view.segNews.widgetDataMap={
      lblTitle:"Provider",
      lblTime:"Time",
      lblNews:"Headline"
    };
    this.view.segNews.setData(results);
    }else{
      this.view.flxNews.isVisible=false;
    }
  },
  onRefresh:function(){
    var wealthMod = applicationManager.getModulesPresentationController("WealthModule");
   // var isinCode=this.view.lblInstrumentSymbol.text.split("|")[0].trim();
     var navManager=applicationManager.getNavigationManager();
   var data1=navManager.getCustomInfo('frmInstrumentDetails');
   var riccode=data1.response.RICCode;
      var isin=data1.response.ISIN;
        var param = {
            "ISINCode": isin,
          "RICCode":riccode
        };
        wealthMod.getInstrumentDetails(param);
        this.onFilterChanged(this.view.investmentLineChart.currentFilter);
  },
  setDataOnRefresh:function(){
     var navManager=applicationManager.getNavigationManager();
   var data1=navManager.getCustomInfo('frmInstrumentDetails');
   var data=data1.instrumentDetails;
   var currentposition=data1.response;
   this.setInstrumentDetailsTop(data,data.pricingData,currentposition);
    this.setCurrentPosition(currentposition,data.instrumentDetails);
    this.setPricingData(data);
    this.setStockNews(data);
		this.setDocuments(data);
    
  },
  
  initActions: function(){
    let filterValues = Object.keys(this.chartFilters).map(key => this.chartFilters[key]);
    this.view.investmentLineChart.setChartFilters(filterValues);
  //  this.setInstrumentDetails(this.mockData);
    this.view.imgRefresh.onTouchEnd=this.onRefresh;
    this.view.customHeader.flxBack.onClick =this.backOnClick;
this.view.flxView.onClick = this.viewAllClick;
    this.view.btnBuy.onClick=this.onPlaceorder.bind(this,0);
    this.view.btnSell.onClick=this.onPlaceorder.bind(this,1);
      this.view.segPdf.onRowClick=this.downloadPDF;

    },
  downloadPDF:function(){
     var selectedRowIndex = this.view.segPdf.selectedRowIndex[1];
        var selectedRow = this.view.segPdf.data[selectedRowIndex];
       
    kony.application.openURLAsync({
      url: selectedRow.link
    });
  },
  onPlaceorder:function(option){
     var navMan = applicationManager.getNavigationManager();
     var Data = navMan.getCustomInfo("frmPlaceOrder");
    if(kony.sdk.isNullOrUndefined(Data)){
      var Data={};
      Data.buy=!option;
    }else{
      Data.buy=!option;
    }
    if(Data.buy===true){
      this.view.btnBuy.skin="sknBtnBgFFFFFFBorder1pxFont15px003E75";
      this.view.btnBuy.focusSkin="sknBtnBgFFFFFFBorder1pxFont15px003E75";
      this.view.btnSell.skin="sknBtnBg003E75Border1pxFont15pxFFFFFF";
    }else{
       this.view.btnBuy.skin="sknBtnBg003E75Border1pxFont15pxFFFFFF";
      this.view.btnSell.focusSkin="sknBtnBgFFFFFFBorder1pxFont15px003E75";
      this.view.btnSell.skin="sknBtnBgFFFFFFBorder1pxFont15px003E75";
    
    }
     applicationManager.getPresentationUtility().showLoadingScreen();
     navMan.setCustomInfo("frmPlaceOrder", Data);
    navMan.navigateTo('frmPlaceOrder'); 
  },
  backOnClick:function(){
    
    var navigationMan = applicationManager.getNavigationManager();
     var wealthModule = applicationManager.getModulesPresentationController("WealthModule");
	
    if(scope_WealthPresentationController.searchEntryPoint===true){
        var params = {"portfolioId":scope_WealthPresentationController.portfolioId,"navPage":"Portfolio","graphDuration":"OneY"};
	 	  wealthModule.getPortfolioAndGraphDetails(params);
    }else{
     var params = {
      "portfolioId":scope_WealthPresentationController.portfolioId,
      "navPage":"Holdings",
      "sortBy":"description",
      "searchByInstrumentName":""
    }
    wealthModule.getHoldings(params);
    }
  },
viewAllClick:function(){
    var navigationMan = applicationManager.getNavigationManager();
		navigationMan.navigateTo("frmTopNews");
  },
  // Called when chart filter changed - Mapped in onFilterChange event on CHart Component
  onFilterChanged : function (filter) {
    if(scope_WealthPresentationController.instrumentDetailsEntry===true){
      scope_WealthPresentationController.instrumentDetailsEntry=false;
      this.view.investmentLineChart.currentFilter='1D';
            
      this.setChartData();
           this.setUiData();
    }else{
     scope_WealthPresentationController.instrumentChartFilter=filter;
    var wealthMod = applicationManager.getModulesPresentationController("WealthModule");
    var navManager=applicationManager.getNavigationManager();
    var data1=navManager.getCustomInfo('frmInstrumentDetails');
    var data=data1.instrumentDetails;
    wealthMod.getHistoricalInstrumentData(data1.response.RICCode,filter);}
  },

  setChartData : function() {
    var navManager=applicationManager.getNavigationManager();
       var data1 = navManager.getCustomInfo("frmInstrumentDetails");
    
    this.view.investmentLineChart.setChartData(data1.chartData,null,null,null,"CURRENCY");
  },
  
  setFilterData : function () {
     var XaxisArray = [];
    var YaxisArray = [];
    var data = [];
    var maxVal = 0;
    if (filter === this.chartFilters.ONE_DAY) {
      XaxisArray = ['9 AM', '10 AM', '11 AM', '12 PM', '1 PM', '2 PM'];
      YaxisArray = [10, 30, 50, 50, 60, 100, 80];
      data = [10, 30, 50, 50, 60, 100, 80];
      maxVal = Math.max.apply(null,data)+20;
    }
    else if (filter === this.chartFilters.ONE_YEAR) {
      XaxisArray = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
      YaxisArray = [0, 500, 1000, 1500, 2000, 2500, 5000];
      data = [0, 1000, 500, 2000, 1000, 5000, 2000, 2500, 1500, 2000, 1000, 500];
      maxVal = Math.max.apply(null,data)+200;
    }
    else if (filter === this.chartFilters.ONE_MONTH) {
      XaxisArray = ['5', '10', '15', '20', '25', '30'];
      YaxisArray = [0, 10, 20, 50, 60, 100, 120];
      data = [10, 40, 69, 90, 5, 120];
      maxVal = Math.max.apply(null,data)+20;
    }
    else if (filter === this.chartFilters.YTD) {
    } 
    else {
      return;
    }
    this.view.investmentLineChart.setChartData(data,XaxisArray,null,this.chartConfig);
  },

  chartFilters: {
    ONE_DAY:'1D',
    ONE_MONTH:'1M',
    ONE_YEAR:'1Y',
    YTD:'YTD',    
  },

  mockData : {
    instrumentName : 'Amazon.com Inc.',
    ISINCode : 'USAMAZON2345',
    exchange : 'NASDAQ',
    lastPrice : '2399.44',
    netchange : '36.51',
    percentageChange: '+1.55',
    referenceCurrency: 'USD',
  },

  setInstrumentDetails : function(data) {
    let formatUtil=applicationManager.getFormatUtilManager();
	let instrumentDetails=data.instrumentDetails;
    
    this.view.lblInstrumentName.text = instrumentDetails.instrumentName;
    this.view.lblInstrumentSymbol.text = instrumentDetails.ISINCode + " | "+instrumentDetails.exchange;
    this.view.lblInstrumentValue.text = instrumentDetails.referenceCurrency+' '+formatUtil.formatAmount(parseFloat(instrumentDetails.lastPrice));
    this.view.lblCurrentValue.text = instrumentDetails.netchange + '('+ instrumentDetails.percentageChange +')';

    let date = new Date();
    let options = { year: 'numeric', month: 'short', day: 'numeric',hour:'numeric',minute:'numeric'};
    let dateString = date.toLocaleDateString('en-US', options);
    this.view.lblInstrumentValueTime.text = 'As of '+dateString;
  }

});