define({
    sortingData: [],
    segData: [],
    dateRange: [],
    selectedRow: "",
    customData: "",
    init: function() {
        this.view.preShow = this.preShow;
        var scope = this;
        var navManager = applicationManager.getNavigationManager();
        var currentForm = navManager.getCurrentForm();
        applicationManager.getPresentationFormUtility().initCommonActions(this, "YES", currentForm);
    },
    initActions: function() {
        this.view.btnApply.onClick = this.onApplyClick;
        this.view.btnReset.onClick = this.onResetClick;
        this.view.customHeader.flxBack.onClick = this.flxBackOnClick;
        this.view.segSortingValues.onRowClick = this.onValueSelect;
    },
    onValueSelect: function() {
        var rowIndex = this.view.segSortingValues.selectedRowIndex[1];
        this.sortingData = this.view.segSortingValues.data;
        this.selectedRow = rowIndex;
        this.sortingData.forEach(function(e) {
            e.isSelected = false;
            e.sortName.skin = "sknlbl727272SSP17px";
            e.imageDetails.isVisible = false
        });
        this.sortingData[rowIndex].isSelected = true;
        this.sortingData[rowIndex].sortName = {
            "skin": "sknLbl4176A4SSPReg26px",
            "text": this.segData[rowIndex].sortName
        };
        this.sortingData[rowIndex].imageDetails = {
            "src": "correct.png",
            "isVisible": true
        };
        this.view.segSortingValues.setData(this.sortingData);
    },
    flxBackOnClick: function() {
        var navMan = applicationManager.getNavigationManager();
        navMan.goBack();
    },
    preShow: function() {
        if (applicationManager.getPresentationFormUtility().getDeviceName() !== "iPhone") {
            this.view.flxHeader.isVisible = true;
        } else {
            this.view.flxHeader.isVisible = false;
        }
        var navManager = applicationManager.getNavigationManager();
        var prevForm = navManager.getPreviousForm();
        if (prevForm === "frmHoldings") {
            this.customData = navManager.getCustomInfo("frmHoldings");
        } else if (prevForm === "frmTransactions") {
            this.customData = navManager.getCustomInfo("frmTransactions");
        }
      else if(prevForm === "frmAccounts") {
            this.customData = navManager.getCustomInfo("frmAccounts");
        }
      else if(prevForm === "frmOrder") {
            this.customData = navManager.getCustomInfo("frmOrder");
        }
        this.dateRange = navManager.getCustomInfo("frmDateRange");
        this.initializeData();
        this.setUpData();
        this.initActions();
    },
    initializeData: function() {
        var navManager = applicationManager.getNavigationManager();
        var prevForm = navManager.getPreviousForm();
        if (prevForm === "frmHoldings") {
            this.segData = [{
                    "sortName": "Instrument Alphabetically",
                    "sortIndex": "description",
                    "isSelected": false
                },
                {
                    "sortName": "Quantity",
                    "sortIndex": "quantity",
                    "isSelected": false
                },
                {
                    "sortName": "Price",
                    "sortIndex": "marketPrice",
                    "isSelected": false
                },
                {
                    "sortName": "Average Cost",
                    "sortIndex": "costPrice",
                    "isSelected": false
                },
                {
                    "sortName": "Market Value",
                    "sortIndex": "marketValue",
                    "isSelected": false
                },
                {
                    "sortName": "Unrealized P&L",
                    "sortIndex": "unrealPLMkt",
                    "isSelected": false
                }
            ];

          } else if (prevForm === "frmOrder") {
            this.segData = [{
                    "sortName": "Instrument Alphabetically",
                    "sortIndex": "description",
                    "isSelected": false
                },
                {
                    "sortName": "Date",
                    "sortIndex": "tradeDate",
                    "isSelected": false
                },
                {
                    "sortName": "Type",
                    "sortIndex": "orderType",
                    "isSelected": false
                },
                {
                    "sortName": "Quantity",
                    "sortIndex": "quantity",
                    "isSelected": false
                },
                {
                    "sortName": "Limit Price",
                    "sortIndex": "limitPrice",
                    "isSelected": false
                },
                {
                    "sortName": "Price",
                    "sortIndex": "price",
                    "isSelected": false
                },
                {
                    "sortName": "Status",
                    "sortIndex": "status",
                    "isSelected": false
                }
            ];

        } else if (prevForm === "frmAccounts") {
            this.segData = [{
                    "sortName": "Booking Date",
                    "sortIndex": "bookingDate",
                    "isSelected": false
                },
                {
                    "sortName": "Type",
                    "sortIndex": "displayName",
                    "isSelected": false
                },
                {
                    "sortName": "Instrument Alphabetically",
                    "sortIndex": "shortName",
                    "isSelected": false
                },
                {
                    "sortName": "Change",
                    "sortIndex": "amount",
                    "isSelected": false
                },
                {
                    "sortName": "Value Date",
                    "sortIndex": "valueDate",
                    "isSelected": false
                },
                {
                    "sortName": "Account Balance",
                    "sortIndex": "balance",
                    "isSelected": false
                }
            ];
        } else {
            this.segData = [{
                    "sortName": "Instrument Alphabetically",
                    "sortIndex": "description",
                    "isSelected": false
                },
                {
                    "sortName": "Type",
                    "sortIndex": "orderType",
                    "isSelected": false
                },
                {
                    "sortName": "Quantity",
                    "sortIndex": "quantity",
                    "isSelected": false
                },
                {
                    "sortName": "Price",
                    "sortIndex": "limitPrice",
                    "isSelected": false
                },
                {
                    "sortName": "Net Amount",
                    "sortIndex": "netAmount",
                    "isSelected": false
                },
                {
                    "sortName": "Trade Date",
                    "sortIndex": "tradeDate",
                    "isSelected": false
                }
            ];
        }
    },
    setUpData: function() {
        var selectedColumn = "";
        if (this.customData.sortByValue == undefined || this.customData.sortByValue == "") {
            var navManager = applicationManager.getNavigationManager();
            var prevForm = navManager.getPreviousForm();
            if (prevForm == "frmHoldings") {
                selectedColumn = "description";
                this.selectedRow = 0;
            } else if (prevForm == "frmTransactions" || prevForm == "frmOrder") {
                selectedColumn = "tradeDate";
                this.selectedRow = 5;
            } else {
                selectedColumn = "bookingDate";
                this.selectedRow = 0;
            }
        } else {
            selectedColumn = this.customData.sortByValue;
        }
        this.segData.forEach(function(e) {
            if (e.sortIndex === selectedColumn)
                e.isSelected = true;
            else e.isSelected = false;
        });
        this.sortingData = [];
        this.loadSegment();
    },
    loadSegment: function() {
        var data = [];
        data = this.segData;
        for (var list in data) {
            var storeData;
            if (data[list].isSelected) {
                storeData = {
                    isSelected: true,
                    sortName: {
                        text: data[list].sortName,
                        skin: "sknLbl4176A4SSPReg26px"
                    },
                    imageDetails: {
                        src: "correct.png",
                        isVisible: true
                    },
                    sortIndex: data[list].sortIndex
                }
            } else {
                storeData = {
                    isSelected: false,
                    sortName: {
                        text: data[list].sortName,
                        skin: "sknlbl727272SSP17px"
                    },
                    imageDetails: {
                        isVisible: false
                    },
                    sortIndex: data[list].sortIndex
                }
            }
            this.sortingData.push(storeData);
        }
        this.view.segSortingValues.widgetDataMap = {
            lblSortFactor: "sortName",
            imgTick: "imageDetails"
        }
        this.view.segSortingValues.removeAll();
        this.view.segSortingValues.setData(this.sortingData);
    },
    onApplyClick: function() {
        var navManager = applicationManager.getNavigationManager();
        var prevForm = navManager.getPreviousForm();
        if (prevForm === "frmHoldings") {
            var isSelected = this.sortingData[this.selectedRow].isSelected;
            var sortByData = "";
            if (isSelected) {
                sortByData = this.sortingData[this.selectedRow].sortIndex;
            }
            var params = {
                "portfolioId": scope_WealthPresentationController.portfolioId,
                "navPage": "Holdings",
                "sortBy": sortByData,
                "searchByInstrumentName": this.customData.searchText
            }
            var data = {};
            data.response = sortByData;
            data.searchText = this.customData.searchText;
            navManager.setCustomInfo("frmSortBy", data);
            var wealthModule = applicationManager.getModulesPresentationController("WealthModule");
            wealthModule.getHoldings(params);
        } else if(prevForm === "frmOrder") {
          
          var isSelected = this.sortingData[this.selectedRow].isSelected;
            var sortByData = "";
            if (isSelected) {
                sortByData = this.sortingData[this.selectedRow].sortIndex;
            }
            var orderId; 
            if(scope_WealthPresentationController.orderList.length === 0){
                orderId = null;
            }
            else{
                orderId = scope_WealthPresentationController.orderList.toString();
            }
        var today = new Date();
        var previousDate = new Date(today.getTime() - (30 * 24 * 60 * 60 * 1000));
        var startDate = previousDate.getFullYear() + '-' + ('0' + (previousDate.getMonth() + 1)).slice(-2) + '-' + ('0' + previousDate.getDate()).slice(-2);
        var endDate = today.getFullYear() + '-' + ('0' + (today.getMonth() + 1)).slice(-2) + '-' + ('0' + today.getDate()).slice(-2);
        
          if(scope_WealthPresentationController.ordType === "open") {
            var params = {
                "portfolioId":scope_WealthPresentationController.portfolioId,
				//"navPage": "Orders",
                "sortBy": sortByData,
                "searchByInstrumentName":this.customData.searchText,
                "orderId": orderId,
                "type": scope_WealthPresentationController.ordType,
				"startDate":"2018-12-06",
				"endDate": endDate
                }
          }
          else{
            if (this.dateRange.startDate == undefined) {  
            var params = {
                "portfolioId": scope_WealthPresentationController.portfolioId,
                //"navPage": "Orders",
                "sortBy": sortByData,
                "searchByInstrumentName": this.customData.searchText,
              	"orderId": orderId,
                "type": scope_WealthPresentationController.ordType,
                "startDate": startDate,
                "endDate": endDate,
            }
          	} else {
                var params = {
                "portfolioId":scope_WealthPresentationController.portfolioId,
				//"navPage": "Orders",
                "sortBy": sortByData,
                "searchByInstrumentName":this.customData.searchText,
                "orderId": orderId,
                "type": scope_WealthPresentationController.ordType,
				"startDate":this.dateRange.startDate,
				"endDate":this.dateRange.endDate
                }
              }
            }
            var data = {};
            data.response = sortByData;
            data.searchText = this.customData.searchText;
            navManager.setCustomInfo("frmSortBy", data);
            var wealthModule = applicationManager.getModulesPresentationController("WealthModule");
            wealthModule.getOrdersDetails(params);
            wealthModule.commonFunctionForNavigation("frmOrder");
        }  
          else if (prevForm === "frmAccounts") {
			var today = new Date();
            var endDate = today.getFullYear() + ('0' + (today.getMonth() + 1)).slice(-2) + ('0' + today.getDate()).slice(-2);
            var previousDate = new Date(today.getTime() - (30 * 24 * 60 * 60 * 1000));
            var startDate = previousDate.getFullYear() + ('0' + (previousDate.getMonth() + 1)).slice(-2) + ('0' + previousDate.getDate()).slice(-2);
            var isSelected = this.sortingData[this.selectedRow].isSelected;
            var sortByData = "";
            if (isSelected) {
                sortByData = this.sortingData[this.selectedRow].sortIndex;
            }
            if (this.dateRange.startDate == undefined) {
              var params = {
				"portfolioId":scope_WealthPresentationController.portfolioId,
				"accountId":scope_WealthPresentationController.accountNumber,
				"dateFrom":startDate,
				"dateTo":endDate,
				"listType":"SEARCH",
				"sortBy":sortByData,
				"searchByInstrumentName":this.customData.searchText
                }
            } else {
                var params = {
                "portfolioId":scope_WealthPresentationController.portfolioId,
				"accountId":scope_WealthPresentationController.accountNumber,
				"dateFrom":this.dateRange.startDate,
				"dateTo":this.dateRange.endDate,
				"listType":"SEARCH",
				"sortBy":sortByData,
				"searchByInstrumentName":this.customData.searchText
                }
            }
            var data = {};
            data.response = sortByData;
            data.searchText = this.customData.searchText;
            navManager.setCustomInfo("frmSortBy", data);
            var wealthModule = applicationManager.getModulesPresentationController("WealthModule");
            wealthModule.getAccountActivity(params);
        } else {
            var today = new Date();
            var endDate = today.getFullYear() + '-' + ('0' + (today.getMonth() + 1)).slice(-2) + '-' + ('0' + today.getDate()).slice(-2);
            var previousDate = new Date(today.getTime() - (30 * 24 * 60 * 60 * 1000));
            var startDate = previousDate.getFullYear() + '-' + ('0' + (previousDate.getMonth() + 1)).slice(-2) + '-' + ('0' + previousDate.getDate()).slice(-2);
            var isSelected = this.sortingData[this.selectedRow].isSelected;
            var sortByData = "";
            if (isSelected) {
                sortByData = this.sortingData[this.selectedRow].sortIndex;
            }
            if (this.dateRange.startDate == undefined) {
                var params = {
                    "portfolioId": scope_WealthPresentationController.portfolioId,
                    "startDate": startDate,
                    "endDate": endDate,
                    "searchByInstrumentName": this.customData.searchText,
                    "sortBy": sortByData
                }
            } else {
                var params = {
                    "portfolioId": scope_WealthPresentationController.portfolioId,
                    "startDate": this.dateRange.startDate,
                    "endDate": this.dateRange.endDate,
                    "searchByInstrumentName": this.customData.searchText,
                    "sortBy": sortByData
                }
            }
            var data = {};
            data.response = sortByData;
            data.searchText = this.customData.searchText;
            navManager.setCustomInfo("frmSortBy", data);
            var wealthModule = applicationManager.getModulesPresentationController("WealthModule");
            wealthModule.getTransactions(params);
        }
    },
    onResetClick: function() {
        this.customData.sortByValue = "";
        this.setUpData();
    }
});