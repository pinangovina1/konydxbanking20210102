define({
	ricCode: "",
	isinCode: "",
	lastPrice: "",
	instrumentName: "",
	orderMode: "Buy",
	init: function () {
		var navManager = applicationManager.getNavigationManager();
		var currentForm = navManager.getCurrentForm();
		applicationManager.getPresentationFormUtility().initCommonActions(this, "YES", currentForm);
	},


	preShow: function () {
		if (applicationManager.getPresentationFormUtility().getDeviceName() === "iPhone") {
			this.view.flxHeader.setVisibility(false);
			this.view.flxMainContainer.top = "0dp";
		} else {
			this.view.flxHeader.setVisibility(true);
		}


	},

	postShow: function () {
		this.initActions();
		this.setUIData();
		applicationManager.getPresentationUtility().dismissLoadingScreen();

	},

	initActions: function () {
		this.view.customHeader.flxBack.onTouchEnd = this.onBack;
		this.view.customHeader.btnRight.onClick = this.cancelOnClick;
		this.view.btnContinue.onClick = this.confirmOnClick;
		this.view.lblTransferCash.onTouchEnd = this.onTransferCash;
		this.view.lblConvertCurrency.onTouchEnd = this.onConvertCurrency;
		this.view.btnTglBuy.onClick = this.onToggleConversionPreference.bind(this, 0);
		this.view.btnTglSell.onClick = this.onToggleConversionPreference.bind(this, 1);
		this.view.imgArrow.onTouchEnd = this.selectAccount;
		this.view.imgRefresh.onTouchEnd = this.onRefresh;

	},
	onRefresh: function () {
		var wealthMod = applicationManager.getModulesPresentationController("WealthModule");
		var param = {
			"ISINCode": this.isinCode,
			"RICCode": this.ricCode
		}
		wealthMod.getPlaceOrderDetails(param);
	},
	selectAccount: function () {
		var navMan = applicationManager.getNavigationManager();
		var placeorderdata = navMan.getCustomInfo('frmPlaceOrder');
		var ricCode = placeorderdata.ricCode;
		if (kony.sdk.isNullOrUndefined(ricCode)) {
			placeorderdata.RICCode = this.ricCode;
		}
		var params = {
			"customerId": "100777"
		};
		//  var wealthModule = applicationManager.getModulesPresentationController("WealthModule");
		//wealthModule.getPortfolioList(params);
		navMan.navigateTo('frmInvestmentAcc');
	},
	setCashBalance: function (data, currency) {
		var finalAmount;
		var finalCurrency;
		if (data.length === 1) {
			finalAmount = data[0].balance;
			finalCurrency = data[0].currency;
		} else {
			for (var num in data) {
				if (currency === data[num].currency) {
					finalAmount = data[num].balance;
					finalCurrency = data[num].currency;
					break;
				} else {
					finalAmount = data[0].balance;
					finalCurrency = data[0].currency;
				}
			}
		}
		var formatUtil = applicationManager.getFormatUtilManager();
		this.view.lblCashVal.text = formatUtil.formatAmountandAppendCurrencySymbol(finalAmount, finalCurrency);
		var navManager = applicationManager.getNavigationManager();
		var amount = navManager.getCustomInfo("frmInvestmentAcc");
		if (kony.sdk.isNullOrUndefined(amount)) {
			var amount = {};
			amount.finalAmount = finalAmount;
		} else {
			amount.finalAmount = finalAmount;
		}
		navManager.setCustomInfo('frmInvestmentAcc', amount);


	},
	setUIData: function () {
		var navMan = applicationManager.getNavigationManager();
		var formatUtil = applicationManager.getFormatUtilManager();
		var Data = navMan.getCustomInfo("frmPlaceOrder");
		var data1 = navMan.getCustomInfo("frmInstrumentDetails");
		var instrumentdetails = data1.instrumentDetails;
		var instrumentdetails1 = instrumentdetails.instrumentDetails;
		var currentposition = data1.response;
		this.instrumentName = currentposition.description;
		this.lastPrice = instrumentdetails.pricingDetails.closeRate;
		this.ricCode = currentposition.RICCode;
		this.isinCode = currentposition.ISIN;
		this.setDetails(instrumentdetails, false);
		var wealthMod = applicationManager.getModulesPresentationController("WealthModule");
		if (scope_WealthPresentationController.instrumentAcc == true) {
			var data = navMan.getCustomInfo("frmInvestmentAcc");
			var cashdata = data.cashData;
			if (kony.sdk.isNullOrUndefined(data.lblAccountName)) {
				this.view.lblAccount.text = wealthMod.getMaskedAccountName();
			} else {
				this.view.lblAccount.text = data.lblAccountName;
			}
			// this.view.lblCashVal.text = formatUtil.formatAmountandAppendCurrencySymbol(data.lblAccountBalValue, instrumentdetails1.referenceCurrency);
			//   this.view.lblCashVal.text = formatUtil.formatAmountandAppendCurrencySymbol(cashdata.totalCashBalance,cashdata.totalCashBalanceCurrency);
			//scope_WealthPresentationController.totalCashBalance=cashdata.totalCashBalance;
			this.setCashBalance(cashdata.cashAccounts, instrumentdetails1.referenceCurrency);
		} else {
			this.view.lblAccount.text = wealthMod.getMaskedAccountName();
			//Need to check the cashbalance variable
			// this.view.lblCashVal.text = formatUtil.formatAmountandAppendCurrencySymbol(scope_WealthPresentationController.totalCashBalance, scope_WealthPresentationController.totalCashBalanceCurrency);
			this.setCashBalance(scope_WealthPresentationController.portfolioCashAccounts, instrumentdetails1.referenceCurrency);
		}
		if (Data.buy === true) {
			this.onToggleConversionPreference(0);
		} else {
			this.onToggleConversionPreference(1);
		}
		if (scope_WealthPresentationController.instrumentAcc == true) {
			scope_WealthPresentationController.instrumentAcc = false;
			if ((scope_WealthPresentationController.currentPos != "") && (scope_WealthPresentationController.quantity != "")) {
				this.view.blCurrentPosVal.text = formatUtil.formatAmountandAppendCurrencySymbol(scope_WealthPresentationController.currentPos, instrumentdetails1.referenceCurrency) + " (" + scope_WealthPresentationController.quantity + ")";
				this.view.flxCurrentPosition.isVisible = true;
			} else {
				this.view.flxCurrentPosition.isVisible = false;
			}
		} else {
			if (currentposition.holdingsId) {
				this.view.blCurrentPosVal.text = formatUtil.formatAmountandAppendCurrencySymbol(currentposition.marketValue, instrumentdetails1.referenceCurrency) + " (" + currentposition.quantity + ")";
				this.view.flxCurrentPosition.isVisible = true;

			} else {
				this.view.flxCurrentPosition.isVisible = false;

			}
		}
	},
	setDetails: function (instrumentDetails1, refresh) {
		var instrumentDetails = instrumentDetails1.instrumentDetails;
		var formatUtil = applicationManager.getFormatUtilManager();
		//  this.view.lblInstrument.text=instrumentDetails.instrumentName;
		this.view.lblInstrument.text = this.instrumentName;
		this.view.lblExchange.text = instrumentDetails.ISINCode + " | " + instrumentDetails.exchange;
		/* if (scope_WealthPresentationController.instrumentAcc == true) {
		     if ((scope_WealthPresentationController.currentPos!= "") && (scope_WealthPresentationController.quantity != "")) {
		         this.view.lblAmount.text=formatUtil.formatAmountandAppendCurrencySymbol(instrumentDetails.lastPrice,instrumentDetails.referenceCurrency);
		          }
		     else{
		     this.view.lblAmount.text=formatUtil.formatAmountandAppendCurrencySymbol('0.00',instrumentDetails.referenceCurrency);  
		     }
		   }
		  else{
		  this.view.lblAmount.text=formatUtil.formatAmountandAppendCurrencySymbol(instrumentDetails.lastPrice,instrumentDetails.referenceCurrency);
		  }*/
		if (instrumentDetails.lastPrice === 0) {
			this.view.lblAmount.text = formatUtil.formatAmountandAppendCurrencySymbol(this.lastPrice, instrumentDetails.referenceCurrency);

		} else {

			this.view.lblAmount.text = formatUtil.formatAmountandAppendCurrencySymbol(instrumentDetails.lastPrice, instrumentDetails.referenceCurrency);
		}
		if (instrumentDetails.percentageChange === 0 || instrumentDetails.percentageChange === "0" || instrumentDetails.percentageChange === "0.00") {
			this.view.lblPL.skin = "sknlbl727272SSP17px";
			this.view.lblPL.text = instrumentDetails.netchange + "(" + instrumentDetails.percentageChange + "%" + ")";
		} else {
			if (instrumentDetails.percentageChange.substr(0, 1) === "-") {
				this.view.lblPL.skin = "sknIblEE0005SSPsb45px";
				if (instrumentDetails.netchange.substr(0, 1) === '-') {
					this.view.lblPL.text = instrumentDetails.netchange + "(" + instrumentDetails.percentageChange + "%" + ")";

				} else {
					this.view.lblPL.text = "-" + instrumentDetails.netchange + "(" + instrumentDetails.percentageChange + "%" + ")";

				}
			} else {
				this.view.lblPL.skin = "sknIbl2f8523SSPsb45px";
				if (instrumentDetails.percentageChange.substr(0, 1) === "+") {
					this.view.lblPL.text = "+" + instrumentDetails.netchange + "(" + instrumentDetails.percentageChange + "%" + ")";

				} else {
					this.view.lblPL.text = "+" + instrumentDetails.netchange + "(+" + instrumentDetails.percentageChange + "%" + ")";

				}

			}

		}
		//this.view.lblPL.text=instrumentDetails.percentageChange.substr(0,1)+instrumentDetails.netchange+"("+instrumentDetails.percentageChange+"%"+")";


		this.setDate(instrumentDetails);

	},
	setDate: function (instrumentDetails) {
		var date = new Date();
		var hours;
		var minutes;
		if (instrumentDetails.dateReceived) {
			var time = instrumentDetails.timeReceived.split(":");
			hours = time[0];
			minutes = time[1];
		} else {
			hours = date.getHours();
			minutes = date.getMinutes();
			minutes = minutes < 10 ? '0' + minutes : minutes;
		}

		var ampm = hours >= 12 ? 'PM' : 'AM';
		hours = hours % 12;
		hours = hours ? hours : 12; // the hour '0' should be '12'

		var strTime = hours + ':' + minutes + ' ' + ampm;

		var shortMonths = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
		var month = shortMonths[date.getMonth()];
		//return strTime;
		if (instrumentDetails.dateReceived) {

			var date = instrumentDetails.dateReceived.split(" ");

			this.view.lblTime.text = "As of " + strTime + " " + date[1] + " " + date[0];

		} else {
			this.view.lblTime.text = "As of " + strTime + " " + month + " " + String(date.getDate()).padStart(2, '0')

		}
	},

	onTransferCash: function () {

	},
	onConvertCurrency: function () {
		var navigationMan = applicationManager.getNavigationManager();
		navigationMan.navigateTo('frmSelectCurrency');
	},
	onToggleConversionPreference: function (option) {
		var navMan = applicationManager.getNavigationManager();

		var Data = navMan.getCustomInfo("frmPlaceOrder");

		let activeSkin = 'sknBtnFFFFFFBdr10px';
		let inactiveSkin = 'sknbtn000000SSPSemiBold15px';
		if (!option) {
			this.orderMode = 'Buy';
			this.view.btnTglBuy.skin = activeSkin;
			this.view.btnTglSell.skin = inactiveSkin;
			this.view.btnTglBuy.focusSkin = activeSkin;
			Data.buy = true;
		} else {
			this.orderMode = 'Sell';
			this.view.btnTglBuy.skin = inactiveSkin;
			this.view.btnTglSell.skin = activeSkin;
			this.view.btnTglSell.skin = activeSkin;
			Data.buy = false;
		}
		navMan.setCustomInfo("frmPlaceOrder", Data);

	},

	onBack: function () {

		var navigationMan = applicationManager.getNavigationManager();
		navigationMan.goBack();
	},

	cancelOnClick: function () {
		var params = {
			"portfolioId": scope_WealthPresentationController.portfolioId,
			"navPage": "Portfolio",
			"graphDuration": "OneY"
		};

		var wealthModule = applicationManager.getModulesPresentationController("WealthModule");

		wealthModule.getPortfolioAndGraphDetails(params);

	},


	confirmOnClick: function () {
		scope_WealthPresentationController.placeOrder = true;
		var wealthMod = applicationManager.getModulesPresentationController("WealthModule");
		var data = wealthMod.getWealthObject();
		var nav = applicationManager.getNavigationManager();
		applicationManager.getPresentationUtility().showLoadingScreen();

		nav.setCustomInfo("frmInstrumentOrder", {
			orderMode: this.orderMode
		});
		nav.navigateTo('frmInstrumentOrder');
		/*var param={
		  "buyCurrency":data.buyCurrency,
		  "sellCurrency":data.sellCurrency,
		  "buyAmount":data.buyAmount,
		  "sellAmount":data.sellAmount
		}
      
		wealthMod.createOrder(param);*/

	},
	bindGenericError: function (errorMsg) {
		applicationManager.getPresentationUtility().dismissLoadingScreen();
		var scopeObj = this;
		applicationManager.getDataProcessorUtility().showToastMessageError(scopeObj, errorMsg);
	}


});
