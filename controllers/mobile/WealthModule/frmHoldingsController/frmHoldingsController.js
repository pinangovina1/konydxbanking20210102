define({
    //segmentRowDataId : [],
    sortByCustomData: "",
    segResponse: {},
    totalValue: "",
    selectedRicCode: "",
  ISINCode:"",
    init: function() {
        this.view.preShow = this.preShow;
        var navManager = applicationManager.getNavigationManager();
        var currentForm = navManager.getCurrentForm();
        applicationManager.getPresentationFormUtility().initCommonActions(this, "YES", currentForm);
    },
    preShow: function() {
        if (applicationManager.getPresentationFormUtility().getDeviceName() !== "iPhone") {
            this.view.flxHeader.isVisible = true;
        } else {
            this.view.flxHeader.isVisible = false;
        }
       this.view.flxAdditionalOptions.setVisibility(false);
        var navManager = applicationManager.getNavigationManager();
        var searchFlag = navManager.getCustomInfo("frmPortfolioDetails");
        if (searchFlag == true) {
            this.view.tbxSearch.text = "";
            this.view.imgClose.isVisible = false;
        }
        navManager.setCustomInfo("frmPortfolioDetails", false);
        this.segResponse = navManager.getCustomInfo("frmHoldings");
        this.sortByCustomData = navManager.getCustomInfo("frmSortBy");
        this.formSegmentData(this.segResponse);
        this.initActions();
        this.view.flxAdditionalOptions.isVisible = false;
    },
    initActions: function() {
        this.view.flxMore.onTouchEnd = this.onClickMoreOptions;
        this.view.customHeader.flxBack.onTouchEnd = this.navigateCustomBack;
        this.view.tbxSearch.onDone = this.searchData;
        this.view.tbxSearch.onTextChange = this.onSearch;
        this.view.imgClose.onTouchEnd = this.clearText;
    },
    searchData: function() {
        var searchText = this.view.tbxSearch.text;
        this.view.segmentDetails.removeAll();
        if (this.sortByCustomData.response == undefined) {
            var params = {
                "portfolioId": scope_WealthPresentationController.portfolioId,
                "navPage": "Holdings",
                "sortBy": "description",
                "searchByInstrumentName": searchText
            }
        } else {
            var params = {
                "portfolioId": scope_WealthPresentationController.portfolioId,
                "navPage": "Holdings",
                "sortBy": this.sortByCustomData.response,
                "searchByInstrumentName": searchText
            }
        }
        var wealthModule = applicationManager.getModulesPresentationController("WealthModule");
        var holdingsList = wealthModule.getHoldings(params);
        var portfolioHolding = holdingsList.response.portfolioHoldings;
        if (portfolioHolding.length > 0) {
            this.view.segmentDetails.setVisibility(true);
            this.view.flxError.setVisibility(false);
            var navManager = applicationManager.getNavigationManager();
            this.segResponse = navManager.getCustomInfo("frmHoldings");
            this.formSegmentData(this.segResponse);
        } else {
            this.view.segmentDetails.setVisibility(true);
            this.view.flxError.setVisibility(false);
        }
    },
    onSearch: function() {
        this.view.imgClose.isVisible = true;
        if (this.view.tbxSearch.text == "" || this.view.tbxSearch.text == null) {
            this.view.imgClose.isVisible = false;
        }
    },
    clearText: function() {
        this.view.tbxSearch.text = "";
        this.view.imgClose.isVisible = false;
        this.searchData();
    },
    onClickMoreOptions: function() {
        this.setUpActionSheet("MoreOptions");
    },
    setUpActionSheet: function(triggerPoint) {
        if (triggerPoint === "Holdings") {
            this.view.flxAccounts.isVisible = true;
            this.view.lblPerformance.text = kony.i18n.getLocalizedString("i18n.wealth.view");
            this.view.lblAccounts.text = kony.i18n.getLocalizedString("i18n.wealth.buy");
            this.view.lblReport.text = kony.i18n.getLocalizedString("i18n.wealth.sell");
            this.view.flxPerformance.onTouchEnd = this.onClickView;
            this.view.flxAccounts.onTouchEnd = this.onClickBuy;
            this.view.flxReport.onTouchEnd = this.onClickSell;
            this.view.flxCancelOption.onTouchEnd = this.onClickHoldingsCancel;
        } else {
            this.view.flxAccounts.isVisible = false;
            this.view.lblPerformance.text = "Download Holdings";
            this.view.lblReport.text = kony.i18n.getLocalizedString("i18n.wealth.sortBy");
            this.view.flxPerformance.onTouchEnd = this.onClickDownloadTxns;
            this.view.flxReport.onTouchEnd = this.onClickSortBy;
            this.view.flxCancelOption.onTouchEnd = this.onClickCancel;
        }
        this.view.flxAdditionalOptions.isVisible = true;
    },
    onClickView: function() {
        scope_WealthPresentationController.instrumentDetailsEntry = true;
        this.callOnNavigate('view');
    },
    onClickBuy: function() {
        this.callOnNavigate('buy')
    },
    onClickSell: function() {
        this.callOnNavigate('sell');
    },
    callOnNavigate: function(selectedHoldings) {
        scope_WealthPresentationController.searchEntryPoint = false;
        var navManager = applicationManager.getNavigationManager();
        var ricId = this.selectedRicCode;
      var isin=this.ISINCode;
        var param = {
            "ISINCode": isin,
          "RICCode":ricId
        };
        var selData = {
            'selHoldings': selectedHoldings,
            'response': this.segResponse.response
        };
        navManager.setCustomInfo("frmHoldings", selData);
        var wealthModule = applicationManager.getModulesPresentationController("WealthModule");
        wealthModule.getInstrumentDetails(param);

    },
    onClickHoldingsCancel: function() {
        this.view.flxAdditionalOptions.isVisible = false;
    },
    onClickDownloadTxns: function() {
        var navManager = applicationManager.getNavigationManager();
        navManager.navigateTo("");
    },
    onClickCancel: function() {
        this.view.flxAdditionalOptions.isVisible = false;
    },
    navigateCustomBack: function() {
        var wealthModule = applicationManager.getModulesPresentationController("WealthModule");
        wealthModule.commonFunctionForgoBack();
    },
    onClickSortBy: function() {
        var data = {};
        data.searchText = this.view.tbxSearch.text;
        var navManager = applicationManager.getNavigationManager();
        if (this.sortByCustomData.response == undefined) {
            data.sortByValue = "description";
            navManager.setCustomInfo("frmHoldings", data);
        } else {
            data.sortByValue = this.sortByCustomData.response;
            navManager.setCustomInfo("frmHoldings", data);
        }
        var wealthModule = applicationManager.getModulesPresentationController("WealthModule");
        wealthModule.commonFunctionForNavigation("frmSortBy");

    },
    formSegmentData: function(value) {
        var scope = this;
        var currForm = kony.application.getCurrentForm();
        var val = value.response;
        var forUtility = applicationManager.getFormatUtilManager();
        var totalVal = forUtility.formatAmountandAppendCurrencySymbol(val.marketValue, val.referenceCurrency);
        this.view.lblTotalVal.text = totalVal;
        this.totalValue = val.marketValue;
        var unrealizedPL = forUtility.formatAmountandAppendCurrencySymbol(val.unRealizedPLAmount, val.referenceCurrency);
        var todaysPL = forUtility.formatAmountandAppendCurrencySymbol(val.todayPLAmount, val.referenceCurrency);
        if (val.unRealizedPL == "P") {
            this.view.lblUnrealizedPLValue.skin = "sknIWlbl2F8523SemiBold15px";
            this.view.lblUnrealizedPLValue.text = "+" + unrealizedPL + " (+" + val.unRealizedPLPercentage + "%)";
        } else {
            this.view.lblUnrealizedPLValue.skin = "sknIblEE0005SSPsb45px";
            this.view.lblUnrealizedPLValue.text = "-" + unrealizedPL + " (-" + val.unRealizedPLPercentage + "%)";
        }
        if (val.todayPL == "P") {
            this.view.lblTodayPLValue.skin = "sknIWlbl2F8523SemiBold15px";
            this.view.lblTodayPLValue.text = "+" + todaysPL + " (+" + val.todayPLPercentage + "%)";
        } else {
            this.view.lblTodayPLValue.skin = "sknIblEE0005SSPsb45px";
            this.view.lblTodayPLValue.text = "-" + todaysPL + " (-" + val.todayPLPercentage + "%)";
        }
        var data = val.portfolioHoldings;
        if (data.length == 0) {
            this.view.segmentDetails.setVisibility(false);
            this.view.flxError.setVisibility(true);
            //  this.view.segmentDetails.setVisibility(false);
        } else {
            this.view.segmentDetails.setVisibility(true);
            this.view.flxError.setVisibility(false);
            //this.view.segmentDetails.setVisibility(true);
            var segData = [];
            for (var list in data) {
                var storeData;
                var exchange = data[list].ISIN + ' | ' + data[list].holdingsType;
                var forUtility = applicationManager.getFormatUtilManager();
                var profitLoss = forUtility.formatAmountandAppendCurrencySymbol(data[list].unrealPLMkt.slice(1), data[list].secCCy);
                var latestPrice = forUtility.formatAmountandAppendCurrencySymbol(data[list].marketPrice, data[list].secCCy);
                var marketVal = forUtility.formatAmountandAppendCurrencySymbol(data[list].marketValue, data[list].secCCy);
                var avgCost = forUtility.formatAmountandAppendCurrencySymbol(data[list].costPrice, data[list].secCCy);
                if (data[list].unrealPLMkt.slice(0, 1) == "-") {
                    storeData = {
                        holdingName: data[list].description,
                        marketName: exchange,
                        lblProfitLoss: {
                            "skin": "sknIblEE0005SSPsb45px",
                            "text": "-" + profitLoss
                        },
                        latestPriceVal: latestPrice,
                        marketValue: marketVal,
                        quantity: data[list].quantity,
                        averageCost: avgCost,
                        //           imgMore: {"src": "more_detail.png" ,"onTouchEnd":function(){
                        // 						    scope.onHoldingSelect(data);
                        // 						}.bind(this)
                        //               },
                        flx: {
                            "onClick": function(event, context) {
                                scope.onHoldingSelect(event, context);
                            }.bind(this)
                        },
                        imgMore: {
                            "src": "more_detail.png"
                        },
                        latestPrice: kony.i18n.getLocalizedString("i18n.wealth.latestPrice"),
                        mktValue: kony.i18n.getLocalizedString("i18n.wealth.mktValue"),
                        qty: kony.i18n.getLocalizedString("i18n.wealth.qty"),
                        avgCost: kony.i18n.getLocalizedString("i18n.wealth.avgCost"),
                        pl: kony.i18n.getLocalizedString("i18n.wealth.pl")
                    }
                } else {
                    storeData = {
                        holdingName: data[list].description,
                        marketName: exchange,
                        lblProfitLoss: {
                            "skin": "sknIbl2f8523SSPsb45px",
                            "text": "+" + profitLoss
                        },
                        latestPriceVal: latestPrice,
                        marketValue: marketVal,
                        quantity: data[list].quantity,
                        averageCost: avgCost,
                        //           imgMore: {"src": "more_detail.png" , "onTouchEnd":function(){
                        // 						     scope.onHoldingSelect(data);
                        // 						}.bind(this)
                        //                },
                        imgMore: {
                            "src": "more_detail.png"
                        },
                        flx: {
                            "onClick": function(event, context) {
                                scope.onHoldingSelect(event, context);
                            }.bind(this)
                        },
                        latestPrice: kony.i18n.getLocalizedString("i18n.wealth.latestPrice"),
                        mktValue: kony.i18n.getLocalizedString("i18n.wealth.mktValue"),
                        qty: kony.i18n.getLocalizedString("i18n.wealth.qty"),
                        avgCost: kony.i18n.getLocalizedString("i18n.wealth.avgCost"),
                        pl: kony.i18n.getLocalizedString("i18n.wealth.pl")
                    }
                }
                segData.push(storeData);
            }
            this.view.segmentDetails.widgetDataMap = {
                // this.view.segmentDetails.widgetDataMap = {
                lblName: "holdingName",
                lblId: "marketName",
                lblOneValue: "latestPriceVal",
                lblOneKey: "latestPrice",
                lblTwoValue: "quantity",
                lblTwoKey: "qty",
                lblThreeValue: "averageCost",
                lblThreeKey: "avgCost",
                lblFourValue: "lblProfitLoss",
                lblFourKey: "pl",
                lblFiveValue: "marketValue",
                lblFiveKey: "mktValue",
                flxClick: "flx",
                imgChevron: "imgMore"
            }
            this.view.segmentDetails.setData(segData);
            //this.view.segmentDetails.setData(segData);
        }
        currForm.forceLayout();
    },
    onHoldingSelect: function(event, context) {
        var rowIndex = context.rowIndex;
        var data = {};
        var holdings = this.segResponse.response.portfolioHoldings[rowIndex];
        var id = holdings.ISIN;
        holdings.totalValue = this.totalValue;
        data.response = holdings;
        var navManager = applicationManager.getNavigationManager();
        navManager.setCustomInfo("frmInstrumentDetails", data);
        // this.segmentRowDataId = id;
        this.selectedRicCode = holdings.RICCode;
      this.ISINCode=holdings.ISIN;
        this.setUpActionSheet("Holdings");
    }
});