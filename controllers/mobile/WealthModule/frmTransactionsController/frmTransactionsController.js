define({ 
sortByCustomData : "",
segValue:{},
dateRange : [],
init : function(){
    var navManager = applicationManager.getNavigationManager();
    var currentForm=navManager.getCurrentForm();
    applicationManager.getPresentationFormUtility().initCommonActions(this,"YES",currentForm);
 this.view.lblPreviousDays.text=kony.i18n.getLocalizedString("i18n.wealth.previous30Days");
  },
  preShow:function(){
    if(applicationManager.getPresentationFormUtility().getDeviceName() !== "iPhone"){
      this.view.flxHeader.isVisible = true;
    }
    else{
      this.view.flxHeader.isVisible = false;
    }
   this.view.flxAdditionalOptions.setVisibility(false);
   var navManager=applicationManager.getNavigationManager();
   var searchFlag = navManager.getCustomInfo("frmPortfolioDetails");
    if(searchFlag == true){
      this.view.tbxSearch.text = "";
      this.view.imgClose.isVisible =  false;
    }
    navManager.setCustomInfo("frmPortfolioDetails", false);
   this.segValue = navManager.getCustomInfo("frmTransactions");
    this.sortByCustomData = navManager.getCustomInfo("frmSortBy");
    this.dateRange = navManager.getCustomInfo("frmDateRange");
   this.setDataToSegment(this.segValue);
    this.initActions();
  },
  initActions:function(){
    this.view.customHeader.flxSearch.onTouchEnd = this.moreOptions;
    this.view.flxCancelOption.onTouchEnd = this.onCancel;
    this.view.flxSortBy.onTouchEnd = this.onClickSortBy;
    this.view.flxDownloadTransactions.onTouchEnd = this.onClickDownloadTxns;
    this.view.customHeader.flxBack.onClick =this.onBack;
    this.view.flxTimePeriod.onTouchEnd = this.timePeriod;
    this.setLblPreviousDays();
    this.view.tbxSearch.onDone = this.searchData;
    this.view.tbxSearch.onTextChange=this.onSearch;
    this.view.imgClose.onTouchEnd = this.clearText;
  },
  postShow:function(){

  },
  setLblPreviousDays:function(){
     var forUtility = applicationManager.getFormatUtilManager();
      if(this.dateRange.selectedPeriod){
    if(this.dateRange.selectedPeriod=="previous30DaysSelected"){
      this.view.lblPreviousDays.text=kony.i18n.getLocalizedString("i18n.wealth.previous30Days");
    }else if(this.dateRange.selectedPeriod=="3MonthsSelected"){
      this.view.lblPreviousDays.text=kony.i18n.getLocalizedString("i18n.wealth.threeMonths");
    }else if(this.dateRange.selectedPeriod=="6MonthsSelected"){
       this.view.lblPreviousDays.text=kony.i18n.getLocalizedString("i18n.wealth.sixMonths");
    }else if(this.dateRange.selectedPeriod=="lastYearSelected"){
       this.view.lblPreviousDays.text=kony.i18n.getLocalizedString("i18n.wealth.lastYear");
    }else{
      var startDateObj=this.dateRange.startDate.split("-");
        var formattedstartDate=startDateObj[1] +"/"+ startDateObj[2] +"/"+ startDateObj[0];
       var endDateObj=this.dateRange.endDate.split("-");
         var formattedendDate=endDateObj[1] +"/"+ endDateObj[2] +"/"+ endDateObj[0];
      this.view.lblPreviousDays.text=formattedstartDate + " - " + formattedendDate;
    }
      }else{
         this.view.lblPreviousDays.text = kony.i18n.getLocalizedString("i18n.wealth.previous30Days"); 
      }
  },
  moreOptions:function(){
    this.view.flxAdditionalOptions.setVisibility(true);
  },
  onCancel:function(){
    this.view.flxAdditionalOptions.setVisibility(false);
  },
  searchData: function(){
    var searchText = this.view.tbxSearch.text;
    this.view.segmentDetails.removeAll();
    var today = new Date();
    var endDate = today.getFullYear() + '-' +('0' + (today.getMonth()+1)).slice(-2) + '-' + ('0' + today.getDate()).slice(-2) ;
    var previousDate= new Date(today.getTime() - (30 * 24 * 60 * 60 * 1000));
    var startDate= previousDate.getFullYear() + '-' + ('0' + (previousDate.getMonth()+1)).slice(-2) + '-' + ('0' + previousDate.getDate()).slice(-2);
    var sortVal = "tradeDate"; 
    if(this.sortByCustomData.response == undefined && this.dateRange.startDate == undefined){
      this.dateRange.startDate = startDate;
      this.dateRange.endDate = endDate;
    }
    else if(this.sortByCustomData.response == undefined || this.dateRange.startDate == undefined){
      if(this.dateRange.startDate){
        startDate = this.dateRange.startDate;
        endDate = this.dateRange.endDate;
      }
      else{
        sortVal = this.sortByCustomData.response;
      }
    }
    else{
      startDate = this.dateRange.startDate;
      endDate = this.dateRange.endDate;
      sortVal = this.sortByCustomData.response;
    }
     var params = {
      "portfolioId": scope_WealthPresentationController.portfolioId,
      "startDate":startDate,
      "endDate":endDate,
      "sortBy": sortVal,
      "searchByInstrumentName": searchText
    }
    var wealthModule = applicationManager.getModulesPresentationController("WealthModule");
    var transactionList = wealthModule.getTransactions(params);
    var transactions = transactionList.response.portfolioTransactions;
      if(transactions.length>0){
        this.view.segmentDetails.setVisibility(true);
        this.view.flxError.setVisibility(false);
        var navManager=applicationManager.getNavigationManager();
        var segValue = navManager.getCustomInfo("frmTransactions");
        this.setDataToSegment(segValue);
      }
    else{
      this.view.segmentDetails.setVisibility(true);
      this.view.flxError.setVisibility(false);
    }
  },
    onSearch:function(){  
     this.view.imgClose.isVisible =  true;
     if(this.view.tbxSearch.text==""||this.view.tbxSearch.text==null){
      		this.view.imgClose.isVisible =  false;
			}   
   },
    clearText: function(){
    this.view.tbxSearch.text = "";
    this.view.imgClose.isVisible =  false;
    this.searchData();
  },
  onClickSortBy: function(){
	   var data={};
    data.searchText= this.view.tbxSearch.text;
    var navManager = applicationManager.getNavigationManager();
    if(this.sortByCustomData.response == undefined){
      data.sortByValue="tradeDate";
      navManager.setCustomInfo("frmTransactions", data);
    }
    else{
      data.sortByValue=this.sortByCustomData.response;
      navManager.setCustomInfo("frmTransactions", data);
    }
    var navManager = applicationManager.getNavigationManager();
    navManager.navigateTo("frmSortBy");
	  
   },
    onBack : function () {
    var navigationMan=applicationManager.getNavigationManager();
    navigationMan.goBack();
  },
    timePeriod: function(){
      var navManager = applicationManager.getNavigationManager();
      var dateFlag = navManager.getCustomInfo("frmDateRange");
      var selectedValue = this.view.lblPreviousDays.text;
      var dataSet = {};
      var period;
      if(selectedValue == "Previous 30 days"){
        period ="previous30DaysSelected";
      }
      else if(selectedValue == "3 Months"){
        period ="3MonthsSelected";
      }
      else if(selectedValue == "6 Months"){
        period ="6MonthsSelected";
      }
      else if(selectedValue == "Last year"){
        period ="lastYearSelected";
      }
      else{
        period ="freeDateSelected";
      }
      dataSet.searchValue = this.view.tbxSearch.text;
      dataSet.flag = dateFlag.flag;
      dataSet.selectedDays = period ;
      navManager.setCustomInfo('frmTransactions', dataSet);
      navManager.navigateTo("frmDateRange");
  },

  setDataToSegment:function(value){
    var scope=this;
    var currForm = kony.application.getCurrentForm();
    var data=value.response.portfolioTransactions;
    if(data.length == 0){
      this.view.segmentDetails.setVisibility(false);
      this.view.flxError.setVisibility(true);
    }
    else{
      this.view.segmentDetails.setVisibility(true);
      this.view.flxError.setVisibility(false);
      var segData = [];
      for(var list in data){	
        var storeData;
        var exchange = data[list].ISIN + ' | ' + data[list].holdingsType;
        var forUtility = applicationManager.getFormatUtilManager();
        var formattedLimitPrice = forUtility.formatAmountandAppendCurrencySymbol(data[list].limitPrice, value.response.referenceCurrency);
        var formattedTotal=forUtility.formatAmountandAppendCurrencySymbol(data[list].total,data[list].instrumentCurrency);
        var tradeDateObj=forUtility.getDateObjectfromString(data[list].tradeDate);
        var formattedTradeDate=forUtility.getFormatedDateString(tradeDateObj, forUtility.getApplicationDateFormat());
        var 
        storeData = {
          transactionName: data[list].description,
          marketName: exchange,
          priceValue: formattedLimitPrice,
          totalValue: formattedTotal,
          quantityValue: data[list].quantity,
          typeValue: data[list].orderType,
          tradeDateValue: {"text": formattedTradeDate, "skin": "sknLbl424242SSPReg26px"},
          price: kony.i18n.getLocalizedString("i18n.wealth.price"),
          total: kony.i18n.getLocalizedString("i18n.wealth.total"),
          quantity: kony.i18n.getLocalizedString("i18n.wealth.qty"),
          type: kony.i18n.getLocalizedString("i18n.wealth.orderType"),
          tradeDate: kony.i18n.getLocalizedString("i18n.wealth.tradeDate"),
          imageDetails: {"isVisible": false},
          flx:{
          "onClick":function(event, context){
                             scope.onTransactionSelect(event,context);
                        }.bind(this)
          }
        };
        segData.push(storeData);
      }

      this.view.segmentDetails.widgetDataMap = {
        lblName: "transactionName",
        lblId: "marketName",
        lblOneValue: "tradeDateValue",
        lblOneKey: "tradeDate",
        lblTwoValue: "quantityValue",
        lblTwoKey: "quantity",
        lblThreeValue: "typeValue",
        lblThreeKey: "type",
        lblFourValue: "priceValue",
        lblFourKey: "price",
        lblFiveValue: "totalValue",
        lblFiveKey: "total",
        imgChevron: "imageDetails",
        flxMain: "flx"
      };		
      this.view.segmentDetails.setData(segData);
      currForm.forceLayout();
    }
  },
  onTransactionSelect:function(event,context){
     var navManager=applicationManager.getNavigationManager();
     var data={};
     var rowIndexValue=context.rowIndex;
    var transaction = this.segValue.response.portfolioTransactions[rowIndexValue];
     data.response=transaction;
     data.response.referenceCurrency =  this.segValue.response.referenceCurrency;
     navManager.setCustomInfo("frmViewTransactionDetails", data);
     navManager.navigateTo("frmViewTransactionDetails");
  },
    onClickDownloadTxns: function(){
    var navManager = applicationManager.getNavigationManager();
    navManager.navigateTo("");
  },

});