define({
  init: function () {
    try {
      this.view.preShow = this.preShow;
      this.view.postShow = this.postShow;
    } catch (e) {
      kony.print("Exception in init::" + e);
    }
  },

  preShow: function () {
    this.resetUI();
  },

  postShow: function () {
    this.setupOnClicks();
    this.view.forceLayout();
    this.getDataFromFrm();
    this.view.flxSpacer.setVisibility(true);
  },

  isiPhone: function () {
    return applicationManager.getPresentationFormUtility().getDeviceName() === "iPhone";
  },

  setupOnClicks: function () {
    this.view.customHeader.flxBack.onClick = this.dummyGoBack.bind(this);
    this.view.btnAll.onClick = this.removeAllSelectedFromSegement.bind(this);
    this.view.btnSelectTransactionType.onClick = this.showOrHideTransactionType.bind(this);

    this.view.btnSortBy.onClick = this.showOrHideSortBy.bind(this);
    this.view.btnApprovalDate.onClick = this.selectApprovalDateSortBy.bind(this);
    this.view.btnStatus.onClick = this.selectStatusSortBy.bind(this);
    this.view.btnTransactionType.onClick = this.selectTypeSortBy.bind(this);

    this.view.btnViewBy.onClick = this.showOrHideStatus.bind(this);
    this.view.btnViewByStatusAll.onClick = this.selectStatusAll.bind(this);
    this.view.btnApproved.onClick = this.selectApproved.bind(this);
    this.view.btnRejected.onClick = this.selectRejected.bind(this);
    this.view.btnPending.onClick = this.selectPending.bind(this);

    this.view.btnViewByDuration.onClick = this.showOrHideViewByDuration.bind(this);
    this.view.btnLastSixMonths.onClick = this.selectLastSixMonths.bind(this);
    this.view.btnToday.onClick = this.selectToday.bind(this);
    this.view.btnlastoneweek.onClick = this.selectLastOneWeek.bind(this);
    this.view.btnLastOneMonth.onClick = this.selectLastOneMonths.bind(this);
    this.view.btnLastOneYear.onClick = this.selectLastOneYear.bind(this);
    this.view.btnApplyFilter.onClick = this.sendData2Form.bind(this);

    this.view.segSingleTransactions.onRowClick = this.singleSegmentSelected.bind(this);
    this.view.segBulkTransactions.onRowClick = this.bulkSegmentSelected.bind(this);
    this.view.segOtherRequests.onRowClick = this.otherSegmentSelected.bind(this);
    this.view.btnApplyFilter.onClick = this.sendData2Frm.bind(this);
  },

  resetUI: function () {
    if (this.isiPhone()) {
      this.view.flxHeader.setVisibility(false);
    } else {
      this.view.flxHeader.setVisibility(true);
    }
    this.view.flxMain.setVisibility(true);

    this.view.flxSelectTransactionType.setVisibility(true);
    this.view.lblSelectTransactionTypeSeperator.setVisibility(true);
    this.view.lblSelectTransactionType.setVisibility(true);
    this.view.imgSelectTransactionType.setVisibility(true);
    this.view.imgSelectTransactionType.src = "arrowup.png";
    this.view.btnSelectTransactionType.setVisibility(true);

    this.view.flxSelectTransactionTypeS.setVisibility(true);
    this.view.flxAll.setVisibility(true);
    this.view.flxAllSeperator.setVisibility(true);
    this.view.imgAll.setVisibility(true);
    this.view.imgAll.src = "tickmark_green.png";
    this.view.flxAllText.setVisibility(true);
    this.view.lblAllCounter.setVisibility(false);
    this.view.btnAll.setVisibility(true);

    this.view.flxSortBy.setVisibility(true);
    this.view.flxSortBys.setVisibility(true);
    this.view.imgSortBy.src = "arrowup.png";
    this.removeAllTicksFromSortBy();

    this.view.flxViewByStatus.setVisibility(true);
    this.view.flxViewByStatuss.setVisibility(true);
    this.view.imgViewBy.src = "arrowup.png";

    this.view.flxViewByDuration.setVisibility(true);
    this.view.flxViewByDurations.setVisibility(true);
    this.view.btnApplyFilter.setVisibility(true);
    this.view.flxSpacer.setVisibility(true);

    this.selectStatusAll();
    this.selectLastOneYear();

    this.view.segSingleTransactions.widgetDataMap = {
      "flxApprovalsAndRequestFilter": "flxApprovalsAndRequestFilter",
      "flxText": "flxText",
      "imgSelect": "imgSelect",
      "lblCounter": "lblCounter",
      "lblSeperator": "lblSeperator",
      "lblText": "lblText"
    };
    this.view.segBulkTransactions.widgetDataMap = {
      "flxApprovalsAndRequestFilter": "flxApprovalsAndRequestFilter",
      "flxText": "flxText",
      "imgSelect": "imgSelect",
      "lblCounter": "lblCounter",
      "lblSeperator": "lblSeperator",
      "lblText": "lblText"
    };
    this.view.segOtherRequests.widgetDataMap = {
      "flxApprovalsAndRequestFilter": "flxApprovalsAndRequestFilter",
      "flxText": "flxText",
      "imgSelect": "imgSelect",
      "lblCounter": "lblCounter",
      "lblSeperator": "lblSeperator",
      "lblText": "lblText"
    };

    this.view.flxSpacer.setVisibility(true);

    var scope = this;
    var presenter = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule('ACHModule');
    this.selectedFilter = "";
    if(presenter.presentationController.isSingleApprovalsHistoryAvailable) {
      var segSingleData = presenter.presentationController.singleApprovalsHistory;
      var i = 0;
      for(i = 0; i < segSingleData.length; i++) {
        if(this.selectedFilter !== "") {
          if(segSingleData[i]["lblText"] === this.selectedFilter)
            segSingleData[i]["imgSelect"]["src"] = "tickmark_green.png";
          else
            segSingleData[i]["imgSelect"]["src"] = "transparent.png";
        }
        else {
          segSingleData[i]["imgSelect"]["src"] = "transparent.png";
        }
      }
      scope.view.segSingleTransactions.setData(segSingleData);
      scope.view.segSingleTransactions.setVisibility(true);
      scope.view.flxSingleTransactions.setVisibility(true);
    }
    else {
      scope.view.flxSingleTransactions.setVisibility(false);
      scope.view.segSingleTransactions.setVisibility(false);
    }

    if(presenter.presentationController.isBulkApprovalsHistoryAvailable) {
      var segBulkData = presenter.presentationController.bulkApprovalsHistory;
      var j = 0;
      for(j = 0; j < segBulkData.length; j++) {
        if(this.selectedFilter !== "") {
          if(segBulkData[j]["lblText"] === this.selectedFilter)
            segBulkData[j]["imgSelect"]["src"] = "tickmark_green.png";
          else
            segBulkData[j]["imgSelect"]["src"] = "transparent.png";
        }
        else {
          segBulkData[j]["imgSelect"]["src"] = "transparent.png";
        }
      }
      scope.view.segBulkTransactions.setData(segBulkData);
      scope.view.segBulkTransactions.setVisibility(true);
      scope.view.flxBulkTransactions.setVisibility(true);
    }
    else {
      scope.view.flxBulkTransactions.setVisibility(false);
      scope.view.segBulkTransactions.setVisibility(false);
    }

    if(presenter.presentationController.isOtherApprovalsHistoryAvailable) {
      var segOtherData = presenter.presentationController.otherApprovalsHistory;
      var k = 0;
      for(k = 0; k < segOtherData.length; k++) {
        if(this.selectedFilter !== "") {
          if(segOtherData[k]["lblText"] === this.selectedFilter)
            segOtherData[k]["imgSelect"]["src"] = "tickmark_green.png";
          else
            segOtherData[k]["imgSelect"]["src"] = "transparent.png";
        }
        else {
          segOtherData[k]["imgSelect"]["src"] = "transparent.png";
        }
      }
      scope.view.segOtherRequests.setData(segOtherData);
      scope.view.segOtherRequests.setVisibility(true);
      scope.view.flxOtherRequests.setVisibility(true);
    }
    else {
      scope.view.flxOtherRequests.setVisibility(false);
      scope.view.segOtherRequests.setVisibility(false);
    }
  },

  showOrHideTransactionType: function () {
    if (this.view.imgSelectTransactionType.src === "arrowup.png") {
      this.view.flxSelectTransactionTypeS.setVisibility(false);
      this.view.imgSelectTransactionType.src = "arrowdown.png";
    } else {
      this.view.flxSelectTransactionTypeS.setVisibility(true);
      this.view.imgSelectTransactionType.src = "arrowup.png";
    }
    this.view.forceLayout();
  },

  selectAllTransactionType: function () {
    this.removeAllTicksFromTransactionType(); //.bind(this);
    this.view.imgAll.src = "tickmark_green.png";
    this.view.forceLayout();
  },

  removeAllTicksFromTransactionType: function () {
    if (this.view.segSingleTransactions.isVisible) {
      var singleData = this.view.segSingleTransactions.data;
      singleData.forEach(function (entry) {
        if (!kony.sdk.isNullOrUndefined(entry["imgSelect"]))
          entry["imgSelect"]["src"] = "transparent.png";
      });
      this.view.segSingleTransactions.data = singleData;
    }
    if (this.view.segBulkTransactions.isVisible) {
      var bulkData = this.view.segBulkTransactions.data;
      bulkData.forEach(function (entry) {
        if (!kony.sdk.isNullOrUndefined(entry["imgSelect"]))
          entry["imgSelect"]["src"] = "transparent.png";
      });
      this.view.segBulkTransactions.data = bulkData;
    }
    if (this.view.segOtherRequests.isVisible) {
      var otherData = this.view.segOtherRequests.data;
      otherData.forEach(function (entry) {
        if (!kony.sdk.isNullOrUndefined(entry["imgSelect"]))
          entry["imgSelect"]["src"] = "transparent.png";
      });
      this.view.segOtherRequests.data = otherData;
    }
    this.view.imgAll.src = "transparent.png";
  },

  showOrHideSortBy: function () {
    if (this.view.imgSortBy.src === "arrowup.png") {
      this.view.flxSortBys.setVisibility(false);
      this.view.imgSortBy.src = "arrowdown.png";
    } else {
      this.view.flxSortBys.setVisibility(true);
      this.view.imgSortBy.src = "arrowup.png";
    }
    this.view.forceLayout();
  },

  removeAllTicksFromSortBy: function () {
    this.view.flxpprovaldate.src = "transparent.png";
    this.view.imgStatus.src = "transparent.png";
    this.view.imgTransactionType.src = "transparent.png";
  },

  selectApprovalDateSortBy: function () {
    this.removeAllTicksFromSortBy();
    this.view.flxpprovaldate.src = "tickmark_green.png";
  },

  selectStatusSortBy: function () {
    this.removeAllTicksFromSortBy();
    this.view.imgStatus.src = "tickmark_green.png";
  },

  selectTypeSortBy: function () {
    this.removeAllTicksFromSortBy();
    this.view.imgTransactionType.src = "tickmark_green.png";
  },

  showOrHideStatus: function () {
    if (this.view.imgViewBy.src === "arrowup.png") {
      this.view.flxViewByStatuss.setVisibility(false);
      this.view.imgViewBy.src = "arrowdown.png";
    } else {
      this.view.flxViewByStatuss.setVisibility(true);
      this.view.imgViewBy.src = "arrowup.png";
    }
    this.view.forceLayout();
  },

  removeCheckBoxesFromViewByStatus: function () {
    this.view.imgViewByStatusAll.src = "checkboxempty.png";
    this.view.imgApproved.src = "checkboxempty.png";
    this.view.imgRejected.src = "checkboxempty.png";
    this.view.imgPending.src = "checkboxempty.png";
  },

  selectStatusAll: function () {
    this.removeCheckBoxesFromViewByStatus();
    this.view.imgViewByStatusAll.src = "checkboxtick.png";
  },

  areAllSelected: function () {
    if (this.view.imgApproved.src === "checkboxtick.png")
      if (this.view.imgRejected.src === "checkboxtick.png")
        if (this.view.imgPending.src === "checkboxtick.png")
          return true;
    return false;
  },

  selectApproved: function () {
    if (this.view.imgViewByStatusAll.src === "checkboxtick.png")
      this.removeCheckBoxesFromViewByStatus();
    this.view.imgApproved.src = "checkboxtick.png";
    if (this.areAllSelected())
      this.selectStatusAll();
  },

  selectRejected: function () {
    if (this.view.imgViewByStatusAll.src === "checkboxtick.png")
      this.removeCheckBoxesFromViewByStatus();
    this.view.imgRejected.src = "checkboxtick.png";
    if (this.areAllSelected())
      this.selectStatusAll();
  },

  selectPending: function () {
    if (this.view.imgViewByStatusAll.src === "checkboxtick.png")
      this.removeCheckBoxesFromViewByStatus();
    this.view.imgPending.src = "checkboxtick.png";
    if (this.areAllSelected())
      this.selectStatusAll();
  },

  showOrHideViewByDuration: function () {
    if (this.view.imgViewByDuration.src === "arrowup.png") {
      this.view.flxViewByDurations.setVisibility(false);
      this.view.imgViewByDuration.src = "arrowdown.png";
    } else {
      this.view.flxViewByDurations.setVisibility(true);
      this.view.imgViewByDuration.src = "arrowup.png";
    }
    this.view.forceLayout();
  },

  removeAllDurations: function () {
    this.view.imgLastSixMonths.src = "radiobuttoninactive_big.png";
    this.view.imgToday.src = "radiobuttoninactive_big.png";
    this.view.imglastoneweek.src = "radiobuttoninactive_big.png";
    this.view.imgLastOneMonth.src = "radiobuttoninactive_big.png";
    this.view.imgLastOneYear.src = "radiobuttoninactive_big.png";
  },

  selectLastSixMonths: function () {
    this.removeAllDurations();
    this.view.imgLastSixMonths.src = "radiobtn_big.png";
  },

  selectToday: function () {
    this.removeAllDurations();
    this.view.imgToday.src = "radiobtn_big.png";
  },

  selectLastOneWeek: function () {
    this.removeAllDurations();
    this.view.imglastoneweek.src = "radiobtn_big.png";
  },

  selectLastOneMonths: function () {
    this.removeAllDurations();
    this.view.imgLastOneMonth.src = "radiobtn_big.png";
  },

  selectLastOneYear: function () {
    this.removeAllDurations();
    this.view.imgLastOneYear.src = "radiobtn_big.png";
  },

  sendData2Form : function () {
    try {
      if(this.view.imgAll.src === "tickmark_green.png") {
        this.setFilters("");
      }
      else {
        if(this.view.flxSingleTransactions.isVisible) {
          var single = this.view.segSingleTransactions.data;
          single.forEach(function (entry) {
            if (!kony.sdk.isNullOrUndefined(entry["imgSelect"]))
              if(entry["imgSelect"] === "tickmark_green.png") {
                var presenter = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule('ACHModule');
                var presentData = presenter.presentationController.getFilters();
                var name = entry["lblText"];
                if(presentData === "") {
                  presenter.presentationController.setFilters(name);
                }
                else {
                  presenter.presentationController.setFilters(presentData + "," + name);
                }
              }
          });
        }
        if(this.view.flxBulkTransactions.isVisible) {
          var bulk = this.view.segBulkTransactions.data;
          bulk.forEach(function (entry) {
            if (!kony.sdk.isNullOrUndefined(entry["imgSelect"]))
              if(entry["imgSelect"] === "tickmark_green.png") {
                var presenter = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule('ACHModule');
                var presentData = presenter.presentationController.getFilters();
                var name = entry["lblText"];
                if(presentData === "") {
                  presenter.presentationController.setFilters(name);
                }
                else {
                  presenter.presentationController.setFilters(presentData + "," + name);
                }
              }
          });
        }
        if(this.view.flxOtherRequests.isVisible) {
          var other = this.view.segOtherRequests.data;
          other.forEach(function (entry) {
            if (!kony.sdk.isNullOrUndefined(entry["imgSelect"]))
              if(entry["imgSelect"] === "tickmark_green.png") {
                var presenter = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule('ACHModule');
                var presentData = presenter.presentationController.getFilters();
                var name = entry["lblText"];
                if(presentData === "") {
                  presenter.presentationController.setFilters(name);
                }
                else {
                  presenter.presentationController.setFilters(presentData + "," + name);
                }
              }
          });
        }
        if(this.view.imgApproved.src ="checkboxtick.png")
        {
          var presenter = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule('ACHModule');
          var presentData = presenter.presentationController.getFilters();             
          if(presentData === "") {
            presenter.presentationController.setFilters("Approved");
          } else {
            presenter.presentationController.setFilters(presentData + "," +"Approved");
          }
        }else if(this.view.imgRejected.src ="checkboxtick.png"){
          var presenter = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule('ACHModule');
          var presentData = presenter.presentationController.getFilters();             
          if(presentData === "") {
            presenter.presentationController.setFilters("Rejected");
          } else {
            presenter.presentationController.setFilters(presentData + "," +"Rejected");
          }
        }else if(this.view.imgPending.src ="checkboxtick.png"){
          var presenter = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule('ACHModule');
          var presentData = presenter.presentationController.getFilters();             
          if(presentData === "") {
            presenter.presentationController.setFilters("Pending");
          } else {
            presenter.presentationController.setFilters(presentData + "," +"Pending");
          }
        }
      }
    }
    catch(e) {}
    this.dummyGoBack();
  },

  sendData2Frm: function() {
    this.sendData2Form();
  },

  getDataFromFrm : function () {

  },

  setFilters : function(data) {
    if(kony.sdk.isNullOrUndefined(data)) data = "";
    var presenter = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule('ACHModule');
    return presenter.presentationController.setFilters(data);
  },

  getFilters : function() {
    var presenter = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule('ACHModule');
    return presenter.presentationController.getFilters();
  },

  singleSegmentSelected : function(context1){
    //var selectedRowItem = context1["selectedRowItems"];
    this.view.imgAll.src = "transparent.png";
    var data = this.view.segSingleTransactions.data;
    if(data[context1["selectedRowIndices"][0][1][0]]["imgSelect"] === "transparent.png")
      data[context1["selectedRowIndices"][0][1][0]]["imgSelect"] = "tickmark_green.png";
    else
      data[context1["selectedRowIndices"][0][1][0]]["imgSelect"] = "transparent.png";
    this.view.segSingleTransactions.data = data;
  },

  bulkSegmentSelected : function(context1){
    this.view.imgAll.src = "transparent.png";
    var data = this.view.segBulkTransactions.data;
    if(data[context1["selectedRowIndices"][0][1][0]]["imgSelect"] === "transparent.png")
      data[context1["selectedRowIndices"][0][1][0]]["imgSelect"] = "tickmark_green.png";
    else
      data[context1["selectedRowIndices"][0][1][0]]["imgSelect"] = "transparent.png";
    this.view.segBulkTransactions.data = data;
  },

  otherSegmentSelected : function(context1){
    this.view.imgAll.src = "transparent.png";
    var data = this.view.segOtherRequests.data;
    if(data[context1["selectedRowIndices"][0][1][0]]["imgSelect"] === "transparent.png")
      data[context1["selectedRowIndices"][0][1][0]]["imgSelect"] = "tickmark_green.png";
    else
      data[context1["selectedRowIndices"][0][1][0]]["imgSelect"] = "transparent.png";
    this.view.segOtherRequests.data = data;
  },
  
  selectAll : function () {
    this.removeAllSelectedFromSegement();
    this.view.imgAll.src = "tickmark_green.png";
  },

  removeAllSelectedFromSegement : function() {
    this.view.imgAll.src = "transparent.png";
    
    var data0 = this.view.segOtherRequests.data;
    var data1 = this.view.segBulkTransactions.data;
    var data2 = this.view.segSingleTransactions.data;

    data0.forEach(function (entry) {
      entry["imgSelect"] = {
        "src" : "transparent.png"
      };
    });

    data1.forEach(function (entry) {
      entry["imgSelect"] = {
        "src" : "transparent.png"
      };
    });

    data2.forEach(function (entry) {
      entry["imgSelect"] = {
        "src" : "transparent.png"
      };
    });
    
    this.view.segOtherRequests.data = data0;
    this.view.segBulkTransactions.data = data1;
    this.view.segSingleTransactions.data = data2;
  },

  goBack : function () {
    //var navManager = applicationManager.getNavigationManager();
    //navManager.goBack();
    this.init();
  },

  dummyGoBack : function () {
    //var navManager = applicationManager.getNavigationManager();
    //navManager.goBack();
    //this.init();
    var ntf = new kony.mvc.Navigation(kony.application.getPreviousForm().id);
    ntf.navigate();
  },
});