define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** init defined for frmBenEmailAddressEurope **/
    AS_Form_c82f35b0c8c4420db0cbabe2609d879c: function AS_Form_c82f35b0c8c4420db0cbabe2609d879c(eventobject) {
        var self = this;
        return self.init.call(this);
    },
    /** preShow defined for frmBenEmailAddressEurope **/
    AS_Form_ef8a71013117419e900bdc5c608a488b: function AS_Form_ef8a71013117419e900bdc5c608a488b(eventobject) {
        var self = this;
        return self.preshow.call(this);
    }
});