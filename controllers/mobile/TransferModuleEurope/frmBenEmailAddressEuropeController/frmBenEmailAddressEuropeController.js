define({ 

 //Type your controller code here 
  init: function(){
	var navManager = applicationManager.getNavigationManager();
    var currentForm = navManager.getCurrentForm();
    applicationManager.getPresentationFormUtility().initCommonActions(this,"YES",currentForm);
  },
   initActions:function(){
    var scope=this;
    this.view.txtEmailAddress.text = "";
    this.view.customHeader.flxBack.onClick = scope.flxBackOnClick;
    this.view.customHeader.btnRight.onClick = scope.onClickCancel;
    this.view.btnVerify.onClick = scope.onSaveEmailAddress;
  },
  preshow: function() {
        this.view.txtEmailAddress.onTextChange =  this.updateUI;
        this.renderTitleBar();
        this.initActions();
        this.updateUI();
        var navManager = applicationManager.getNavigationManager();
        var currentForm = navManager.getCurrentForm();
        applicationManager.getPresentationFormUtility().logFormName(currentForm);
    },
   onClickCancel: function() {
    applicationManager.getPresentationUtility().showLoadingScreen();
    var transferModPresentationController = applicationManager.getModulesPresentationController("TransferModule");
    transferModPresentationController.commonFunctionForNavigation("frmBenVerifyDetailsEurope");
  },
   flxBackOnClick: function() {
    var navMan=applicationManager.getNavigationManager();
    navMan.goBack();
  },
   onSaveEmailAddress:function(){
    applicationManager.getPresentationUtility().showLoadingScreen();
    var data ={};
	var transferModulePresentationController = applicationManager.getModulesPresentationController("TransferModule");
    if(this.view.txtEmailAddress.text !== ""){
     data ={  
      "emailAddress" :this.view.txtEmailAddress.text
     };       
    }
     transferModulePresentationController.navigateToVerifyDetailsFromEmailAddress(data);
  },
  updateUI: function() {
        if (this.view.txtEmailAddress.text!== "") {
            this.view.btnVerify.skin = "sknBtn0095e4RoundedffffffSSP26px";
            this.view.btnVerify.setEnabled(true);
        } else {
			this.view.btnVerify.skin = "sknBtnE2E9F0Rounded";
            this.view.btnVerify.setEnabled(false);
        }
        this.view.forceLayout();
    },
   renderTitleBar :function(){
    var deviceUtilManager = applicationManager.getDeviceUtilManager();
    var isIphone = deviceUtilManager.isIPhone();
    if(!isIphone){
      this.view.flxHeader.isVisible = true;
    }
    else{
      this.view.flxHeader.isVisible = false;
    }
  },
  enableVerifyButton:function(){
    this.view.btnVerify.setEnabled(true);
    this.view.btnVerify.skin = "sknBtn0095e4RoundedffffffSSP26px";
  }

 });