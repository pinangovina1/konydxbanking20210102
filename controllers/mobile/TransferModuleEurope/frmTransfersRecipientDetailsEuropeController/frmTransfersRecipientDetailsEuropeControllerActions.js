define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    AS_BarButtonItem_ef0049161b6d40d18e84be786a54fcf6: function AS_BarButtonItem_ef0049161b6d40d18e84be786a54fcf6(eventobject) {
        var self = this;
        this.onClickCancel();
    },
    /** preShow defined for frmTransfersRecipientDetailsEurope **/
    AS_Form_d92a3e16687f43fb8488cd6ec6858832: function AS_Form_d92a3e16687f43fb8488cd6ec6858832(eventobject) {
        var self = this;
        this.frmPreShow();
    },
    /** init defined for frmTransfersRecipientDetailsEurope **/
    AS_Form_eafca79a20b94407b5207b40d934da21: function AS_Form_eafca79a20b94407b5207b40d934da21(eventobject) {
        var self = this;
        this.init();
    }
});