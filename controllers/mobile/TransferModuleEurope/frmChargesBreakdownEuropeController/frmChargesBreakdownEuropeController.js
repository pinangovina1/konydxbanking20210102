define({ 

  //Type your controller code here 
  initActions: function () {
    this.view.customHeader.flxBack.onTouchEnd = this.navigateCustomBack;
    this.view.customHeader.btnRight.onClick = this.cancelOnClick;
    this.getSegmentData();
  },
  navigateCustomBack: function() {
    var transferModule = applicationManager.getModulesPresentationController("TransferModule");
    transferModule.commonFunctionForgoBack();
  },
  cancelOnClick:function(){
    var transferModule = applicationManager.getModulesPresentationController("TransferModule");
    transferModule.cancelCommon();
  },

  getWidgetDataMap : function () {
    var map = {
      lblTitle:"property",
      lblDetails:"value",
    }
    return map;
  },
  getSegmentData : function () {
    var transferPresentationController = applicationManager.getModulesPresentationController("TransferModule");
    var segData = transferPresentationController.getChargesBreakdown();
    this.view.segDataCharges.widgetDataMap = this.getWidgetDataMap();
    this.view.segDataCharges.setData(segData);

  }
  
});