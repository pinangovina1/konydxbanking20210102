define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    AS_BarButtonItem_ab6c12595b3c4ae28dcd817417adbcad: function AS_BarButtonItem_ab6c12595b3c4ae28dcd817417adbcad(eventobject) {
        var self = this;
        this.blockBackgroundonAdditionalOptions();
    },
    AS_BarButtonItem_i7e8b91642ad47eab92a5c0efd41ffa9: function AS_BarButtonItem_i7e8b91642ad47eab92a5c0efd41ffa9(eventobject) {
        var self = this;
        this.navigateCustomBack();
    },
    /** init defined for frmBeneficiaryDetailsEurope **/
    AS_Form_d10a62827207450d82302956733a9d0b: function AS_Form_d10a62827207450d82302956733a9d0b(eventobject) {
        var self = this;
        this.init();
    },
    /** preShow defined for frmBeneficiaryDetailsEurope **/
    AS_Form_ebd1bfed75ff497b8b98eb5087dabc58: function AS_Form_ebd1bfed75ff497b8b98eb5087dabc58(eventobject) {
        var self = this;
        this.preShow();
    },
    /** postShow defined for frmBeneficiaryDetailsEurope **/
    AS_Form_ha2567a69e05422a94225d07d2060aba: function AS_Form_ha2567a69e05422a94225d07d2060aba(eventobject) {
        var self = this;
        this.postShow();
    }
});