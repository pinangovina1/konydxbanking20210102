it("searchTransaction", async function() {
	// :User Injected Code Snippet [// - [11 lines]]
	await kony.automation.playback.waitFor(["frmPFMCategorisedTransactions","tbxSearch"]);
	    kony.automation.textbox.enterText(["frmPFMCategorisedTransactions","tbxSearch"],"spent on");
	    await kony.automation.playback.waitFor(["frmPFMCategorisedTransactions","segTransactions"]);
	    kony.automation.segmentedui.scrollToRow(["frmPFMCategorisedTransactions","segTransactions[0,0]"]);
	    kony.automation.segmentedui.click(["frmPFMCategorisedTransactions","segTransactions[0,0]"]);
	    await kony.automation.playback.waitFor(["frmPFMTransactionDetails","lblDescValueTrans"]);
	    // :User Injected Code Snippet [// - [1 lines]]
	    expect(kony.automation.widget.getWidgetProperty(["frmPFMTransactionDetails","lblDescValueTrans"], "text")).toContain("Spent on");
	    // :End User Injected Code Snippet {38d43400-a067-8f8d-9372-35ba7241fb30}
	    await kony.automation.device.deviceBack();
	    await kony.automation.playback.waitFor(["frmPFMCategorisedTransactions","tbxSearch"]);
	// :End User Injected Code Snippet {2a8b2e75-0224-ddcb-e559-8c34e436e500}
});