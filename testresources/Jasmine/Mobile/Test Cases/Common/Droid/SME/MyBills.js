function navigateMyBillPage(){
  kony.automation.playback.waitFor(["frmUnifiedDashboard","customHeader","flxBack"]);
  kony.automation.flexcontainer.click(["frmUnifiedDashboard","customHeader","flxBack"]);
  kony.automation.playback.waitFor(["frmUnifiedDashboard","Hamburger","segHamburger"]);
  kony.automation.segmentedui.click(["frmUnifiedDashboard","Hamburger","segHamburger[0,5]"]);
  kony.automation.playback.waitFor(["frmBillPay","tbxSearch"]);
}

function Logout() {
  kony.automation.playback.waitFor(["frmUnifiedDashboard","customHeader","flxBack"],15000);
  kony.automation.flexcontainer.click(["frmUnifiedDashboard","customHeader","flxBack"]);
  kony.automation.playback.waitFor(["frmUnifiedDashboard","Hamburger","flxLogout"],5000);
  kony.automation.widget.touch(["frmUnifiedDashboard","Hamburger","flxLogout"], null,null,[34,28]);
  kony.automation.playback.waitFor(["frmLogout","btnLogIn"],10000);
  kony.automation.button.click(["frmLogout","btnLogIn"]);
  kony.automation.playback.waitFor(["frmLogin","login","tbxUsername"],10000);
}