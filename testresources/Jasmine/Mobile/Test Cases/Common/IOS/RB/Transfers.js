async function goToTransfers() {
    await kony.automation.playback.waitFor(["frmUnifiedDashboard", "customFooter", "lblTransfer"]);
    kony.automation.flexcontainer.click(["frmUnifiedDashboard", "customFooter", "flxTransfer"]);
    await kony.automation.playback.wait(2000);
    await kony.automation.playback.waitFor(["frmMMTransferFromAccount", "tbxSearch"]);
}

async function searchInFromAndToScreen(fromAccount, toAccount) { //params are case sensitive
    await kony.automation.playback.waitFor(["frmMMTransferFromAccount", "tbxSearch"]);
    kony.automation.widget.touch(["frmMMTransferFromAccount", "tbxSearch"], [110, 23], null, null);
    await kony.automation.playback.waitFor(["frmMMTransferFromAccount", "customSearchbox", "tbxSearch"]);
    await kony.automation.playback.wait(2000);
    kony.automation.textbox.enterText(["frmMMTransferFromAccount", "customSearchbox", "tbxSearch"], fromAccount);
    await kony.automation.playback.wait(2000);
    await kony.automation.playback.waitFor(["frmMMTransferFromAccount", "segTransactions"]);
    kony.automation.segmentedui.scrollToRow(["frmMMTransferFromAccount", "segTransactions[0,0]"]);
    // :User Injected Code Snippet [// - [2 lines]]
    var fromAccountName = kony.automation.widget.getWidgetProperty(["frmMMTransferFromAccount", "segTransactions[0,0]", "lblAccountName"], "text");
    expect(fromAccountName).toContain(fromAccount);
    // :End User Injected Code Snippet {8ee899e9-e37c-2be5-f725-bbcdb1020dc4}
    kony.automation.segmentedui.click(["frmMMTransferFromAccount", "segTransactions[0,0]"]);
    await kony.automation.playback.waitFor(["frmMMTransferToAccount", "tbxSearch"]);
    kony.automation.widget.touch(["frmMMTransferToAccount", "tbxSearch"], [146, 13], null, null);
    await kony.automation.playback.waitFor(["frmMMTransferToAccount", "customSearchbox", "tbxSearch"]);
    await kony.automation.playback.wait(2000);
    kony.automation.textbox.enterText(["frmMMTransferToAccount", "customSearchbox", "tbxSearch"], toAccount);
    await kony.automation.playback.waitFor(["frmMMTransferToAccount", "segTransactions"]);
    kony.automation.segmentedui.scrollToRow(["frmMMTransferToAccount", "segTransactions[0,0]"]);
    await kony.automation.playback.wait(2000);
    // :User Injected Code Snippet [// - [2 lines]]
    var accountName = kony.automation.widget.getWidgetProperty(["frmMMTransferToAccount", "segTransactions[0,0]", "lblAccountName"], "text");
    expect(accountName).toContain(toAccount);
    // :End User Injected Code Snippet {195a3ae8-db55-4acb-396f-09b3a08ff861}
    kony.automation.segmentedui.click(["frmMMTransferToAccount", "segTransactions[0,0]"]);
    await kony.automation.playback.waitFor(["frmMMTransferAmount", "keypad", "btnThree"]);
}

async function enterAmount() {
    await kony.automation.playback.waitFor(["frmMMTransferAmount", "keypad", "btnThree"]);
    kony.automation.button.click(["frmMMTransferAmount", "keypad", "btnThree"]);
    kony.automation.button.click(["frmMMTransferAmount", "keypad", "btnZero"]);
    kony.automation.button.click(["frmMMTransferAmount", "keypad", "btnZero"]);
    kony.automation.button.click(["frmMMTransferAmount", "btnContinue"]);
    await kony.automation.playback.waitFor(["frmMMReview", "flxConfirmationDetails", "segDetails"]);
}

async function oneTimeTransfer() {

    await kony.automation.playback.waitFor(["frmMMReview", "btnTransfer"]);
    kony.automation.button.click(["frmMMReview", "btnTransfer"]);
    await kony.automation.playback.waitFor(["frmMMConfirmation", "lblSuccessMessage"]);
    // :User Injected Code Snippet [//Assert success message - [1 lines]]
	var lblMessage = kony.automation.widget.getWidgetProperty(["frmMMConfirmation", "lblMessage"], "text");
	var lblSuccessMessage = kony.automation.widget.getWidgetProperty(["frmMMConfirmation", "lblSuccessMessage"], "text");
		            
	expect(lblMessage.toLowerCase()).toContain("success");
	expect(lblSuccessMessage.toLowerCase()).toContain("success");
    //expect(kony.automation.widget.getWidgetProperty(["frmMMConfirmation", "lblSuccessMessage"], "text")).toContain("uccess");
    // :End User Injected Code Snippet {d03b935e-9e64-4c3e-db7c-f2e1959569b7}
    await kony.automation.playback.waitFor(["frmMMConfirmation", "btnDashboard"]);
    kony.automation.button.click(["frmMMConfirmation", "btnDashboard"]);
    await kony.automation.playback.waitFor(["frmUnifiedDashboard", "flxDashboard", "segAccounts"]);
}

async function transferScheduledOnce() {
    await kony.automation.playback.waitFor(["frmMMReview", "segDetails"]);
    kony.automation.segmentedui.click(["frmMMReview", "segDetails[0,0]"]);
    await kony.automation.playback.wait(3000);
    await kony.automation.playback.waitFor(["frmMMFrequency", "segOptions"]);
    kony.automation.segmentedui.click(["frmMMFrequency", "segOptions[0,0]"]);
    await kony.automation.playback.wait(5000);
    await kony.automation.playback.waitFor(["frmMMStartDate", "customCalendar", "flxNextMonth"]);
    kony.automation.flexcontainer.click(["frmMMStartDate", "customCalendar", "flxNextMonth"]);
    await kony.automation.playback.wait(2000);
    // :User Injected Code Snippet [//Select a date - [3 lines]]
    await kony.automation.playback.waitFor(["frmMMStartDate", "customCalendar", "flxNextMonth"]);
    kony.automation.widget.touch(["frmMMStartDate", "customCalendar", "flxNextMonth"], [178, 125], null, [178, 125]);
    kony.automation.widget.touch(["frmMMStartDate", "customCalendar", "flxMonth", "m3CopyLabel0ac5bc532de9c4c"], null, null, [17, 17]);
    // :End User Injected Code Snippet {414f83d0-e0a2-735c-c437-a564878872a6}
    await kony.automation.playback.wait(3000);
    await kony.automation.playback.waitFor(["frmMMStartDate", "btnContinue"]);
    kony.automation.button.click(["frmMMStartDate", "btnContinue"]);
    await kony.automation.playback.wait(4000);
    await kony.automation.playback.waitFor(["frmMMReview", "btnTransfer"]);
    kony.automation.button.click(["frmMMReview", "btnTransfer"]);
    await kony.automation.playback.wait(7000);
    // :User Injected Code Snippet [//Asser for success - [1 lines]]
	var lblMessage = kony.automation.widget.getWidgetProperty(["frmMMConfirmation", "lblMessage"], "text");
	var lblSuccessMessage = kony.automation.widget.getWidgetProperty(["frmMMConfirmation", "lblSuccessMessage"], "text");
  
	expect(lblMessage.toLowerCase()).toContain("success");
	expect(lblSuccessMessage.toLowerCase()).toContain("success");
    //expect(kony.automation.widget.getWidgetProperty(["frmMMConfirmation", "lblSuccessMessage"], "text")).toEqual("We successfully scheduled your transfer");
    // :End User Injected Code Snippet {ca0bdba2-106a-5d30-5a72-5e4aa2ed92df}
    kony.automation.button.click(["frmMMConfirmation", "btnDashboard"]);
    await kony.automation.playback.wait(8000);
    await kony.automation.playback.waitFor(["frmUnifiedDashboard", "flxDashboard", "segAccounts"]);
}