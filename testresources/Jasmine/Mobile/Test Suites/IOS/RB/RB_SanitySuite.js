describe("RB_SanitySuite", function() {
	beforeEach(async function() {
	
	    var flgLoginForm = await kony.automation.playback.waitFor(["frmLogin", "login", "btnLogIn"], 2000);
	    var flgLogoutForm = await kony.automation.playback.waitFor(["frmLogout", "btnLogIn"], 2000);
	    kony.print("flgLoginForm: " + flgLoginForm + " ,flgLogoutForm: " + flgLogoutForm);
	
	    if (flgLogoutForm === true || flgLogoutForm === 1) {
	        kony.automation.button.click(["frmLogout", "btnLogIn"]);
	        kony.automation.playback.waitFor(["frmLogin", "login", "tbxUsername"], 10000);
	        await kony.automation.playback.wait(3000);
	        expect(kony.automation.widget.getWidgetProperty(["frmLogin", "login", "tbxPassword"], "text")).toEqual("");
	        await login("dbxJasmine");
	    } else if (flgLoginForm === true || flgLoginForm === 1) {
	        await login("dbxJasmine");
	    }
	});
	
	
	async function login(username) {
	    await kony.automation.playback.waitFor(["frmLogin", "login", "tbxUsername"]);
	    kony.automation.textbox.enterText(["frmLogin", "login", "tbxUsername"], username);
	    kony.automation.textbox.enterText(["frmLogin", "login", "tbxPassword"], "Kony@1234");
	    await kony.automation.playback.waitFor(["frmLogin", "login", "btnLogIn"]);
	    kony.automation.button.click(["frmLogin", "login", "btnLogIn"]);
	    //Verifying Terms and Condition page -
	    var frmTnC = await kony.automation.playback.waitFor(["frmTermsAndCondition", "flxCheckBox"], 20000);
	    if (frmTnC) {
	        kony.automation.flexcontainer.click(["frmTermsAndCondition", "flxCheckBox"]);
	        await kony.automation.playback.waitFor(["frmTermsAndCondition", "btnContinue"]);
	        kony.automation.button.click(["frmTermsAndCondition", "btnContinue"]);
	    }
	
	    await kony.automation.playback.waitFor(["frmUnifiedDashboard", "lblBankName"], 15000);
	}
	
	async function openMyBillsPage(){
		await kony.automation.playback.wait(2000);
		await kony.automation.playback.waitFor(["frmUnifiedDashboard","customFooter","flxBillPay"]);
		kony.automation.flexcontainer.click(["frmUnifiedDashboard","customFooter","flxBillPay"]);
		await kony.automation.playback.wait(10000);
		await kony.automation.playback.waitFor(["frmBillPay","tbxSearch"]);
	}
	
	async function searchBill(bill){
	  await kony.automation.playback.waitFor(["frmBillPay","tbxSearch"]);
		kony.automation.widget.touch(["frmBillPay","tbxSearch"], [111,20],null,null);
		await kony.automation.playback.wait(2000);
		kony.automation.textbox.enterText(["frmBillPay","customSearchbox","tbxSearch"],bill);
		await kony.automation.playback.wait(5000);
		// :User Injected Code Snippet [//Asert on search value - [3 lines]]
		kony.automation.segmentedui.scrollToRow(["frmBillPay","flxMainContainer","segTransactions[0,0]"]);
		await kony.automation.playback.wait(5000);
		expect(kony.automation.widget.getWidgetProperty(["frmBillPay","flxMainContainer","segTransactions[0,0]","flxAccountName","lblAccountName"], "text")).toContain(bill);
		// :End User Injected Code Snippet {30aa2644-535e-35f6-b6b3-176928a92236}
		await kony.automation.playback.wait(1000);
		await kony.automation.playback.waitFor(["frmBillPay","customSearchbox","btnCancel"]);
		kony.automation.button.click(["frmBillPay","customSearchbox","btnCancel"]);
		await kony.automation.playback.wait(8000);
	}
	
	async function goBackToDashboard(){
		await kony.automation.playback.waitFor(["frmBillPay","customFooter","flxAccounts"]);
		kony.automation.flexcontainer.click(["frmBillPay","customFooter","flxAccounts"]);
		await kony.automation.playback.wait(3000);
		await kony.automation.playback.waitFor(["frmUnifiedDashboard","flxDashboard","segAccounts"]);
	}
	
	async function openMenu(){
		kony.automation.flexcontainer.click(["frmUnifiedDashboard","customFooter","flxMore"]);
		await kony.automation.playback.wait(1000);
		await kony.automation.playback.waitFor(["frmUnifiedDashboard","Hamburger","flxMenu","segHamburger"]);
	}
	
	async function openMenu(menu){
		kony.automation.flexcontainer.click(["frmUnifiedDashboard","customFooter","flxMore"]);
		await kony.automation.playback.wait(1000);
		await kony.automation.playback.waitFor(["frmUnifiedDashboard","Hamburger","flxMenu","segHamburger"]);
		await kony.automation.playback.wait(1000);
		var menuOptions = kony.automation.widget.getWidgetProperty(["frmUnifiedDashboard","Hamburger","flxMenu","segHamburger"], "data");
		kony.print("menuOptions: "+menuOptions);
		var menuIndex = -1;
		for(i=0; i<menuOptions.length; i++){
			if(menuOptions[i].text === menu){
				menuIndex = i;
				break;
			}
	    }
		if(menuIndex > -1){
			kony.automation.segmentedui.click(["frmUnifiedDashboard","Hamburger","segHamburger[0," + menuIndex+ "]" ]);
	    }else{
	      expect(menuIndex).toBeGreaterThan(-1);
	    }
	}
	
	async function listOfRecipients(accountType){
		await kony.automation.playback.waitFor(["frmManageRecipientType","segRecipientType"]);
	
		var segAccountType = kony.automation.widget.getWidgetProperty(["frmManageRecipientType","segRecipientType"],"data");
		kony.print("segAccountTyep: "+segAccountType);
		var indexAccountType = -1;
		for(i=0; i<segAccountType[0][1].length; i++){
	      if(segAccountType[0][1][i].lblTransactionMode === accountType){
	        indexAccountType = i;
	        break;
	      }
	    }
		if(indexAccountType > -1){
		kony.automation.segmentedui.click(["frmManageRecipientType","segRecipientType[0,"+indexAccountType+"]"]);
		await kony.automation.playback.wait(3000);
		await kony.automation.playback.waitFor(["frmManageRecipientList","flxMainContainer","segRecipients"]);
		// :User Injected Code Snippet [// - [3 lines]]
		var segSize = kony.automation.widget.getWidgetProperty(["frmManageRecipientList","flxMainContainer","segRecipients"],"data");
		
		expect(true).toBe(  segSize.length >=1);
		// :End User Injected Code Snippet {b36868d0-465d-3125-0fdd-6359df041f5d}
	    }else{
	      expect(indexAccountType).toBeGreaterThan(-1);
	    }
	}
	
	async function addRecipientSameBank(){
	  await kony.automation.playback.waitFor(["frmManageRecipientList","btnAddRecipient"]);
		kony.automation.button.click(["frmManageRecipientList","btnAddRecipient"]);
		await kony.automation.playback.waitFor(["frmEnterBenAccNo","keypad","btnOne"]);
		kony.automation.button.click(["frmEnterBenAccNo","keypad","btnOne"]);
		kony.automation.button.click(["frmEnterBenAccNo","keypad","btnTwo"]);
		kony.automation.button.click(["frmEnterBenAccNo","keypad","btnThree"]);
		kony.automation.button.click(["frmEnterBenAccNo","keypad","btnFour"]);
		kony.automation.button.click(["frmEnterBenAccNo","keypad","btnFive"]);
		kony.automation.button.click(["frmEnterBenAccNo","keypad","btnSix"]);
		kony.automation.button.click(["frmEnterBenAccNo","btnContinue"]);
		await kony.automation.playback.waitFor(["frmReEnterBenAccNo","keypad","btnOne"]);
		kony.automation.button.click(["frmReEnterBenAccNo","keypad","btnOne"]);
		kony.automation.button.click(["frmReEnterBenAccNo","keypad","btnTwo"]);
		kony.automation.button.click(["frmReEnterBenAccNo","keypad","btnThree"]);
		kony.automation.button.click(["frmReEnterBenAccNo","keypad","btnFour"]);
		kony.automation.button.click(["frmReEnterBenAccNo","keypad","btnFive"]);
		kony.automation.button.click(["frmReEnterBenAccNo","keypad","btnSix"]);
		kony.automation.button.click(["frmReEnterBenAccNo","btnContinue"]);
		kony.automation.textbox.enterText(["frmBenName","txtRecipientName"],"samebankIOS");
		await kony.automation.playback.waitFor(["frmBenName","btnContinue"]);
		kony.automation.button.click(["frmBenName","btnContinue"]);
		await kony.automation.playback.waitFor(["frmBenVerifyDetails","btnContinue"]);
		kony.automation.button.click(["frmBenVerifyDetails","btnContinue"]);
		await kony.automation.playback.wait(3000);
		// :User Injected Code Snippet [// - [3 lines]]
		await kony.automation.playback.waitFor(["frmManageRecipientList","customPopup","lblPopup"]);
		
		expect(kony.automation.widget.getWidgetProperty(["frmManageRecipientList","customPopup","lblPopup"],"text")).toContain("added");
		// :End User Injected Code Snippet {6c03a7a9-f57f-f8d9-62f8-21a0ec441e4f}
	}
	
	async function addRecipientExternalAccount(){
		await kony.automation.playback.waitFor(["frmManageRecipientList","btnAddRecipient"]);
		kony.automation.button.click(["frmManageRecipientList","btnAddRecipient"]);
		await kony.automation.playback.waitFor(["frmAddBenRoutNo","keypad","btnOne"]);
		kony.automation.button.click(["frmAddBenRoutNo","keypad","btnOne"]);
		kony.automation.button.click(["frmAddBenRoutNo","keypad","btnTwo"]);
		kony.automation.button.click(["frmAddBenRoutNo","keypad","btnThree"]);
		kony.automation.button.click(["frmAddBenRoutNo","keypad","btnFour"]);
		kony.automation.button.click(["frmAddBenRoutNo","keypad","btnFive"]);
		kony.automation.button.click(["frmAddBenRoutNo","keypad","btnSix"]);
		kony.automation.button.click(["frmAddBenRoutNo","btnContinue"]);
		await kony.automation.playback.waitFor(["frmEnterBenAccNo","keypad","btnOne"]);
		kony.automation.button.click(["frmEnterBenAccNo","keypad","btnOne"]);
		kony.automation.button.click(["frmEnterBenAccNo","keypad","btnOne"]);
		kony.automation.button.click(["frmEnterBenAccNo","keypad","btnOne"]);
		kony.automation.button.click(["frmEnterBenAccNo","keypad","btnOne"]);
		kony.automation.button.click(["frmEnterBenAccNo","btnContinue"]);
		await kony.automation.playback.waitFor(["frmReEnterBenAccNo","keypad","btnOne"]);
		kony.automation.button.click(["frmReEnterBenAccNo","keypad","btnOne"]);
		kony.automation.button.click(["frmReEnterBenAccNo","keypad","btnOne"]);
		kony.automation.button.click(["frmReEnterBenAccNo","keypad","btnOne"]);
		kony.automation.button.click(["frmReEnterBenAccNo","keypad","btnOne"]);
		kony.automation.button.click(["frmReEnterBenAccNo","btnContinue"]);
		await kony.automation.playback.waitFor(["frmBenName","txtRecipientName"]);
		kony.automation.textbox.enterText(["frmBenName","txtRecipientName"],"ExtAccIOS");
		kony.automation.button.click(["frmBenName","btnContinue"]);
		await kony.automation.playback.waitFor(["frmBenVerifyDetails","btnContinue"]);
		kony.automation.button.click(["frmBenVerifyDetails","btnContinue"]);
		await kony.automation.playback.wait(3000);
		// :User Injected Code Snippet [// - [3 lines]]
		await kony.automation.playback.waitFor(["frmManageRecipientList","customPopup","lblPopup"]);
			
		expect(kony.automation.widget.getWidgetProperty(["frmManageRecipientList","customPopup","lblPopup"],"text")).toContain("added");
		// :End User Injected Code Snippet {79cae66c-ebcc-f103-0342-b9656a846e97}
	}
	
	async function addRecipientInternationalAccount(){
		await kony.automation.playback.waitFor(["frmManageRecipientList","btnAddRecipient"]);
		kony.automation.button.click(["frmManageRecipientList","btnAddRecipient"]);
		await kony.automation.playback.waitFor(["frmBenSwiftCode","txtSwiftCode"]);
		kony.automation.textbox.enterText(["frmBenSwiftCode","txtSwiftCode"],"12345678");
		kony.automation.button.click(["frmBenSwiftCode","btnContinue"]);
		await kony.automation.playback.waitFor(["frmEnterBenAccNumorIBAN","tbxAccountNumber"]);
		kony.automation.textbox.enterText(["frmEnterBenAccNumorIBAN","tbxAccountNumber"],"123456");
		kony.automation.button.click(["frmEnterBenAccNumorIBAN","btnContinue"]);
		await kony.automation.playback.waitFor(["frmReEnterBenAccNumorIBAN","tbxReEnteredAccountNumber"]);
		kony.automation.textbox.enterText(["frmReEnterBenAccNumorIBAN","tbxReEnteredAccountNumber"],"123456");
		kony.automation.button.click(["frmReEnterBenAccNumorIBAN","btnContinue"]);
		await kony.automation.playback.waitFor(["frmBenName","txtRecipientName"]);
		kony.automation.textbox.enterText(["frmBenName","txtRecipientName"],"InternationalAcc");
		kony.automation.button.click(["frmBenName","btnContinue"]);
		await kony.automation.playback.waitFor(["frmBenVerifyDetails","btnContinue"]);
		kony.automation.button.click(["frmBenVerifyDetails","btnContinue"]);
		// :User Injected Code Snippet [// - [2 lines]]
		await kony.automation.playback.waitFor(["frmManageRecipientList","customPopup","lblPopup"]);
		expect(kony.automation.widget.getWidgetProperty(["frmManageRecipientList","customPopup","lblPopup"],"text")).toContain("added");
		// :End User Injected Code Snippet {409ef018-77ec-57b6-6e74-114d25885fc6}
	}
	
	async function openManageRecipientPage(){
		await kony.automation.playback.waitFor(["frmUnifiedDashboard","Hamburger","segHamburger"]);
		kony.automation.segmentedui.click(["frmUnifiedDashboard","Hamburger","segHamburger[0,3]"]);
		await kony.automation.playback.wait(2000);
		await kony.automation.playback.waitFor(["frmManageRecipientType","segRecipientType"]);
	}
	
	async function goBackToDashboardFromManageRecipient(){
		await kony.automation.playback.waitFor(["frmManageRecipientList","flxMainContainer","segRecipients"]);
		// :User Injected Code Snippet [// - [3 lines]]
		kony.automation.device.deviceBack();
		await kony.automation.playback.wait(1000);
		kony.automation.device.deviceBack();
	}
	
	async function goToTransfers() {
	    await kony.automation.playback.waitFor(["frmUnifiedDashboard", "customFooter", "lblTransfer"]);
	    kony.automation.flexcontainer.click(["frmUnifiedDashboard", "customFooter", "flxTransfer"]);
	    await kony.automation.playback.wait(2000);
	    await kony.automation.playback.waitFor(["frmMMTransferFromAccount", "tbxSearch"]);
	}
	
	async function searchInFromAndToScreen(fromAccount, toAccount) { //params are case sensitive
	    await kony.automation.playback.waitFor(["frmMMTransferFromAccount", "tbxSearch"]);
	    kony.automation.widget.touch(["frmMMTransferFromAccount", "tbxSearch"], [110, 23], null, null);
	    await kony.automation.playback.waitFor(["frmMMTransferFromAccount", "customSearchbox", "tbxSearch"]);
	    await kony.automation.playback.wait(2000);
	    kony.automation.textbox.enterText(["frmMMTransferFromAccount", "customSearchbox", "tbxSearch"], fromAccount);
	    await kony.automation.playback.wait(2000);
	    await kony.automation.playback.waitFor(["frmMMTransferFromAccount", "segTransactions"]);
	    kony.automation.segmentedui.scrollToRow(["frmMMTransferFromAccount", "segTransactions[0,0]"]);
	    // :User Injected Code Snippet [// - [2 lines]]
	    var fromAccountName = kony.automation.widget.getWidgetProperty(["frmMMTransferFromAccount", "segTransactions[0,0]", "lblAccountName"], "text");
	    expect(fromAccountName).toContain(fromAccount);
	    // :End User Injected Code Snippet {8ee899e9-e37c-2be5-f725-bbcdb1020dc4}
	    kony.automation.segmentedui.click(["frmMMTransferFromAccount", "segTransactions[0,0]"]);
	    await kony.automation.playback.waitFor(["frmMMTransferToAccount", "tbxSearch"]);
	    kony.automation.widget.touch(["frmMMTransferToAccount", "tbxSearch"], [146, 13], null, null);
	    await kony.automation.playback.waitFor(["frmMMTransferToAccount", "customSearchbox", "tbxSearch"]);
	    await kony.automation.playback.wait(2000);
	    kony.automation.textbox.enterText(["frmMMTransferToAccount", "customSearchbox", "tbxSearch"], toAccount);
	    await kony.automation.playback.waitFor(["frmMMTransferToAccount", "segTransactions"]);
	    kony.automation.segmentedui.scrollToRow(["frmMMTransferToAccount", "segTransactions[0,0]"]);
	    await kony.automation.playback.wait(2000);
	    // :User Injected Code Snippet [// - [2 lines]]
	    var accountName = kony.automation.widget.getWidgetProperty(["frmMMTransferToAccount", "segTransactions[0,0]", "lblAccountName"], "text");
	    expect(accountName).toContain(toAccount);
	    // :End User Injected Code Snippet {195a3ae8-db55-4acb-396f-09b3a08ff861}
	    kony.automation.segmentedui.click(["frmMMTransferToAccount", "segTransactions[0,0]"]);
	    await kony.automation.playback.waitFor(["frmMMTransferAmount", "keypad", "btnThree"]);
	}
	
	async function enterAmount() {
	    await kony.automation.playback.waitFor(["frmMMTransferAmount", "keypad", "btnThree"]);
	    kony.automation.button.click(["frmMMTransferAmount", "keypad", "btnThree"]);
	    kony.automation.button.click(["frmMMTransferAmount", "keypad", "btnZero"]);
	    kony.automation.button.click(["frmMMTransferAmount", "keypad", "btnZero"]);
	    kony.automation.button.click(["frmMMTransferAmount", "btnContinue"]);
	    await kony.automation.playback.waitFor(["frmMMReview", "flxConfirmationDetails", "segDetails"]);
	}
	
	async function oneTimeTransfer() {
	
	    await kony.automation.playback.waitFor(["frmMMReview", "btnTransfer"]);
	    kony.automation.button.click(["frmMMReview", "btnTransfer"]);
	    await kony.automation.playback.waitFor(["frmMMConfirmation", "lblSuccessMessage"]);
	    // :User Injected Code Snippet [//Assert success message - [1 lines]]
		var lblMessage = kony.automation.widget.getWidgetProperty(["frmMMConfirmation", "lblMessage"], "text");
		var lblSuccessMessage = kony.automation.widget.getWidgetProperty(["frmMMConfirmation", "lblSuccessMessage"], "text");
			            
		expect(lblMessage.toLowerCase()).toContain("success");
		expect(lblSuccessMessage.toLowerCase()).toContain("success");
	    //expect(kony.automation.widget.getWidgetProperty(["frmMMConfirmation", "lblSuccessMessage"], "text")).toContain("uccess");
	    // :End User Injected Code Snippet {d03b935e-9e64-4c3e-db7c-f2e1959569b7}
	    await kony.automation.playback.waitFor(["frmMMConfirmation", "btnDashboard"]);
	    kony.automation.button.click(["frmMMConfirmation", "btnDashboard"]);
	    await kony.automation.playback.waitFor(["frmUnifiedDashboard", "flxDashboard", "segAccounts"]);
	}
	
	async function transferScheduledOnce() {
	    await kony.automation.playback.waitFor(["frmMMReview", "segDetails"]);
	    kony.automation.segmentedui.click(["frmMMReview", "segDetails[0,0]"]);
	    await kony.automation.playback.wait(3000);
	    await kony.automation.playback.waitFor(["frmMMFrequency", "segOptions"]);
	    kony.automation.segmentedui.click(["frmMMFrequency", "segOptions[0,0]"]);
	    await kony.automation.playback.wait(5000);
	    await kony.automation.playback.waitFor(["frmMMStartDate", "customCalendar", "flxNextMonth"]);
	    kony.automation.flexcontainer.click(["frmMMStartDate", "customCalendar", "flxNextMonth"]);
	    await kony.automation.playback.wait(2000);
	    // :User Injected Code Snippet [//Select a date - [3 lines]]
	    await kony.automation.playback.waitFor(["frmMMStartDate", "customCalendar", "flxNextMonth"]);
	    kony.automation.widget.touch(["frmMMStartDate", "customCalendar", "flxNextMonth"], [178, 125], null, [178, 125]);
	    kony.automation.widget.touch(["frmMMStartDate", "customCalendar", "flxMonth", "m3CopyLabel0ac5bc532de9c4c"], null, null, [17, 17]);
	    // :End User Injected Code Snippet {414f83d0-e0a2-735c-c437-a564878872a6}
	    await kony.automation.playback.wait(3000);
	    await kony.automation.playback.waitFor(["frmMMStartDate", "btnContinue"]);
	    kony.automation.button.click(["frmMMStartDate", "btnContinue"]);
	    await kony.automation.playback.wait(4000);
	    await kony.automation.playback.waitFor(["frmMMReview", "btnTransfer"]);
	    kony.automation.button.click(["frmMMReview", "btnTransfer"]);
	    await kony.automation.playback.wait(7000);
	    // :User Injected Code Snippet [//Asser for success - [1 lines]]
		var lblMessage = kony.automation.widget.getWidgetProperty(["frmMMConfirmation", "lblMessage"], "text");
		var lblSuccessMessage = kony.automation.widget.getWidgetProperty(["frmMMConfirmation", "lblSuccessMessage"], "text");
	  
		expect(lblMessage.toLowerCase()).toContain("success");
		expect(lblSuccessMessage.toLowerCase()).toContain("success");
	    //expect(kony.automation.widget.getWidgetProperty(["frmMMConfirmation", "lblSuccessMessage"], "text")).toEqual("We successfully scheduled your transfer");
	    // :End User Injected Code Snippet {ca0bdba2-106a-5d30-5a72-5e4aa2ed92df}
	    kony.automation.button.click(["frmMMConfirmation", "btnDashboard"]);
	    await kony.automation.playback.wait(8000);
	    await kony.automation.playback.waitFor(["frmUnifiedDashboard", "flxDashboard", "segAccounts"]);
	}
	
	it("LoginAndWaitForDashboard", async function() {
		await kony.automation.playback.waitFor(["frmLogin","login","tbxUsername"]);
		kony.automation.textbox.enterText(["frmLogin","login","tbxUsername"],"dbxJasmine");
		kony.automation.textbox.enterText(["frmLogin","login","tbxPassword"],"Kony@1234");
		await kony.automation.playback.waitFor(["frmLogin","login","btnLogIn"]);
		kony.automation.button.click(["frmLogin","login","btnLogIn"]);
		//Verifying Terms and Condition page -
			var frmTnC = await kony.automation.playback.waitFor(["frmTermsAndCondition","flxCheckBox"],20000);
			if(frmTnC){
				kony.automation.flexcontainer.click(["frmTermsAndCondition","flxCheckBox"]);
				await kony.automation.playback.waitFor(["frmTermsAndCondition","btnContinue"]);
				kony.automation.button.click(["frmTermsAndCondition","btnContinue"]);
		}
		
		await kony.automation.playback.waitFor(["frmUnifiedDashboard","lblBankName"],5000);
	},90000);
	
	it("ViewAllTransactions", async function() {
		// :User Injected Code Snippet [// - []]
		
		// :End User Injected Code Snippet {586b41fd-0b17-31aa-0584-bad24903b87f}
		await kony.automation.playback.waitFor(["frmUnifiedDashboard","btnViewTransactionsGraph"]);
		await kony.automation.scrollToWidget(["frmUnifiedDashboard","btnViewTransactionsGraph"]);
		await kony.automation.playback.wait(1000);
		kony.automation.button.click(["frmUnifiedDashboard","btnViewTransactionsGraph"]);
		await kony.automation.playback.wait(15000);
		await kony.automation.playback.waitFor(["frmPFMCategorisedTransactions","flxMainContainer","segTransactions"]);
		// :User Injected Code Snippet [// - [2 lines]]
		var segSize = kony.automation.widget.getWidgetProperty(["frmPFMCategorisedTransactions","flxMainContainer","segTransactions"],"data");
		expect(true).toBe(segSize.length > 0);
		// :End User Injected Code Snippet {9b56cb59-8470-3853-283c-77d3cf4c2dd1}
		kony.automation.segmentedui.click(["frmPFMCategorisedTransactions","segTransactions[0,0]"]);
		await kony.automation.playback.wait(1000);
		await kony.automation.playback.waitFor(["frmPFMTransactionDetails","lblRecurrenceValueTrans"]);
		// :User Injected Code Snippet [// - [12 lines]]
		
		var status = kony.automation.widget.getWidgetProperty(["frmPFMTransactionDetails","lblSuccess"],"text");
		var date = kony.automation.widget.getWidgetProperty(["frmPFMTransactionDetails","lblTransDateValueTrans"],"text");
		var refNumber = kony.automation.widget.getWidgetProperty(["frmPFMTransactionDetails","lblReferenceNoValueTrans"],"text");
		var category = kony.automation.widget.getWidgetProperty(["frmPFMTransactionDetails","lblSelectedCategoryValue"],"text");
		
		if(status!=null && date!=null && refNumber!=null && category!=null){
		expect(true).toBe(true);
		}
		else{
		expect(true).toBe(false);
		}
		// :End User Injected Code Snippet {3f46ce39-be32-664c-6b83-6616fb41acaa}
		await kony.automation.device.deviceBack();
		await kony.automation.device.deviceBack();
		await kony.automation.playback.wait(2000);
		await kony.automation.playback.waitFor(["frmUnifiedDashboard","flxDashboard","segAccounts"]);
	});
	
	it("SearchBill", async function() {
		// :User Injected Code Snippet [// - [2 lines]]
		await openMyBillsPage();
		//await searchBill("Electricity");
		// :End User Injected Code Snippet {d2140bba-8ba1-7cf1-75e1-9e8afce40bb6}
	});
	
	it("ManageBillPayShowAllPayee", async function() {
		await kony.automation.playback.waitFor(["frmBillPay","flxManage"]);
		kony.automation.flexcontainer.click(["frmBillPay","flxManage"]);
		await kony.automation.playback.wait(2000);
		// :User Injected Code Snippet [//All payee should be displayed - [8 lines]]
		var isSegAccounts = await kony.automation.playback.waitFor(["frmBillPayAllPayees","flxMainContainer","segAccounts"]);
		if(isSegAccounts){
		var segSize = kony.automation.widget.getWidgetProperty(["frmBillPayAllPayees","flxMainContainer","segAccounts"],"data");	
		expect(true).toBe(segSize.length > 0);
		}
		else{
		expect(isSegAccounts).toBe(true);
		}
		// :End User Injected Code Snippet {2cef7f31-3ef3-b7a0-f1b2-ef596de51464}
		await kony.automation.device.deviceBack();
		await kony.automation.playback.wait(3000);
		await kony.automation.playback.waitFor(["frmBillPay","imgManage"]);
	});
	
	it("OneTimeBillPaymentCreditCard", async function() {
		// :User Injected Code Snippet [//Prerequisite OpenMyBillsPage - []]
		
		// :End User Injected Code Snippet {a65252e0-747f-ad2a-d557-d03b0160ae0a}
		await kony.automation.playback.waitFor(["frmBillPay","flxPayABill"]);
		kony.automation.flexcontainer.click(["frmBillPay","flxPayABill"]);
		await kony.automation.playback.waitFor(["frmBillPaySelectPayee","tbxSearch"]);
		kony.automation.widget.touch(["frmBillPaySelectPayee","tbxSearch"], [168,17],null,null);
		kony.automation.textbox.enterText(["frmBillPaySelectPayee","customSearchbox","tbxSearch"],"credit");
		await kony.automation.playback.wait(2000);
		kony.automation.segmentedui.click(["frmBillPaySelectPayee","segAccounts[0,0]"]);
		await kony.automation.playback.wait(2000);
		await kony.automation.playback.waitFor(["frmBillPayAmount","keypad","btnTwo"]);
		kony.automation.button.click(["frmBillPayAmount","keypad","btnTwo"]);
		kony.automation.button.click(["frmBillPayAmount","keypad","btnZero"]);
		kony.automation.button.click(["frmBillPayAmount","keypad","btnZero"]);
		kony.automation.button.click(["frmBillPayAmount","btnContinue"]);
		await kony.automation.playback.wait(3000);
		await kony.automation.playback.waitFor(["frmBillPayFrequency","segFrequency"]);
		kony.automation.segmentedui.click(["frmBillPayFrequency","segFrequency[0,1]"]);
		await kony.automation.playback.wait(3000);
		await kony.automation.playback.waitFor(["frmBillPayStartDate","customCalendar","flxNextMonth"]);
		kony.automation.flexcontainer.click(["frmBillPayStartDate","customCalendar","flxNextMonth"]);
		await kony.automation.playback.wait(2000);
		// :User Injected Code Snippet [//Select a date - [3 lines]]
		await kony.automation.playback.waitFor(["frmBillPayStartDate","customCalendar","flxNextMonth"]);
		kony.automation.widget.touch(["frmBillPayStartDate","customCalendar","flxNextMonth"], [178,125],null,[178,125]);
		kony.automation.widget.touch(["frmBillPayStartDate","customCalendar","flxMonth","m3CopyLabel0ac5bc532de9c4c"], null,null,[17,17]);
		// :End User Injected Code Snippet {8bcca5ef-f943-159e-630f-6d56635bd3dc}
		await kony.automation.playback.wait(4000);
		await kony.automation.playback.waitFor(["frmBillPayConfirmation","txtDescription"]);
		kony.automation.textbox.enterText(["frmBillPayConfirmation","txtDescription"],"Payment by automation");
		kony.automation.flexcontainer.click(["frmBillPayConfirmation","flxCheckBox"]);
		await kony.automation.playback.wait(1000);
		await kony.automation.playback.waitFor(["frmBillPayConfirmation","btnContinue"]);
		kony.automation.button.click(["frmBillPayConfirmation","btnContinue"]);
		await kony.automation.playback.wait(2000);
		// :User Injected Code Snippet [//Assert on popup for success message - [3 lines]]
		await kony.automation.playback.waitFor(["frmBillPay","flxPopup","customPopup","flxPopupWrapper","lblPopup"],15000);
		
		expect(kony.automation.widget.getWidgetProperty(["frmBillPay","flxPopup","customPopup","flxPopupWrapper","lblPopup"], "text")).toContain("uccess");
		// :End User Injected Code Snippet {431282da-2019-5ab3-73e2-bcf00c9e1173}
		await kony.automation.playback.wait(3000);
		// :User Injected Code Snippet [// - [1 lines]]
		await goBackToDashboard();
		// :End User Injected Code Snippet {4b660dbc-0926-fb06-9a96-286702418fa9}
	});
	
	it("SearchInFromAndToScreen", async function() {
		// :User Injected Code Snippet [// - [5 lines]]
		await goToTransfers();
		await searchInFromAndToScreen("Check", "Credit");
		await kony.automation.device.deviceBack();
		await kony.automation.device.deviceBack();
		await kony.automation.device.deviceBack();
		// :End User Injected Code Snippet {cf3928e2-539e-4c52-1a93-19e4aa39a9cd}
		await kony.automation.playback.wait(1000);
		await kony.automation.playback.waitFor(["frmUnifiedDashboard","customFooter","imgTransfer"]);
	});
	
	it("TransferOwnAccountOneTime", async function() {
		// :User Injected Code Snippet [// - [4 lines]]
		await goToTransfers();
		await searchInFromAndToScreen("Check", "Credit");
		await enterAmount();
		await oneTimeTransfer();
		// :End User Injected Code Snippet {b25c4d32-5742-8fd6-a194-4e58ea8338a4}
	});
	
	it("TransferOwnAccountScheduleOnce", async function() {
		// :User Injected Code Snippet [// - [4 lines]]
		await goToTransfers();
		await searchInFromAndToScreen("Check","Credit");
		await enterAmount();
		await transferScheduledOnce();
		// :End User Injected Code Snippet {bc46289f-4cc8-733d-8a0d-8db169bcf97c}
	});
	
	it("ListOfRecipientsExternalAccount", async function() {
		// :User Injected Code Snippet [// - [2 lines]]
		await openMenu("Manage Beneficiaries");
		await listOfRecipients("External Account");
		// :End User Injected Code Snippet {b33c0d83-f774-b00b-3d67-4ec7934816e8}
	});
	
	it("AddRecipientExternalAccount", async function() {
		// :User Injected Code Snippet [// - [2 lines]]
		await addRecipientExternalAccount();
		await goBackToDashboardFromManageRecipient();
		// :End User Injected Code Snippet {bb73f386-0da9-5dea-e9d7-34b0ba9a154a}
	});
	
	it("ListOfRecipientsForSameAccount", async function() {
		// :User Injected Code Snippet [// - [2 lines]]
		await openMenu("Manage Beneficiaries");
		await listOfRecipients("Account");
		// :End User Injected Code Snippet {0ab4f8e6-02ad-6df5-5e61-e45d136bef93}
	});
	
	it("AddRecipientSameBank", async function() {
		// :User Injected Code Snippet [// - [2 lines]]
		await addRecipientSameBank();
		await goBackToDashboardFromManageRecipient();
		// :End User Injected Code Snippet {bb73f386-0da9-5dea-e9d7-34b0ba9a154a}
	});
	
	it("ListOfRecipientsInternationalAccount", async function() {
		// :User Injected Code Snippet [// - [2 lines]]
		await openMenu("Manage Beneficiaries");
		await listOfRecipients("International Account");
		// :End User Injected Code Snippet {b33c0d83-f774-b00b-3d67-4ec7934816e8}
	});
	
	it("AddRecipientInternationalAccount", async function() {
		// :User Injected Code Snippet [// - [2 lines]]
		await addRecipientInternationalAccount();
		await goBackToDashboardFromManageRecipient();
		// :End User Injected Code Snippet {bec13940-9a90-5c8f-8d46-6be6cbedc0f2}
	});
	
	it("TransferSameBankAccountOneTime", async function() {
		// :User Injected Code Snippet [// - [4 lines]]
		await goToTransfers();
		await searchInFromAndToScreen("Check", "samebank");
		await enterAmount();
		await oneTimeTransfer();
		// :End User Injected Code Snippet {b25c4d32-5742-8fd6-a194-4e58ea8338a4}
	});
	
	it("TransferSameBankAccountScheduleOnce", async function() {
		// :User Injected Code Snippet [// - [4 lines]]
		await goToTransfers();
		await searchInFromAndToScreen("Check","samebank");
		await enterAmount();
		await transferScheduledOnce();
		// :End User Injected Code Snippet {bc46289f-4cc8-733d-8a0d-8db169bcf97c}
	});
	
	it("TransferExternalAccountOneTime", async function() {
		// :User Injected Code Snippet [// - [4 lines]]
		await goToTransfers();
		await searchInFromAndToScreen("Check", "ExtAccIOS");
		await enterAmount();
		await oneTimeTransfer();
		// :End User Injected Code Snippet {cbc92a73-fbb1-4eb2-d096-a72bcb460a3c}
	});
	
	it("TransferExternalAccountScheduleOnce", async function() {
		// :User Injected Code Snippet [// - [4 lines]]
		await goToTransfers();
		await searchInFromAndToScreen("Check", "ExtAccIOS");
		await enterAmount();
		await transferScheduledOnce();
		// :End User Injected Code Snippet {cbc92a73-fbb1-4eb2-d096-a72bcb460a3c}
	});
	
	it("Logout", async function() {
		await kony.automation.playback.waitFor(["frmUnifiedDashboard","customFooter","flxMore"]);
		kony.automation.flexcontainer.click(["frmUnifiedDashboard","customFooter","flxMore"]);
		await kony.automation.playback.waitFor(["frmUnifiedDashboard","Hamburger","flxLogout"]);
		kony.automation.widget.touch(["frmUnifiedDashboard","Hamburger","flxLogout"], null,null,[50,31]);
		await kony.automation.playback.wait(4000);
		await kony.automation.playback.waitFor(["frmLogout","btnLogIn"]);
		kony.automation.button.click(["frmLogout","btnLogIn"]);
		await kony.automation.playback.wait(3000);
		await kony.automation.playback.waitFor(["frmLogin","login","tbxUsername"]);
		expect(kony.automation.widget.getWidgetProperty(["frmLogin","login","tbxPassword"], "text")).toEqual("");
	});
});