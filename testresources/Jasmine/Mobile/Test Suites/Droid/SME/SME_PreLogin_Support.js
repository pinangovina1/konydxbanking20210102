describe("SME_PreLogin_Support", function() {
	it("Support_AppVersion", async function() {
		await kony.automation.playback.waitFor(["frmLogin","btnSupport"]);
		kony.automation.button.click(["frmLogin","btnSupport"]);
	
		await kony.automation.playback.waitFor(["frmSupport","flxAppVersion","lblAppVersion"]);
		var appVersion = kony.automation.widget.getWidgetProperty(["frmSupport","flxAppVersion","lblAppVersion"],"text");
	
		expect(appVersion).toEqual('App Version 2020.10.00');
		kony.automation.flexcontainer.click(["frmSupport","customHeader","flxBack"]);
		await kony.automation.playback.waitFor(["frmLogin","btnSupport"]);
	});
	
	it("PreLogin_VerifyLanguage_UK_English", async function() {
		await kony.automation.playback.waitFor(["frmLogin","flxLanguageSelection"]);
		kony.automation.flexcontainer.click(["frmLogin","flxLanguageSelection"]);
		await kony.automation.playback.waitFor(["frmLogin","segSelectLanguage"]);
		kony.automation.segmentedui.click(["frmLogin","segSelectLanguage[0,4]"]);
		await kony.automation.playback.waitFor(["frmLogin","btnupdateLanguage"]);
		kony.automation.button.click(["frmLogin","btnupdateLanguage"]);
		kony.automation.alert.click(0);
		await kony.automation.playback.waitFor(["frmLogin","flxLanguageSelection"]);
	});
	
	it("PreLogin_VerifyLanguage_UK_English", async function() {
		await kony.automation.playback.waitFor(["frmLogin","flxLanguageSelection"]);
		kony.automation.flexcontainer.click(["frmLogin","flxLanguageSelection"]);
		await kony.automation.playback.waitFor(["frmLogin","segSelectLanguage"]);
		kony.automation.segmentedui.click(["frmLogin","segSelectLanguage[0,3]"]);
		await kony.automation.playback.waitFor(["frmLogin","btnupdateLanguage"]);
		kony.automation.button.click(["frmLogin","btnupdateLanguage"]);
		kony.automation.alert.click(0);
		await kony.automation.playback.waitFor(["frmLogin","flxLanguageSelection"]);
	});
	
	it("PreLogin_VerifyLanguage_UK_English", async function() {
		await kony.automation.playback.waitFor(["frmLogin","flxLanguageSelection"]);
		kony.automation.flexcontainer.click(["frmLogin","flxLanguageSelection"]);
		await kony.automation.playback.waitFor(["frmLogin","segSelectLanguage"]);
		kony.automation.segmentedui.click(["frmLogin","segSelectLanguage[0,2]"]);
		await kony.automation.playback.waitFor(["frmLogin","btnupdateLanguage"]);
		kony.automation.button.click(["frmLogin","btnupdateLanguage"]);
		kony.automation.alert.click(0);
		await kony.automation.playback.waitFor(["frmLogin","flxLanguageSelection"]);
	});
	
	it("PreLogin_VerifyLanguage_UK_English", async function() {
		await kony.automation.playback.waitFor(["frmLogin","flxLanguageSelection"]);
		kony.automation.flexcontainer.click(["frmLogin","flxLanguageSelection"]);
		await kony.automation.playback.waitFor(["frmLogin","segSelectLanguage"]);
		kony.automation.segmentedui.click(["frmLogin","segSelectLanguage[0,1]"]);
		await kony.automation.playback.waitFor(["frmLogin","btnupdateLanguage"]);
		kony.automation.button.click(["frmLogin","btnupdateLanguage"]);
		kony.automation.alert.click(0);
		await kony.automation.playback.waitFor(["frmLogin","flxLanguageSelection"]);
	});
	
	it("PreLogin_VerifyLanguage_UK_English", async function() {
		await kony.automation.playback.waitFor(["frmLogin","flxLanguageSelection"]);
		kony.automation.flexcontainer.click(["frmLogin","flxLanguageSelection"]);
		await kony.automation.playback.waitFor(["frmLogin","segSelectLanguage"]);
		kony.automation.segmentedui.click(["frmLogin","segSelectLanguage[0,0]"]);
		await kony.automation.playback.waitFor(["frmLogin","btnupdateLanguage"]);
		kony.automation.button.click(["frmLogin","btnupdateLanguage"]);
		kony.automation.alert.click(0);
		await kony.automation.playback.waitFor(["frmLogin","flxLanguageSelection"]);
	});
	
	it("VerifyFAQTnC", async function() {
	  await kony.automation.playback.waitFor(["frmLogin","btnSupport"]);
	  kony.automation.button.click(["frmLogin","btnSupport"]);
	  await kony.automation.playback.waitFor(["frmSupport","segSupport"]);
	  kony.automation.segmentedui.click(["frmSupport","segSupport[0,0]"]);
	  await kony.automation.playback.waitFor(["frmSupportInfo","segFaq"]);
	  await kony.automation.scrollToWidget(["frmSupportInfo","segFaq"]);
	  await kony.automation.playback.waitFor(["frmSupportInfo","segFaq"]);
	  kony.automation.segmentedui.click(["frmSupportInfo","segFaq[9,4]"]);
	  await kony.automation.playback.waitFor(["frmSupportInfo","customHeader","flxBack"]);
	  kony.automation.flexcontainer.click(["frmSupportInfo","customHeader","flxBack"]);
	  await kony.automation.playback.waitFor(["frmSupport","segSupport"]);
	  kony.automation.segmentedui.click(["frmSupport","segSupport[0,1]"]);
	  await kony.automation.playback.waitFor(["frmSupportInfo","customHeader","flxBack"]);
	  kony.automation.flexcontainer.click(["frmSupportInfo","customHeader","flxBack"]);
	  await kony.automation.playback.waitFor(["frmSupport","segSupport"]);
	  kony.automation.segmentedui.click(["frmSupport","segSupport[0,2]"]);
	  await kony.automation.playback.waitFor(["frmSupportInfo","customHeader","flxBack"]);
	  kony.automation.flexcontainer.click(["frmSupportInfo","customHeader","flxBack"]);
	  await kony.automation.playback.waitFor(["frmSupport","customHeader","flxBack"]);
	  kony.automation.flexcontainer.click(["frmSupport","customHeader","flxBack"]);
	  await kony.automation.playback.waitFor(["frmLogin","btnSupport"]);
	  kony.automation.playback.wait(15000);
	});
});