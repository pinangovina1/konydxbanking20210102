require(["Test Suites/Droid/RB/PreLogin_Support"], function() {
	require(["Test Suites/Droid/SME/SME_PreLogin_Support"], function() {
		require(["Test Suites/Droid/RB/MyBills"], function() {
			require(["Test Suites/Droid/SME/SME_MyBills"], function() {
				require(["Test Suites/Droid/RB/SettingsSuite"], function() {
					require(["Test Suites/Droid/SME/SME_SettingsSuite"], function() {
						require(["Test Suites/Droid/RB/Transfers"], function() {
							require(["Test Suites/Droid/SME/SME_Transfers"], function() {
																jasmine.getEnv().execute();
							});
						});
					});
				});
			});
		});
	});
});