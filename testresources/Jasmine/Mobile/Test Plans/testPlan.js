//This is the entry point for automation. You can either:
//1.Require any one of the created test plans like this:
//require([/*<test plan file>*/]);

// or
//2.  require the test suites along with executing jasmine as below
//Nested require for test suites will ensure the order of test suite exectuion
//Since this is file is to be manually edited, make sure to update 
//any changes (rename/delete) to the test suites/plans.


var deviceInfo = kony.os.deviceInfo();

var userType = "_RB"; //valid values are "_RB" and "_SME"
var executionType = "_Sanity"; // "_Sanity" or "_Regression"

if (deviceInfo.name === "iPhone") {
    jasmine.DEFAULT_TIMEOUT_INTERVAL = 90000;
//     require(["Test Plans/TestPlanIOS" + userType + executionType]);
	require(["Test Plans/SanityPack_IOS"]);
} else {
    //require(["Test Plans/TestPlanAndroid"+userType+ executionType]);
    require(["Test Plans/SanityPack_Android"]);
}


/*

if(deviceInfo.name === "iPhone"){
  
  require(["Test Plans/DemoIOS"]);
  
}else{
  
  require(["Test Plans/DemoAndroid"]);
}
*/