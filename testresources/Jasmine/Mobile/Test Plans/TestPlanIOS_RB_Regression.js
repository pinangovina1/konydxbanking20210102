require(["Test Suites/IOS/RB/RB_PreLogin_Support"], function() {
	require(["Test Suites/IOS/RB/RB_LoginSuite"], function() {
		require(["Test Suites/IOS/RB/RB_AccountsDashboard"], function() {
			require(["Test Suites/IOS/RB/RB_MyBills"], function() {
				require(["Test Suites/IOS/RB/RB_AddRecipientInternationalAccount"], function() {
					require(["Test Suites/IOS/RB/RB_AddRecipientExternalAccount"], function() {
						require(["Test Suites/IOS/RB/RB_AddRecipientSameBank"], function() {
							require(["Test Suites/IOS/RB/RB_TransfersSuite"], function() {
								require(["Test Suites/IOS/RB/RB_LogoutSuite"], function() {
																		jasmine.getEnv().execute();
								});
							});
						});
					});
				});
			});
		});
	});
});